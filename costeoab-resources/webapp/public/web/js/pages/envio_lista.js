var urlMain;

function EnvioLista(url) {
    urlMain = url;
    this.url = url;
    this.tblEnvios = $('#tbl-envios');
    this.inptFiltroDesde = $('#inpt-filtro-desde');
    this.inptFiltroHasta = $('#inpt-filtro-hasta');
    this.slcFiltroEmpresa = $('#slc-filtro-empresa');
    this.slcFiltroAnio = $('#slc-filtro-anio');
    this.inptFiltroSemana = $('#inpt-filtro-semana');
    this.btnBuscar = $('#btn-buscar');
    this.btnNuevo = $('#btn-nuevo');

    this.modalNuevo = $('#modal-nuevo');
    this.frmNuevo = $('#frm-nuevo');
    this.slcEmpresa = $('#slc-empresa');
    this.slcAnio = $('#slc-anio');
    this.inptSemana = $('#inpt-semana');
    this.inptFechaProduccion = $('#inpt-fecha-produccion');
    this.inptCcg = $('#inpt-ccg');
    this.inptMmi = $('#inpt-mmi');
    this.inptViajes = $('#inpt-viajes');
    this.inptObservaciones = $('#inpt-observaciones');
    this.btnGuardar = $('#btn-guardar');

    this.modalGenerarCosteo = $('#modal-generar-costeo');
    this.frmGenerarCosteo = $('#frm-generar-costeo');
    this.inptGenerarNumero = $('#inpt-generar-numero');
    this.inptGenerarEmpresa = $('#inpt-generar-empresa');
    this.inptGenerarAnio = $('#inpt-generar-anio');
    this.inptGenerarSemana = $('#inpt-generar-semana');
    this.inptGenerarFechaProduccion = $('#inpt-generar-fecha-produccion');
    this.inptGenerarCcg = $('#inpt-generar-ccg');
    this.inptGenerarMmi = $('#inpt-generar-mmi');
    this.inptGenerarViajes = $('#inpt-generar-viajes');
    this.inptFechaTipoCambio = $('#inpt-fecha-tipo-cambio');
    this.inptTipoCambio = $('#inpt-tipo-cambio');
    this.inptPrecioFleteViaje = $('#inpt-precio-flete-viaje');
    this.btnGenerarCosteo = $('#btn-generar-costeo');
}

EnvioLista.prototype.init = function () {
    this.listarEnvios();
    this.validarFrmNuevo();
    this.validarFrmGenerarCosteo();
    this.handler();
};

EnvioLista.prototype.handler = function () {
    var obj = this;

    obj.inptFiltroDesde.datepickerInFullscreen({
        todayWord: 'Hoy',
        clearWord: 'Limpiar',
        closeWord: 'Cerrar',
        datepicker: {
            language: 'es'
        },
        format: 'DD/MM/YYYY',
        fakeInputFormat: 'DD/MM/YYYY',
        touchSwipe: false,
        beforeOpen : function(modal, settings) {
            var hastaDate = moment(obj.inptFiltroHasta.val(), 'DD/MM/YYYY');
            if (hastaDate.isValid()) {
                settings.datepicker.endDate 		= hastaDate.toDate();
                settings.datepicker.defaultViewDate = hastaDate.toDate();
            } else {
                settings.datepicker.endDate         = Infinity;
                settings.datepicker.defaultViewDate = new Date();
            }
        }
    });

    obj.inptFiltroHasta.datepickerInFullscreen({
        todayWord: 'Hoy',
        clearWord: 'Limpiar',
        closeWord: 'Cerrar',
        datepicker: {
            language: 'es'
        },
        format: 'DD/MM/YYYY',
        fakeInputFormat: 'DD/MM/YYYY',
        touchSwipe: false,
        beforeOpen : function(modal, settings) {
            var desdeDate = moment(obj.inptFiltroDesde.val(), 'DD/MM/YYYY');
            if(desdeDate.isValid()){
                settings.datepicker.startDate 		= desdeDate.toDate();
                settings.datepicker.defaultViewDate = desdeDate.toDate();
            }else{
                settings.datepicker.startDate       = -Infinity;
                settings.datepicker.defaultViewDate = new Date();
            }
        }
    });

    obj.inptFechaProduccion.datepickerInFullscreen({
        todayWord: 'Hoy',
        clearWord: 'Limpiar',
        closeWord: 'Cerrar',
        datepicker: {
            language: 'es'
        },
        format: 'DD/MM/YYYY',
        fakeInputFormat: 'DD/MM/YYYY',
        fakeInput: false,
        touchSwipe: false,
        onChange: function(modal, settings) {
            obj.inptFechaProduccion.valid();
        }
    });

    obj.inptFechaTipoCambio.datepickerInFullscreen({
        todayWord: 'Hoy',
        clearWord: 'Limpiar',
        closeWord: 'Cerrar',
        datepicker: {
            language: 'es'
        },
        format: 'DD/MM/YYYY',
        fakeInputFormat: 'DD/MM/YYYY',
        fakeInput: false,
        touchSwipe: false,
        onChange: function(modal, settings) {
            obj.inptFechaTipoCambio.valid();
        }
    });

    obj.btnBuscar.on('click', function (e) {
        e.preventDefault();

        obj.reloadTblEnvios();
    });

    obj.btnNuevo.on('click', function (e) {
        e.preventDefault();

        obj.nuevoEnvio();

        obj.modalNuevo.modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    obj.btnGuardar.on('click', function (e) {
        e.preventDefault();

        if (obj.frmNuevo.valid()) {
            obj.guardarEnvio();
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos ','error','Aceptar');
        }
    });

    obj.tblEnvios.on('click', '.btn-eliminar', function (e) {
        e.preventDefault();

        var data = obj.tblEnvios.DataTable().row($(this).parents('tr')).data();
        Custom.showAlertSiNo(
            'Eliminar',
            '¿Est\u00E1 seguro de eliminar el Env\u00EDo con A\u00F1o \"' + data.envioAnio + '\" y Semana \"' + data.envioSemana + '\"?',
            'question',
            'Si',
            'No',
            function (result) {
                if (result.value) {
                    obj.eliminarEnvio(data.envioId);
                }
            });
    });

    obj.tblEnvios.on('click', '.btn-costeo', function (e) {
        e.preventDefault();

        var data = obj.tblEnvios.DataTable().row($(this).parents('tr')).data();

        obj.inptGenerarNumero.val(data.envioId);
        obj.inptGenerarEmpresa.val(data.empresa.empresaRazonSocial);
        obj.inptGenerarAnio.val(data.envioAnio);
        obj.inptGenerarSemana.val(data.envioSemana);
        obj.inptGenerarFechaProduccion.val(obj._formatFecha(data.envioFechaProduccion));
        obj.inptGenerarCcg.val(data.envioCCG);
        obj.inptGenerarMmi.val(data.envioMMI);
        obj.inptGenerarViajes.val(data.envioViajes);

        obj.modalGenerarCosteo.modal({
            backdrop: 'static',
            keyboard: false
        });

    });

    obj.btnGenerarCosteo.on('click', function (e) {
        e.preventDefault();

        if (obj.frmGenerarCosteo.valid()) {
            Custom.showAlertSiNo(
                'Confirmar',
                '¿Est\u00e1 seguro de Generar el Costeo para el Env\u00EDo?',
                'question',
                'Si',
                'No',
                function (result) {
                    if (result.value) {
                        obj.generarCosteo();
                    }
                });
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos ','error','Aceptar');
        }

    });
};

EnvioLista.prototype.validarFrmNuevo = function () {
    Custom.validateForm(this.frmNuevo, {
        data: {
            rules: {
                slcEmpresa: {
                    required: true,
                    minlength: 1
                },
                slcAnio: {
                    required: true,
                    minlength: 1
                },
                inptSemana: {
                    required: true,
                    number: true
                },
                inptFechaProduccion: {
                    required: true
                },
                inptCcg: {
                    required: true,
                    number: true
                },
                inptMmi: {
                    required: true,
                    number: true
                },
                inptViajes: {
                    required: true,
                    number: true
                },
                inptObservaciones: {
                    required: false
                }
            },
            messages: {
                slcEmpresa: {
                    required: 'Campo obligatorio',
                    minlength: 'Campo obligatorio'
                },
                slcAnio: {
                    required: 'Campo obligatorio',
                    minlength: 'Campo obligatorio'
                },
                inptSemana: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                },
                inptFechaProduccion: {
                    required: 'Campo obligatorio'
                },
                inptCcg: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                },
                inptMmi: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                },
                inptViajes: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                },
                inptObservaciones: {
                    required: 'Campo obligatorio'
                }
            }
        }
    })
};

EnvioLista.prototype.validarFrmGenerarCosteo = function () {
    Custom.validateForm(this.frmGenerarCosteo, {
        data: {
            rules: {
                inptFechaTipoCambio: {
                    required: true
                },
                inpTipoCambio: {
                    required: true,
                    number: true
                },
                inptPrecioFleteViaje: {
                    required: true,
                    number: true
                }
            },
            messages: {
                inptFechaTipoCambio: {
                    required: 'Campo obligatorio'
                },
                inpTipoCambio: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                },
                inptPrecioFleteViaje: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                }
            }
        }
    })
};

EnvioLista.prototype.reloadTblEnvios = function () {
    this.tblEnvios.DataTable().ajax.reload();
};

EnvioLista.prototype.listarEnvios = function () {
    var obj = this;

    obj.tblEnvios.DataTable({
        dom: "<''>"+
            "t"+
            "r"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        responsive: true,
        language: {
            search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            emptyTable: "No hay datos que mostrar.",
            processing: "Procesando...",
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No se encontraron resultados",
            info: "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoEmpty:      "Mostrando del 0 al 0 de 0 registros",
            loadingRecords: "Cargando...",
            paginate: {
                first: "<<",
                last: ">>",
                next: ">",
                previous: "<"
            }
        },
        sort: false,
        serverSide : true,
        processing: true,
        lengthMenu: [[5, 10, 20], [5, 10, 20]],
        displayLength: 15,
        ajax: {
            url: this.url + 'envios/listar',
            type: "post",
            data: function (d) {
                d.empresaId = obj.slcFiltroEmpresa.val();
                d.anio = obj.slcFiltroAnio.val();
                d.semana = obj.inptFiltroSemana.val();
                d.desdeString = obj.inptFiltroDesde.val();
                d.hastaString = obj.inptFiltroHasta.val();
            }
        },
        columns: [
            {'title': 'N&uacute;mero', 'class': 'text-center', 'width': '10', 'data': 'envioId', 'orderable': false},
            {'title': 'Empresa', 'data': 'empresa.empresaRazonSocial', 'orderable': false},
            {'title': 'A&ntilde;o', 'data': 'envioAnio', 'orderable': false},
            {'title': 'Semana', 'data': 'envioSemana', 'orderable': false},
            {'title': 'Fecha Producci&oacute;n', 'data': 'envioFechaProduccion', 'orderable': false, 'render': obj._formatFecha},
            {'title': 'Observaciones', 'data': 'envioObservaciones', 'orderable': false},
            {
                'data': null,
                'class': '',
                'width': '130',
                'orderable': false,
                'render': function(data, type, full) {
                    var r = '';
                    r += '<a href="' + urlMain + 'envios/detalle/' + data.envioId + '" class="btn btn-warning btn-xs m-r-xs">';
                    r += '<i class="fa fa-pencil"></i> Detalle';
                    r += '</a>';

                    r += '<button type="button" class="btn btn-danger btn-xs btn-eliminar m-r-xs" data-toggle="tooltip" data-placement="top" title="Eliminar">';
                    r += '<i class="fa fa-trash"></i> ';
                    r += '</button>';

                    if ($.trim(data.costeo.costeoId) === '') {
                        r += '<button type="button" class="btn btn-info btn-xs btn-costeo m-r-xs" data-toggle="tooltip" data-placement="top" title="Generar Costeo">';
                        r += '<i class="fa fa-dollar"></i> ';
                        r += '</button>';
                    } else {
                        r += '<a href="' + urlMain + 'costeos/detalle/' + data.costeo.costeoId + '" class="btn btn-info btn-xs m-r-xs" data-toggle="tooltip" data-placement="top" title="Ver Costeo">';
                        r += '<i class="fa fa-dollar"></i> ';
                        r += '</a>';
                    }

                    return r;
                }
            }
        ]
    });
};

EnvioLista.prototype._formatFecha = function(data, type, full) {
    if (data == null) {
        return '';
    }

    function pad(s) {
        return (s < 10) ? '0' + s : s;
    }

    var d = new Date(data);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
};

EnvioLista.prototype.nuevoEnvio = function () {
    var obj = this;
    obj.slcEmpresa.val('').trigger('change');
    obj.slcAnio.val('').trigger('change');
    obj.inptSemana.val('');
    obj.inptFechaProduccion.val('');
    obj.inptCcg.val('');
    obj.inptMmi.val('');
    obj.inptViajes.val('');
    obj.inptObservaciones.val('');

    obj.frmNuevo.find('.form-group').removeClass('has-error');
    obj.frmNuevo.validate().resetForm();
};

EnvioLista.prototype.guardarEnvio = function () {
    var obj = this;

    var request = {
        empresa: {
            empresaId: obj.slcEmpresa.val()
        },
        envioAnio: obj.slcAnio.val(),
        envioSemana: obj.inptSemana.val(),
        envioFechaProduccion: moment(obj.inptFechaProduccion.val(), 'DD/MM/YYYY'),
        envioCCG: obj.inptCcg.val(),
        envioMMI: obj.inptMmi.val(),
        envioViajes: obj.inptViajes.val(),
        envioObservaciones: obj.inptObservaciones.val().toUpperCase(),
        accion: 1
    };

    $.ajax({
        url: obj.url + 'envios/guardarEnvio',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadTblEnvios();
                obj.modalNuevo.modal('hide');
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al eliminar el Envio, contacte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

EnvioLista.prototype.eliminarEnvio = function (envioId) {
    var obj = this;

    var request = {
        envioId: envioId,
        accion: 3
    };

    $.ajax({
        url: obj.url + 'envios/eliminarEnvio',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadTblEnvios();
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al eliminar el Env\u00EDo, contacte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

EnvioLista.prototype.generarCosteo = function () {
    var obj = this;

    var request = {
        envioId: obj.inptGenerarNumero.val(),
        costeo: {
            costeoFechaTipoCambio: moment(obj.inptFechaTipoCambio.val(), 'DD/MM/YYYY'),
            costeoTipoCambio: obj.inptTipoCambio.val(),
            costeoPrecioFleteViaje: obj.inptPrecioFleteViaje.val()
        },
        accion: 4
    };

    $.ajax({
        url: obj.url + 'envios/generarCosteo',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                obj.reloadTblEnvios();
                obj.modalGenerarCosteo.modal('hide');

                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Ir al Costeo...',
                    function (result) {
                        if (result.value) {
                            window.location = urlMain + 'costeos/detalle/' + data.result;
                        }
                    }
                );
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al generar Costeo, contacte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

