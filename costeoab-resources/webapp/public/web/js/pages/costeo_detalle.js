var urlMain;
var factorFleteMain = 0;

function CosteoDetalle(url) {
    urlMain = url;
    this.url = url;
    this.accion = 0;

    this.frmCosteo = $('#frm-costeo');
    this.inptCosteoId = $('#inpt-costeo-id');
    this.inptFechaTipoCambio = $('#inpt-fecha-tipo-cambio');
    this.inptTipoCambio = $('#inpt-tipo-cambio');
    this.inptPrecioFleteViaje = $('#inpt-precio-flete-viaje');
    this.btnActualizarCosteo = $('#btn-actualizar-costeo');

    this.btnAgregarDetalle = $('#btn-agregar-detalle');
    this.tblDetalles = $('#tbl-detalles');

    this.modalDetalle = $('#modal-detalle');
    this.frmDetalle = $('#frm-detalle');
    this.inptCosteoDetalleId = $('#inpt-costeo-detalle-id');
    this.slcFormula = $('#slc-formula');
    this.inptCantidadProducida = $('#inpt-cantidad-producida');
    this.inptFactorFlete = $('#inpt-factor-flete');
    this.btnGuardarDetalle = $('#btn-guardar-detalle');

    this.divFactura = $('#div-factura');
    this.inptFactura = $('#inpt-factura');
    this.inptFechaEmision = $('#inpt-fecha-emision');
    this.inptGuiaRemision = $('#inpt-guia-remision');
    this.btnFactura = $('#btn-factura');

    this.modalFactura = $('#modal-factura');
    this.frmFactura = $('#frm-factura');
    this.inptFacturaFechaEmision = $('#inpt-factura-fecha-emision');
    this.inptFacturaSerie = $('#inpt-factura-serie');
    this.inptFacturaNumero = $('#inpt-factura-numero');
    this.inptFacturaGuiaRemision = $('#inpt-factura-guia-remision');
    this.btnGenerarFactura = $('#btn-generar-factura');
}

CosteoDetalle.prototype.init = function () {
    this.listarTblDetalles();
    this.validarFrmDetalle();
    this.validarFrmCosteo();
    this.validarFrmFactura();
    this.handler();
};

CosteoDetalle.prototype.handler = function () {
    var obj = this;

    obj.btnAgregarDetalle.on('click', function (e) {
        e.preventDefault();

        if (obj.frmCosteo.valid()) {
            obj.nuevoDetalle();

            obj.modalDetalle.modal({
                backdrop: 'static',
                keyboard: false
            });
        } else {
            Custom.showAlertAceptar('Error', 'Debe ingresar y guardar los datos del Costeo.','error','Aceptar');
        }
    });

    obj.slcFormula.on('change', function (e) {
        e.preventDefault();

        var cantidadProducida = $(this).find(':selected').attr('data-cantidad-producida');
        obj.inptCantidadProducida.val(cantidadProducida);

    });

    obj.btnGuardarDetalle.on('click', function (e) {
        e.preventDefault();

        if (obj.frmDetalle.valid()) {
            obj.guardarDetalle();
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos del Detalle','error','Aceptar');
        }
    });

    obj.tblDetalles.on('click', '.btn-editar', function (e) {
        e.preventDefault();

        var data = obj.tblDetalles.DataTable().row($(this).parents('tr')).data();
        obj.editarDetalle(data.costeoDetalleId);
    });

    obj.inptFechaTipoCambio.datepickerInFullscreen({
        todayWord: 'Hoy',
        clearWord: 'Limpiar',
        closeWord: 'Cerrar',
        datepicker: {
            language: 'es'
        },
        format: 'DD/MM/YYYY',
        fakeInputFormat: 'DD/MM/YYYY',
        fakeInput: false,
        touchSwipe: false,
        onChange: function(modal, settings) {
            obj.inptFechaTipoCambio.valid();
        }
    });

    obj.tblDetalles.on('click', '.btn-eliminar', function (e) {
        e.preventDefault();

        var data = obj.tblDetalles.DataTable().row($(this).parents('tr')).data();
        Custom.showAlertSiNo(
            'Eliminar',
            '¿Est\u00E1 seguro de eliminar el Detalle con la F\u00F3rmula \"' + data.formula.formulaDescripcion + '\"?',
            'question',
            'Si',
            'No',
            function (result) {
                if (result.value) {
                    obj.eliminarDetalle(data.costeoDetalleId, data.formula.formulaId);
                }
            });
    });

    obj.btnActualizarCosteo.on('click', function (e) {
        e.preventDefault();

        if (obj.frmCosteo.valid()) {
            obj.actualizarCosteo();
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos','error','Aceptar');
        }
    });

    obj.btnFactura.on('click', function (e) {
        e.preventDefault();

        obj.inptFacturaFechaEmision.val(obj.inptFechaTipoCambio.val());

        obj.modalFactura.modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    obj.btnGenerarFactura.on('click', function (e) {
        e.preventDefault();

        if (obj.frmFactura.valid()) {
            Custom.showAlertSiNo(
                'Confirmar',
                '¿Est\u00e1 seguro de Generar la Factura para el Costeo?',
                'question',
                'Si',
                'No',
                function (result) {
                    if (result.value) {
                        obj.generarFactura();
                    }
                });
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos','error','Aceptar');
        }
    });
};

CosteoDetalle.prototype.validarFrmCosteo = function () {
    Custom.validateForm(this.frmCosteo, {
        data: {
            rules: {
                inptFechaTipoCambio: {
                    required: true
                },
                inpTipoCambio: {
                    required: true,
                    number: true
                },
                inptPrecioFleteViaje: {
                    required: true,
                    number: true
                }
            },
            messages: {
                inptFechaTipoCambio: {
                    required: 'Campo obligatorio'
                },
                inpTipoCambio: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                },
                inptPrecioFleteViaje: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                }
            }
        }
    });
};

CosteoDetalle.prototype.validarFrmDetalle = function () {
    Custom.validateForm(this.frmDetalle, {
        data: {
            rules: {
                slcFormula: {
                    required: true,
                    minlength: 1
                },
                inptFactorFlete: {
                    required: true,
                    number: true,
                    min: 0.0
                }
            },
            messages: {
                slcFormula: {
                    required: 'Campo obligatorio',
                    minlength: 'Campo obligatorio'
                },
                inptFactorFlete: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros',
                    min: 'Valor minimo: 0.0'
                }
            }
        }
    });
};

CosteoDetalle.prototype.validarFrmFactura = function() {
    Custom.validateForm(this.frmFactura, {
        data: {
            rules: {
                inptFacturaSerie: {
                    required: true,
                    maxlength: 4
                },
                inptFacturaNumero: {
                    required: true,
                    number: true,
                    maxlength: 10
                },
                inptFacturaGuiaRemision: {
                    required: false,
                    maxlength: 60
                }
            },
            messages: {
                inptFacturaSerie: {
                    required: 'Campo obligatorio',
                    maxlength: 'Caracteres maximos: 4'
                },
                inptFacturaNumero: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros',
                    maxlength: 'Caracteres maximos: 10'
                },
                inptFacturaGuiaRemision: {
                    required: 'Campo obligatorio',
                    maxlength: 'Caracteres maximos: 60'
                }
            }
        }
    });
};

CosteoDetalle.prototype.reloadTblDetalles = function () {
    this.tblDetalles.DataTable().ajax.reload();
};

CosteoDetalle.prototype.listarTblDetalles = function () {
    var obj = this;

    obj.tblDetalles.DataTable({
        dom: "<''>"+
            "t"+
            "r"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        responsive: false,
        language: {
            search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            emptyTable: "No hay datos que mostrar.",
            processing: "Procesando...",
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No se encontraron resultados",
            info: "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoEmpty:      "Mostrando del 0 al 0 de 0 registros",
            loadingRecords: "Cargando...",
            paginate: {
                first: "<<",
                last: ">>",
                next: ">",
                previous: "<"
            }
        },
        sort: false,
        serverSide : true,
        processing: true,
        displayLength: 100,
        ajax: {
            url: this.url + 'costeos/listarDetalles',
            type: "post",
            data: function (d) {
                d.costeoId = obj.inptCosteoId.val();
            }
        },
        columns: [
            {
                'title': '#',
                'class': 'text-center',
                'width': '10px',
                'data': null,
                'orderable': false,
                "render": function (data, type, full, meta) {
                    return  meta.row + 1;
                }
            },
            {'title': 'Cantidad (kg)', 'class': 'text-right', 'data': 'formula.formulaCantidadProducida', 'orderable': false, 'render': obj._formatNumber},
            {
                'title': 'F&oacute;rmula',
                'data': null,
                'orderable': false,
                'render': function (data, type, full) {
                    return '<a href="' + urlMain + 'formulas/detalle/' + data.formula.formulaId + '" target="_blank">' +
                                data.formula.formulaCodigo + ' - ' + data.formula.formulaDescripcion + ' <i class="fa fa-external-link-square"></i>' +
                            '</a>'
                }
            },
            {'title': 'Total', 'class': 'text-right', 'data': 'costeoDetalleSubtotal', 'orderable': false, 'render': obj._formatNumber},
            {'title': 'Maquila', 'class': 'text-right', 'data': 'costeoDetalleMaquila', 'orderable': false, 'render': obj._formatNumber},
            {'title': 'Sacos (kg)', 'class': 'text-right', 'data': 'costeoDetalleSacos', 'orderable': false, 'render': obj._formatNumber},
            {'title': 'Flete', 'class': 'text-right', 'data': 'costeoDetalleFlete', 'orderable': false, 'render': obj._formatNumber},
            {'title': 'Factor', 'class': 'text-right', 'data': 'costeoDetalleFactorFlete', 'orderable': false, 'render': obj._formatNumber},
            {'title': 'PU s/IGV', 'class': 'text-right', 'data': 'costeoDetallePrecioUnitario', 'orderable': false, 'render': obj._formatNumber},
            {'title': 'Total', 'class': 'text-right', 'data': 'costeoDetalleTotal', 'orderable': false, 'render': obj._formatNumber},
            {
                'title': 'PU c/IGV',
                'class': 'text-right',
                'data': null,
                'orderable': false,
                'render': function (data, type, full) {
                    var igv = 1.18;
                    var puConIgv = data.costeoDetallePrecioUnitario * igv;
                    return obj._formatNumberPrecision(puConIgv, 4);
                }
            },
            {
                'title': '',
                'data': null,
                'class': '',
                'orderable': false,
                'render': function(data, type, full) {
                    var r = '';
                    r += '<button type="button" class="btn btn-warning btn-xs btn-editar m-r-xs">';
                    r += '<i class="fa fa-pencil"></i> Editar';
                    r += '</button>';

                    r += '<button type="button" class="btn btn-danger btn-xs btn-eliminar m-r-xs" data-toggle="tooltip" data-placement="top" title="Eliminar">';
                    r += '<i class="fa fa-trash"></i> ';
                    r += '</button>';

                    return r;
                }
            }
        ],
        "footerCallback": function(tfoot, data, start, end, display) {
            $(this).find('tfoot').html('');

            var api = this.api();

            // suma factor flete
            factorFleteMain = api.column(7).data().reduce(function (a, b) {
                return a + b;
            }, 0);

            // constantes
            var tonelada = 1000.00;
            var igv = 18.00;

            // suma de la columna "Cantidad"
            var sumaCantidad = api.column(1).data().reduce(function (a, b) {
                return a + b;
            }, 0);

            // suma de la Columna "Total"
            var sumaTotal = api.column(9).data().reduce(function (a, b) {
                return a + b;
            }, 0);

            // calculo de cu sin igv
            var cuSinIgv = 0.00;
            if (sumaCantidad > 0) {
                cuSinIgv = tonelada * sumaTotal / sumaCantidad;
            }

            // calculo del "Total Calculado"
            var totalCalculado = sumaTotal * (1 + (igv / 100));

            // calculo del "Total IGV"
            var totalIgv = totalCalculado - sumaTotal;

            // calculo de cu con igv
            var cuConIgv = 0.00;
            if (sumaCantidad > 0) {
                cuConIgv = tonelada * totalCalculado / sumaCantidad;
            }

            var footer = '<tfoot>';

            footer += '<tr>';
            footer += '<th class="text-center">T</th>';
            footer += '<th class="text-right">' + obj._formatNumber(sumaCantidad) + '</th>';
            footer += '<th class="text-right" colspan="3">CU s/IGV</th>';
            footer += '<th class="text-right">' + obj._formatNumberTruncate(cuSinIgv, 2) +  '</th>';
            footer += '<th class="text-right" colspan="3">Valor Venta</th>';
            footer += '<th class="text-right">' + obj._formatNumber(sumaTotal) + '</th>';
            footer += '<th colspan="2"></th>';
            footer += '</tr>';

            footer += '<tr>';
            footer += '<th class="text-right" colspan="8">IGV</th>';
            footer += '<th class="text-center">' + obj._formatNumber(igv) +  '</th>';
            footer += '<th class="text-right">' + obj._formatNumber(totalIgv) + '</th>';
            footer += '<th colspan="2"></th>';
            footer += '</tr>';

            footer += '<tr>';
            footer += '<th class="text-right" colspan="5">CU c/IGV</th>';
            footer += '<th class="text-right">' + obj._formatNumber(cuConIgv) +  '</th>';
            footer += '<th class="text-right" colspan="3">Total</th>';
            footer += '<th class="text-right">' + obj._formatNumber(totalCalculado) + '</th>';
            footer += '<th colspan="2"></th>';
            footer += '</tr>';

            footer += '</tfoot>';

            $(this).append(footer);
        }
    });
};

CosteoDetalle.prototype._formatNumber = function (data) {
    return accounting.formatMoney(data, "", 2, ",", ".");
};

CosteoDetalle.prototype._formatNumberPrecision = function (data, precision) {
    return accounting.formatMoney(data, "", precision, ",", ".");
};

CosteoDetalle.prototype._formatNumberTruncate = function (num, fixed) {
    fixed = fixed || 0;
    fixed = Math.pow(10, fixed);
    var result = Math.floor(num * fixed) / fixed;
    return this._formatNumber(result);
};

CosteoDetalle.prototype.nuevoDetalle = function () {
    var obj = this;

    obj.inptCosteoDetalleId.val('');
    obj.slcFormula.val('').trigger('change');
    obj.slcFormula.removeAttr("disabled");
    obj.inptCantidadProducida.val('');
    obj.inptFactorFlete.val('');
    obj.accion = 1;

    obj.frmDetalle.find('.form-group').removeClass('has-error');
    obj.frmDetalle.validate().resetForm();
};

CosteoDetalle.prototype.editarDetalle = function (detalleId) {
    var obj = this;

    $.ajax({
        url: obj.url + 'costeos/obtenerDetalle/' + detalleId,
        method: 'get',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                // load data
                obj.frmDetalle.find('.form-group').removeClass('has-error');
                obj.frmDetalle.validate().resetForm();

                obj.inptCosteoDetalleId.val(data.result.costeoDetalleId);
                obj.slcFormula.val(data.result.formula.formulaId).trigger('change');
                obj.slcFormula.attr("disabled", "disabled");
                obj.inptCantidadProducida.val(data.result.formula.formulaCantidadProducida);
                obj.inptFactorFlete.val(data.result.costeoDetalleFactorFlete);
                obj.accion = 2;

                // show modal
                obj.modalDetalle.modal({
                    backdrop: 'static',
                    keyboard: false
                });
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al obtener Detalle, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

CosteoDetalle.prototype.guardarDetalle = function () {
    var obj = this;

    var request = {
        costeoDetalleId: obj.inptCosteoDetalleId.val(),
        formula: {
            formulaId: obj.slcFormula.val()
        },
        costeoDetalleCantidad: obj.inptCantidadProducida.val(),
        costeoDetalleFlete: 0.00,
        costeoDetalleFactorFlete: obj.inptFactorFlete.val(),
        costeo: {
            costeoId: obj.inptCosteoId.val()
        },
        accion: obj.accion
    };

    $.ajax({
        url: obj.url + 'costeos/guardarDetalle',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadTblDetalles();
                obj.modalDetalle.modal('hide');
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al guardar Detalle, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

CosteoDetalle.prototype.eliminarDetalle = function (detalleId, formulaId) {
    var obj = this;

    var request = {
        costeoDetalleId: detalleId,
        formula: {
            formulaId: formulaId
        },
        costeo: {
            costeoId: obj.inptCosteoId.val()
        },
        accion: 3
    };

    $.ajax({
        url: obj.url + 'costeos/eliminarDetalle',
        method: 'delete',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadTblDetalles();
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al eliminar Detalle, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

CosteoDetalle.prototype.actualizarCosteo = function () {
    var obj = this;

    var request = {
        costeoId: obj.inptCosteoId.val(),
        costeoFechaTipoCambio: moment(obj.inptFechaTipoCambio.val(), 'DD/MM/YYYY'),
        costeoTipoCambio: obj.inptTipoCambio.val(),
        costeoPrecioFleteViaje: obj.inptPrecioFleteViaje.val(),
        accion: 2
    };

    $.ajax({
        url: obj.url + 'costeos/guardarCosteo',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadTblDetalles();
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al guardar Costeo, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

CosteoDetalle.prototype.generarFactura = function () {
    var obj = this;

    var request = {
        costeoId: obj.inptCosteoId.val(),
        costeoFacturaFechaEmision: moment(obj.inptFacturaFechaEmision.val(), 'DD/MM/YYYY'),
        costeoFacturaSerie: obj.inptFacturaSerie.val().toUpperCase(),
        costeoFacturaNumero: obj.inptFacturaNumero.val().toUpperCase(),
        costeoFacturaGuiaRemision: obj.inptFacturaGuiaRemision.val().toUpperCase(),
        accion: 4
    };

    $.ajax({
        url: obj.url + 'costeos/generarFactura',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                // actualizar campos de factura
                obj.inptFactura.val(obj.inptFacturaSerie.val() + '-' + obj.inptFacturaNumero.val());
                obj.inptFechaEmision.val(obj.inptFacturaFechaEmision.val());
                obj.inptGuiaRemision.val(obj.inptFacturaGuiaRemision.val());

                obj.divFactura.removeClass('hide');
                obj.btnFactura.addClass('hide');

                // mostrar mensaje
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');

                // cerrar modal
                obj.modalFactura.modal('hide');
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al generar Factura, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};
