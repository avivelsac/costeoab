var urlMain;

function PlantillaLista(url) {
    urlMain = url;
    this.url = url;

    this.slcFiltroEmpresa = $('#slc-filtro-empresa');
    this.inptCodigo = $('#inpt-codigo');
    this.inptDescripcion = $('#inpt-descripcion');
    this.btnBuscar = $('#btn-buscar');
    this.btnNuevo = $('#btn-nuevo');
    this.tblPlantillas = $('#tbl-plantillas');

    this.frmNuevo = $('#frm-nuevo');
    this.modalNuevo = $('#modal-nuevo');
    this.inptPlantillaCodigo = $('#inpt-plantilla-codigo');
    this.inptPlantillaDescripcion = $('#inpt-plantilla-descripcion');
    this.slcPlantillaEmpresa = $('#slc-plantilla-empresa');
    this.inptPlantillaObservaciones = $('#inpt-plantilla-observaciones');
    this.btnGuardar = $('#btn-guardar');

    this.modalClonar = $('#modal-clonar');
    this.frmClonar = $('#frm-clonar');
    this.inptClonarFormulaParentId = $('#inpt-clonar-formula-parent-id');
    this.inptClonarCodigo = $('#inpt-clonar-codigo');
    this.inptClonarDescripcion = $('#inpt-clonar-descripcion');
    this.slcClonarEmpresa = $('#slc-clonar-empresa');
    this.inptClonarObservaciones = $('#inpt-clonar-observaciones');
    this.btnGuardarClonar = $('#btn-guardar-clonar');
}

PlantillaLista.prototype.init = function () {
    this.listarPlantillas();
    this.validarFrmNuevo();
    this.validarFrmClonar();
    this.handler();
};

PlantillaLista.prototype.handler = function () {
    var obj = this;

    obj.btnBuscar.on('click', function (e) {
        e.preventDefault();

        obj.reloadPlantillas();
    });

    obj.btnNuevo.on('click', function (e) {
        e.preventDefault();

        obj.nuevaPlantilla();

        obj.modalNuevo.modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    obj.btnGuardar.on('click', function (e) {
        e.preventDefault();

        if (obj.frmNuevo.valid()) {
            obj.guardarPlantilla();
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos','error','Aceptar');
        }
    });

    obj.tblPlantillas.on('click', '.btn-eliminar', function (e) {
        e.preventDefault();

        var data = obj.tblPlantillas.DataTable().row($(this).parents('tr')).data();
        Custom.showAlertSiNo(
            'Eliminar',
            '¿Est\u00E1 seguro de eliminar la Plantilla con C\u00F3digo \"' + data.formulaCodigo + '\"?',
            'question',
            'Si',
            'No',
            function (result) {
                if (result.value) {
                    obj.eliminarPlantilla(data.formulaId);
                }
            });
    });

    obj.tblPlantillas.on('click', '.btn-clonar', function (e) {
        e.preventDefault();

        var data = obj.tblPlantillas.DataTable().row($(this).parents('tr')).data();
        obj.cargarPlantilla(data.formulaId);
    });

    obj.btnGuardarClonar.on('click', function (e) {
        e.preventDefault();

        if (obj.frmClonar.valid()) {
            obj.clonarFormula();
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos','error','Aceptar');
        }
    });
};

PlantillaLista.prototype.validarFrmNuevo = function () {
    Custom.validateForm(this.frmNuevo, {
        data: {
            rules: {
                inptPlantillaCodigo: {
                    required: true
                },
                inptPlantillaDescripcion: {
                    required: true
                },
                slcPlantillaEmpresa: {
                    required: true,
                    minlength: 1
                }
            },
            messages: {
                inptPlantillaCodigo: {
                    required: 'Campo obligatorio'
                },
                inptPlantillaDescripcion: {
                    required: 'Campo obligatorio'
                },
                slcPlantillaEmpresa: {
                    required: 'Campo obligatorio',
                    minlength: 'Campo obligatorio'
                }
            }
        }
    });
};

PlantillaLista.prototype.validarFrmClonar = function () {
    Custom.validateForm(this.frmClonar, {
        data: {
            rules: {
                inptClonarCodigo: {
                    required: true
                },
                slcClonarEmpresa: {
                    required: true,
                    minlength: 1
                },
                inptClonarDescripcion: {
                    required: true
                },
                inptClonarObservaciones: {
                    required: false
                }
            },
            messages: {
                inptClonarCodigo: {
                    required: 'Campo obligatorio'
                },
                slcClonarEmpresa: {
                    required: 'Campo obligatorio',
                    minlength: 'Campo obligatorio'
                },
                inptClonarDescripcion: {
                    required: 'Campo obligatorio'
                },
                inptClonarObservaciones: {
                    required: 'Campo obligatorio'
                }
            }
        }
    })
};

PlantillaLista.prototype.reloadPlantillas = function () {
    this.tblPlantillas.DataTable().ajax.reload();
};

PlantillaLista.prototype.listarPlantillas = function () {
    var obj = this;

    obj.tblPlantillas.DataTable({
        dom: "<''>"+
            "t"+
            "r"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        responsive: true,
        language: {
            search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            emptyTable: "No hay datos que mostrar.",
            processing: "Procesando...",
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No se encontraron resultados",
            info: "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoEmpty:      "Mostrando del 0 al 0 de 0 registros",
            loadingRecords: "Cargando...",
            paginate: {
                first: "<<",
                last: ">>",
                next: ">",
                previous: "<"
            }
        },
        sort: false,
        serverSide : true,
        processing: true,
        lengthMenu: [[5, 10, 20], [5, 10, 20]],
        displayLength: 15,
        ajax: {
            url: this.url + 'plantillas/listar',
            type: "post",
            data: function (d) {
                d.empresaId = obj.slcFiltroEmpresa.val();
                d.codigo = obj.inptCodigo.val();
                d.descripcion = obj.inptDescripcion.val();
            }
        },
        columns: [
            {'title': 'Empresa', 'data': 'empresa.empresaRazonSocial', 'orderable': false},
            {'title': 'C&oacute;digo', 'data': 'formulaCodigo', 'orderable': false},
            {'title': 'Descripci&oacute;n', 'data': 'formulaDescripcion', 'orderable': false},
            {'title': 'Observaciones', 'data': 'formulaObservaciones', 'orderable': false},
            {
                'data': null,
                'class': '',
                'orderable': false,
                'render': function(data, type, full) {
                    var r = '';
                    r += '<a href="' + urlMain + 'plantillas/detalle/' + data.formulaId + '" class="btn btn-warning btn-xs btn-editar m-r-xs">';
                    r += '<i class="fa fa-pencil"></i> Editar';
                    r += '</a>';

                    /*r += '<button type="button" class="btn btn-danger btn-xs btn-eliminar m-r-xs" data-toggle="tooltip" data-placement="top" title="Eliminar">';
                    r += '<i class="fa fa-trash"></i> ';
                    r += '</button>';

                    r += '<button type="button" class="btn btn-info btn-xs btn-clonar m-r-xs" data-toggle="tooltip" data-placement="top" title="Clonar">';
                    r += '<i class="fa fa-copy"></i> ';
                    r += '</button>';*/

                    return r;
                }
            }
        ]
    });
};

PlantillaLista.prototype.nuevaPlantilla = function () {
    var obj = this;

    obj.inptPlantillaCodigo.val('');
    obj.inptPlantillaDescripcion.val('');
    obj.inptPlantillaObservaciones.val('');
    obj.slcPlantillaEmpresa.val('').trigger('change');

    obj.frmNuevo.find('.form-group').removeClass('has-error');
    obj.frmNuevo.validate().resetForm();
};

PlantillaLista.prototype.guardarPlantilla = function () {
    var obj = this;

    var request = {
        formulaCodigo: obj.inptPlantillaCodigo.val().trim().toUpperCase(),
        formulaDescripcion: obj.inptPlantillaDescripcion.val().trim().toUpperCase(),
        empresa: {
            empresaId: obj.slcPlantillaEmpresa.val()
        },
        formulaObservaciones: obj.inptPlantillaObservaciones.val().trim().toUpperCase(),
        accion: 1
    };

    $.ajax({
        url: obj.url + 'plantillas/guardarPlantilla',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadPlantillas();
                obj.modalNuevo.modal('hide');
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al guardar Plantilla, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

PlantillaLista.prototype.eliminarPlantilla = function (plantillaId) {
    var obj = this;

    var request = {
        formulaId: plantillaId,
        accion: 3
    };

    $.ajax({
        url: obj.url + 'plantillas/eliminarPlantilla',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadPlantillas();
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al eliminar Plantilla, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

PlantillaLista.prototype.cargarPlantilla = function (plantillaId) {
    var obj = this;

    $.ajax({
        url: obj.url + 'plantillas/obtenerPlantilla/' + plantillaId,
        method: 'get',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {

                // load data
                obj.frmClonar.find('.form-group').removeClass('has-error');
                obj.frmClonar.validate().resetForm();

                obj.inptClonarCodigo.val(data.result.formulaCodigo);
                obj.slcClonarEmpresa.val(data.result.empresa.empresaId);
                obj.inptClonarDescripcion.val(data.result.formulaDescripcion);
                obj.inptClonarObservaciones.val(data.result.formulaObservaciones);
                obj.inptClonarFormulaParentId.val(plantillaId);

                // show modal
                obj.modalClonar.modal({
                    backdrop: 'static',
                    keyboard: false
                });
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al obtener Plantilla, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

PlantillaLista.prototype.clonarFormula = function () {
    var obj = this;

    var request = {
        formulaCodigo: obj.inptClonarCodigo.val().toUpperCase(),
        formulaDescripcion: obj.inptClonarDescripcion.val().toUpperCase(),
        empresa: {
            empresaId: obj.slcClonarEmpresa.val()
        },
        formulaObservaciones: obj.inptClonarObservaciones.val().toUpperCase(),
        formulaParentId: obj.inptClonarFormulaParentId.val(),
        formulaTipo: 'P',
        accion: 1
    };

    $.ajax({
        url: obj.url + 'plantillas/clonarPlantilla',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadPlantillas();
                obj.modalClonar.modal('hide');
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al clonar Plantilla, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};