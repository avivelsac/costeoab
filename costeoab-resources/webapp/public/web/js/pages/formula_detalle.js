var urlMain;

function FormulaDetalle(url, formulaEstado) {
    urlMain = url;
    this.url = url;
    this.formulaEstado = formulaEstado;
    this.accion = 0;

    this.inptFormulaId = $('#inpt-formula-id');
    this.frmFormula = $('#frm-formula');
    this.inptFormulaAnio = $('#inpt-formula-anio');
    this.inptFormulaSemana = $('#inpt-formula-semana');
    this.inptFormulaCantidadProducida = $('#inpt-formula-cantidad_producida');
    this.btnGuardarFormula = $('#btn-guardar-formula');

    this.tblDetalles = $('#tbl-detalles');
    this.btnAgregarDetalle = $('#btn-agregar-detalle');

    this.modalDetalle = $('#modal-detalle');
    this.frmDetalle = $('#frm-detalle');
    this.inptFormulaDetalleId = $('#inpt-formula-detalle-id');
    this.inptArticuloCodigo = $('#inpt-articulo-codigo');
    this.btnBuscarArticulo = $('#btn-buscar-articulo');
    this.inptArticuloId = $('#inpt-articulo-id');
    this.inptArticuloCodigoSap = $('#inpt-articulo-codigo-sap');
    this.inptArticuloDescripcion = $('#inpt-articulo-descripcion');
    this.inptArticuloUnidadMedidaId = $('#inpt-articulo-unidad-medida-id');
    this.inptArticuloUnidadMedida = $('#inpt-articulo-unidad-medida');
    this.inptArticuloObservacion = $('#inpt-articulo-observacion');
    this.inptArticuloPrecio = $('#inpt-articulo-precio');
    this.inptArticuloFlete = $('#inpt-articulo-flete');
    this.inptCantidad = $('#inpt-cantidad');
    this.inptObservaciones = $('#inpt-observaciones');
    this.btnGuardarDetalle = $('#btn-guardar-detalle');

    this.modalBuscarArticulos = $('#modal-buscar-articulos');
    this.inptModalCodigo2 = $('#inpt-modal-codigo2');
    this.inptModalCodigo = $('#inpt-modal-codigo');
    this.inptModalDescripcion = $('#inpt-modal-descripcion');
    this.btnModalBuscar = $('#btn-modal-buscar');
    this.tblModalArticulos = $('#tbl-modal-articulos');

    this.modalPrecios = $('#modal-precios');
    this.frmPrecios = $('#frm-precios');
    this.inptPrecioDetalleId = $('#inpt-precio-detalle-id');
    this.inptPrecioArticuloId = $('#inpt-precio-articulo-id');
    this.inptPrecioArticuloDescripcion = $('#inpt-precio-articulo-descripcion');
    this.inptPrecioUnidadMedida = $('#inpt-precio-unidad-medida');
    this.inptPrecio = $('#inpt-precio');
    this.inptPrecioFlete = $('#inpt-precio-flete');
    this.btnGuardarPrecios = $('#btn-guardar-precios');
}

FormulaDetalle.prototype.init = function () {
    this.listarTblDetalles();
    this.listarTblArticulos();
    this.validarFrmDetalle();
    this.validarFrmFormula();
    this.validarFrmPrecios();
    this.handler();
};

FormulaDetalle.prototype.handler = function () {
    var obj = this;

    obj.btnAgregarDetalle.on('click', function (e) {
        e.preventDefault();

        obj.nuevoDetalle();

        obj.modalDetalle.modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    obj.btnBuscarArticulo.on('click', function (e) {
        e.preventDefault();

        obj.modalBuscarArticulos.modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    obj.btnModalBuscar.on('click', function (e) {
        e.preventDefault();

        obj.reloadTblArticulos();
    });

    obj.tblModalArticulos.on('click', '.btn-seleccionar', function (e) {
        e.preventDefault();

        var data = obj.tblModalArticulos.DataTable().row($(this).parents('tr')).data();

        obj.inptArticuloId.val(data.articuloId);
        obj.inptArticuloCodigo.val(data.articuloCodigo2);
        obj.inptArticuloCodigoSap.val(data.articuloCodigo);
        obj.inptArticuloDescripcion.val(data.articuloDescripcion);
        obj.inptArticuloUnidadMedidaId.val(data.unidadMedida.unidadMedidaId);
        obj.inptArticuloUnidadMedida.val(data.unidadMedida.unidadMedidaDescripcion);
        obj.inptArticuloObservacion.val(data.articuloObservacion);
        obj.inptArticuloPrecio.val(data.precio.precioValor);
        obj.inptArticuloFlete.val(data.precio.precioFleteValor);

        obj.frmDetalle.valid();
        obj.modalBuscarArticulos.modal('hide');
    });

    obj.btnGuardarDetalle.on('click', function (e) {
        e.preventDefault();

        if (obj.frmDetalle.valid()) {
            obj.guardarDetalle();
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos del Detalle','error','Aceptar');
        }
    });

    obj.tblDetalles.on('click', '.btn-editar', function (e) {
        e.preventDefault();

        var data = obj.tblDetalles.DataTable().row($(this).parents('tr')).data();
        obj.editarDetalle(data.formulaDetalleId);
    });

    obj.tblDetalles.on('click', '.btn-eliminar', function (e) {
        e.preventDefault();

        var data = obj.tblDetalles.DataTable().row($(this).parents('tr')).data();
        Custom.showAlertSiNo(
            'Eliminar',
            '¿Est\u00E1 seguro de eliminar el Detalle con C\u00F3digo de Art\u00EDculo \"' + data.articulo.articuloCodigo + '\"?',
            'question',
            'Si',
            'No',
            function (result) {
                if (result.value) {
                    obj.eliminarDetalle(data.formulaDetalleId);
                }
            });
    });

    obj.tblDetalles.on('click', '.btn-precios', function (e) {
        e.preventDefault();

        var data = obj.tblDetalles.DataTable().row($(this).parents('tr')).data();

        obj.editarPreciosArticulo(data.formulaDetalleId, data.articulo.articuloId);

        obj.modalPrecios.modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    obj.btnGuardarFormula.on('click', function (e) {
        e.preventDefault();

        if (obj.frmFormula.valid()) {
            obj.actualizarFormula();
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos','error','Aceptar');
        }
    });

    obj.btnGuardarPrecios.on('click', function (e) {
        e.preventDefault();

        if (obj.frmPrecios.valid()) {
            obj.actualizarPrecios();
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos','error','Aceptar');
        }
    });
};

FormulaDetalle.prototype.validarFrmFormula = function () {
    Custom.validateForm(this.frmFormula, {
        data: {
            rules: {
                inptFormulaCantidadProducida: {
                    required: true,
                    number: true
                }
            },
            messages: {
                inptFormulaCantidadProducida: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                }
            }
        }
    })
};

FormulaDetalle.prototype.validarFrmDetalle = function () {
    Custom.validateForm(this.frmDetalle, {
        data: {
            rules: {
                inptArticuloId: {
                    required: true
                },
                inptCantidad: {
                    required: true,
                    number: true
                },
                inptArticuloPrecio: {
                    required: true,
                    number: true
                },
                inptArticuloFlete: {
                    required: true,
                    number: true
                }
            },
            messages: {
                inptArticuloId: {
                    required: 'Campo obligatorio'
                },
                inptCantidad: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                },
                inptArticuloPrecio: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                },
                inptArticuloFlete: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                }
            }
        }
    });
};

FormulaDetalle.prototype.validarFrmPrecios = function () {
    Custom.validateForm(this.frmPrecios, {
        data: {
            rules: {
                inptPrecioArticuloId: {
                    required: true
                },
                inptPrecio: {
                    required: true,
                    number: true
                },
                inptPrecioFlete: {
                    required: true,
                    number: true
                }
            },
            messages: {
                inptArticuloId: {
                    required: 'Campo obligatorio'
                },
                inptPrecio: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                },
                inptPrecioFlete: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                }
            }
        }
    });
};

FormulaDetalle.prototype.reloadTblDetalles = function () {
    this.tblDetalles.DataTable().ajax.reload();
};

FormulaDetalle.prototype.listarTblDetalles = function () {
    var obj = this;

    obj.tblDetalles.DataTable({
        dom: "<''>"+
            "t"+
            "r"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        responsive: false,
        language: {
            search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            emptyTable: "No hay datos que mostrar.",
            processing: "Procesando...",
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No se encontraron resultados",
            info: "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoEmpty:      "Mostrando del 0 al 0 de 0 registros",
            loadingRecords: "Cargando...",
            paginate: {
                first: "<<",
                last: ">>",
                next: ">",
                previous: "<"
            }
        },
        sort: false,
        serverSide : true,
        processing: true,
        displayLength: 100,
        ajax: {
            url: this.url + 'formulas/listarDetalles',
            type: "post",
            data: function (d) {
                d.formulaId = obj.inptFormulaId.val();
            }
        },
        columns: [
            {
                'title': '#',
                'data': null,
                'orderable': false,
                "render": function (data, type, full, meta) {
                    return  meta.row + 1;
                }
            },
            {'title': 'Art&iacute;culo/Insumo', 'data': 'articulo.articuloDescripcion', 'orderable': false},
            {'title': 'Cantidad (KG)', 'class': 'text-right', 'data': 'formulaDetalleCantidad', 'orderable': false, 'render': obj._formatNumber},
            {'title': 'Precio x T', 'class': 'text-right', 'data': 'formulaDetallePrecio', 'orderable': false, 'render': obj._formatNumber},
            {'title': 'Precio x Flete', 'class': 'text-right', 'data': 'formulaDetalleFlete', 'orderable': false, 'render': obj._formatNumber},
            {'title': 'Observaciones', 'data': 'formulaDetalleObservaciones', 'orderable': false},
            {
                'title': '',
                'data': null,
                'class': obj.formulaEstado === 1 ? '' : 'hide',
                'orderable': false,
                'render': function(data, type, full) {
                    var r = '';
                    r += '<button type="button" class="btn btn-warning btn-xs btn-editar m-r-xs">';
                    r += '<i class="fa fa-pencil"></i> Editar';
                    r += '</button>';

                    r += '<button type="button" class="btn btn-danger btn-xs btn-eliminar m-r-xs" data-toggle="tooltip" data-placement="top" title="Eliminar">';
                    r += '<i class="fa fa-trash"></i> ';
                    r += '</button>';

                    r += '<button type="button" class="btn btn-success btn-xs btn-precios m-r-xs" data-toggle="tooltip" data-placement="top" title="Actualizar Precios del Art&iacute;culo/Insumo">';
                    r += '<i class="fa fa-dollar"></i> ';
                    r += '</button>';

                    return r;
                }
            }
        ],
        "footerCallback": function(tfoot, data, start, end, display) {
            $(this).find('tfoot').html('');

            var api = this.api();

            var totalDefault = 1000;
            var totalKg = api.column(2).data().reduce(function (a, b) {
                return a + b;
            }, 0);

            var diferencia = totalDefault - totalKg.toFixed(3);

            var footer = '<tfoot>';
                footer += '<tr>';
                footer += '<th class="text-right">Batch(kg)</th>';
                footer += '<th class="text-right">' + obj._formatNumber(totalKg) + '</th>';
                footer += '<th colspan="5"></th>';
                footer += '</tr>';

                footer += '<tr>';
                footer += '<th class="text-right">Diferencia</th>';
                footer += '<th class="text-right">' + obj._formatNumber(diferencia) + '</th>';
                footer += '<th colspan="5"></th>';
                footer += '</tr>';
                footer += '</tfoot>';

            $(this).append(footer);
        }
    });
};

FormulaDetalle.prototype._formatNumber = function (data) {
    return accounting.formatMoney(data, "", 3, ",", ".");
};

FormulaDetalle.prototype.reloadTblArticulos = function () {
    this.tblModalArticulos.DataTable().ajax.reload();
};

FormulaDetalle.prototype.listarTblArticulos = function () {
    var obj = this;

    obj.tblModalArticulos.DataTable({
        dom: "<''>"+
            "t"+
            "r"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        responsive: false,
        language: {
            search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            emptyTable: "No hay datos que mostrar.",
            processing: "Procesando...",
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No se encontraron resultados",
            info: "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoEmpty:      "Mostrando del 0 al 0 de 0 registros",
            loadingRecords: "Cargando...",
            paginate: {
                first: "<<",
                last: ">>",
                next: ">",
                previous: "<"
            }
        },
        info: false,
        sort: false,
        serverSide : true,
        processing: true,
        displayLength: 15,
        ajax: {
            url: this.url + 'articulos/listar',
            type: "post",
            data: function (d) {
                d.codigo = obj.inptModalCodigo.val();
                d.codigo2 = obj.inptModalCodigo2.val();
                d.descripcion = obj.inptModalDescripcion.val();
            }
        },
        columns: [
            {'title': 'C&oacute;digo', 'data': 'articuloCodigo2', 'orderable': false},
            {'title': 'C&oacute;digo SAP', 'data': 'articuloCodigo', 'orderable': false},
            {'title': 'Descripci&oacute;n', 'data': 'articuloDescripcion', 'orderable': false},
            {'title': 'Unid. Med.', 'data': 'unidadMedida.unidadMedidaDescripcion', 'orderable': false},
            {'title': 'Precio ($)', 'class': 'text-right', 'data': 'precio.precioValor', 'orderable': false},
            {'title': 'P. Flete ($)', 'class': 'text-right', 'data': 'precio.precioFleteValor', 'orderable': false},
            {
                'data': null,
                'class': '',
                'orderable': false,
                'render': function(data, type, full) {
                    var r = '<button type="button" class="btn btn-warning btn-xs btn-seleccionar">';
                    r += '<i class="fa fa-check"></i> Seleccionar';
                    r += '</button>';
                    return r;
                }
            }
        ]
    });
};

FormulaDetalle.prototype.guardarDetalle = function () {
    var obj = this;

    var request = {
        formulaDetalleId: obj.inptFormulaDetalleId.val(),
        articulo: {
            articuloId: obj.inptArticuloId.val()
        },
        formulaDetalleCantidad: obj.inptCantidad.val(),
        formulaDetallePrecio: obj.inptArticuloPrecio.val(),
        formulaDetalleFlete: obj.inptArticuloFlete.val(),
        formulaDetalleObservaciones: obj.inptObservaciones.val().toUpperCase(),
        formula: {
            formulaId: obj.inptFormulaId.val()
        },
        accion: obj.accion
    };

    $.ajax({
        url: obj.url + 'formulas/guardarDetalle',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadTblDetalles();
                obj.modalDetalle.modal('hide');
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al guardar Detalle, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

FormulaDetalle.prototype.nuevoDetalle = function () {
    var obj = this;

    obj.inptFormulaDetalleId.val('');
    obj.inptArticuloId.val('');
    obj.inptArticuloCodigo.val('');
    obj.inptArticuloCodigoSap.val('');
    obj.inptArticuloDescripcion.val('');
    obj.inptArticuloUnidadMedidaId.val('');
    obj.inptArticuloUnidadMedida.val('');
    obj.inptArticuloObservacion.val('');
    obj.inptArticuloPrecio.val('');
    obj.inptArticuloFlete.val('');
    obj.inptObservaciones.val('');
    obj.inptCantidad.val('');
    obj.accion = 1;

    obj.frmDetalle.find('.form-group').removeClass('has-error');
    obj.frmDetalle.validate().resetForm();
};

FormulaDetalle.prototype.editarDetalle = function (detalleId) {
    var obj = this;

    $.ajax({
        url: obj.url + 'formulas/obtenerDetalle/' + detalleId,
        method: 'get',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                // load data
                obj.frmDetalle.find('.form-group').removeClass('has-error');
                obj.frmDetalle.validate().resetForm();

                obj.inptFormulaDetalleId.val(data.result.formulaDetalleId);
                obj.inptArticuloId.val(data.result.articulo.articuloId);
                obj.inptArticuloCodigo.val(data.result.articulo.articuloCodigo2);
                obj.inptArticuloCodigoSap.val(data.result.articulo.articuloCodigo);
                obj.inptArticuloDescripcion.val(data.result.articulo.articuloDescripcion);
                obj.inptArticuloUnidadMedidaId.val(data.result.articulo.unidadMedida.unidadMedidaId);
                obj.inptArticuloUnidadMedida.val(data.result.articulo.unidadMedida.unidadMedidaDescripcion);
                obj.inptArticuloObservacion.val(data.result.articulo.articuloObservacion);
                obj.inptArticuloPrecio.val(data.result.formulaDetallePrecio);
                obj.inptArticuloFlete.val(data.result.formulaDetalleFlete);
                obj.inptCantidad.val(data.result.formulaDetalleCantidad);
                obj.inptObservaciones.val(data.result.formulaDetalleObservaciones);
                obj.accion = 2;

                // show modal
                obj.modalDetalle.modal({
                    backdrop: 'static',
                    keyboard: false
                });
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al obtener Detalle, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

FormulaDetalle.prototype.editarPreciosArticulo = function (detalleId, articuloId) {
    var obj = this;

    $.ajax({
        url: obj.url + 'articulos/obtenerArticulo/' + articuloId,
        method: 'get',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                // load data
                obj.frmPrecios.find('.form-group').removeClass('has-error');
                obj.frmPrecios.validate().resetForm();

                obj.inptPrecioDetalleId.val(detalleId);
                obj.inptPrecioArticuloId.val(data.result.articuloId);
                obj.inptPrecioArticuloDescripcion.val(data.result.articuloDescripcion);
                obj.inptPrecioUnidadMedida.val(data.result.unidadMedida.unidadMedidaDescripcion);
                obj.inptPrecio.val(data.result.precio.precioValor);
                obj.inptPrecioFlete.val(data.result.precio.precioFleteValor);

                // show modal
                obj.modalPrecios.modal({
                    backdrop: 'static',
                    keyboard: false
                });
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al obtener Articulo, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

FormulaDetalle.prototype.eliminarDetalle = function (formulaDetalleId) {
    var obj = this;

    var request = {
        formulaDetalleId: formulaDetalleId,
        accion: 3
    };

    $.ajax({
        url: obj.url + 'formulas/eliminarDetalle',
        method: 'delete',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadTblDetalles();
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al eliminar Detalle, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

FormulaDetalle.prototype.actualizarFormula = function () {
    var obj = this;

    var request = {
        formulaId: obj.inptFormulaId.val(),
        formulaCantidadProducida: obj.inptFormulaCantidadProducida.val(),
        accion: 2
    };

    $.ajax({
        url: obj.url + 'formulas/actualizarFormula',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al actualizar F\u00F3rmula, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

FormulaDetalle.prototype.actualizarPrecios = function () {
    var obj = this;

    var request = {
        formulaDetalleId: obj.inptPrecioDetalleId.val(),
        articulo: {
            articuloId: obj.inptPrecioArticuloId.val(),
            precio: {
                precioValor: obj.inptPrecio.val(),
                precioFleteValor: obj.inptPrecioFlete.val(),
                precioActual: true,
                articulo: {
                    articuloId: obj.inptPrecioArticuloId.val()
                },
                accion: 1
            }
        },
        formulaDetallePrecio: obj.inptPrecio.val(),
        formulaDetalleFlete: obj.inptPrecioFlete.val(),
        formula: {
            formulaAnio: obj.inptFormulaAnio.val(),
            formulaSemana: obj.inptFormulaSemana.val()
        },
        accion: 5
    };

    $.ajax({
        url: obj.url + 'formulas/actualizarDetallePrecios',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadTblDetalles();
                obj.modalPrecios.modal('hide');
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al actualizar Precios, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};