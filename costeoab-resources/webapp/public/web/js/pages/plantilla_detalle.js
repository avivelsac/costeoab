var urlMain;

function PlantillaDetalle(url) {
    urlMain = url;
    this.url = url;
    this.accion = 0;

    this.inptPlantillaId = $('#inpt-plantilla-id');

    this.tblDetalles = $('#tbl-detalles');
    this.btnAgregarDetalle = $('#btn-agregar-detalle');

    this.modalDetalle = $('#modal-detalle');
    this.frmDetalle = $('#frm-detalle');
    this.inptPlantillaDetalleId = $('#inpt-plantilla-detalle-id');
    this.inptArticuloCodigo = $('#inpt-articulo-codigo');
    this.btnBuscarArticulo = $('#btn-buscar-articulo');
    this.inptArticuloId = $('#inpt-articulo-id');
    this.inptArticuloCodigoSap = $('#inpt-articulo-codigo-sap');
    this.inptArticuloDescripcion = $('#inpt-articulo-descripcion');
    this.inptArticuloUnidadMedidaId = $('#inpt-articulo-unidad-medida-id');
    this.inptArticuloUnidadMedida = $('#inpt-articulo-unidad-medida');
    this.inptArticuloObservacion = $('#inpt-articulo-observacion');
    this.inptCantidad = $('#inpt-cantidad');
    this.inptObservaciones = $('#inpt-observaciones');
    this.btnGuardarDetalle = $('#btn-guardar-detalle');

    this.modalBuscarArticulos = $('#modal-buscar-articulos');
    this.inptModalCodigo2 = $('#inpt-modal-codigo2');
    this.inptModalCodigo = $('#inpt-modal-codigo');
    this.inptModalDescripcion = $('#inpt-modal-descripcion');
    this.btnModalBuscar = $('#btn-modal-buscar');
    this.tblModalArticulos = $('#tbl-modal-articulos');
}

PlantillaDetalle.prototype.init = function () {
    this.listarTblDetalles();
    this.listarTblArticulos();
    this.validarFrmDetalle();
    this.handler();
};

PlantillaDetalle.prototype.handler = function () {
    var obj = this;

    obj.btnAgregarDetalle.on('click', function (e) {
        e.preventDefault();

        obj.nuevoDetalle();

        obj.modalDetalle.modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    obj.btnBuscarArticulo.on('click', function (e) {
        e.preventDefault();

        obj.modalBuscarArticulos.modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    obj.btnModalBuscar.on('click', function (e) {
        e.preventDefault();

        obj.reloadTblArticulos();
    });

    obj.tblModalArticulos.on('click', '.btn-seleccionar', function (e) {
        e.preventDefault();

        var data = obj.tblModalArticulos.DataTable().row($(this).parents('tr')).data();

        obj.inptArticuloId.val(data.articuloId);
        obj.inptArticuloCodigo.val(data.articuloCodigo2);
        obj.inptArticuloCodigoSap.val(data.articuloCodigo);
        obj.inptArticuloDescripcion.val(data.articuloDescripcion);
        obj.inptArticuloUnidadMedidaId.val(data.unidadMedida.unidadMedidaId);
        obj.inptArticuloUnidadMedida.val(data.unidadMedida.unidadMedidaDescripcion);
        obj.inptArticuloObservacion.val(data.articuloObservacion);

        obj.frmDetalle.valid();
        obj.modalBuscarArticulos.modal('hide');
    });

    obj.btnGuardarDetalle.on('click', function (e) {
        e.preventDefault();

        if (obj.frmDetalle.valid()) {
            obj.guardarDetalle();
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos del Detalle','error','Aceptar');
        }
    });

    obj.tblDetalles.on('click', '.btn-editar', function (e) {
        e.preventDefault();

        var data = obj.tblDetalles.DataTable().row($(this).parents('tr')).data();
        obj.editarDetalle(data.formulaDetalleId);
    });

    obj.tblDetalles.on('click', '.btn-eliminar', function (e) {
        e.preventDefault();

        var data = obj.tblDetalles.DataTable().row($(this).parents('tr')).data();
        Custom.showAlertSiNo(
            'Eliminar',
            '¿Est\u00E1 seguro de eliminar el Detalle con C\u00F3digo de Art\u00EDculo \"' + data.articulo.articuloCodigo + '\"?',
            'question',
            'Si',
            'No',
            function (result) {
                if (result.value) {
                    obj.eliminarDetalle(data.formulaDetalleId);
                }
            });
    });
};

PlantillaDetalle.prototype.validarFrmDetalle = function () {
    Custom.validateForm(this.frmDetalle, {
        data: {
            rules: {
                inptArticuloId: {
                    required: true
                },
                inptCantidad: {
                    required: true,
                    number: true
                }
            },
            messages: {
                inptArticuloId: {
                    required: 'Campo obligatorio'
                },
                inptCantidad: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                }
            }
        }
    });
};

PlantillaDetalle.prototype.reloadTblDetalles = function () {
    this.tblDetalles.DataTable().ajax.reload();
};

PlantillaDetalle.prototype.listarTblDetalles = function () {
    var obj = this;

    obj.tblDetalles.DataTable({
        dom: "<''>"+
            "t"+
            "r"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        responsive: false,
        language: {
            search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            emptyTable: "No hay datos que mostrar.",
            processing: "Procesando...",
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No se encontraron resultados",
            info: "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoEmpty:      "Mostrando del 0 al 0 de 0 registros",
            loadingRecords: "Cargando...",
            paginate: {
                first: "<<",
                last: ">>",
                next: ">",
                previous: "<"
            }
        },
        sort: false,
        serverSide : true,
        processing: true,
        displayLength: 100,
        ajax: {
            url: this.url + 'plantillas/listarDetalles',
            type: "post",
            data: function (d) {
                d.formulaId = obj.inptPlantillaId.val();
            }
        },
        columns: [
            {
                'title': '#',
                'width': '10px',
                'data': null,
                'orderable': false,
                "render": function (data, type, full, meta) {
                    return  meta.row + 1;
                }
            },
            {'title': 'Art&iacute;culo/Insumo', 'data': 'articulo.articuloDescripcion', 'orderable': false},
            {'title': 'Cantidad (KG)', 'class': 'text-right', 'data': 'formulaDetalleCantidad', 'orderable': false, 'render': obj._formatNumber},
            // {'title': 'Unid. Med.', 'data': 'articulo.unidadMedida.unidadMedidaDescripcion', 'orderable': false},
            {'title': 'Observaciones', 'data': 'formulaDetalleObservaciones', 'orderable': false},
            {
                'title': '',
                'data': null,
                'class': '',
                'orderable': false,
                'render': function(data, type, full) {
                    var r = '';
                    r += '<button type="button" class="btn btn-warning btn-xs btn-editar m-r-xs">';
                    r += '<i class="fa fa-pencil"></i> Editar';
                    r += '</button>';

                    r += '<button type="button" class="btn btn-danger btn-xs btn-eliminar m-r-xs" data-toggle="tooltip" data-placement="top" title="Eliminar">';
                    r += '<i class="fa fa-trash"></i> ';
                    r += '</button>';

                    return r;
                }
            }
        ],
        "footerCallback": function(tfoot, data, start, end, display) {
            $(this).find('tfoot').html('');

            var api = this.api();

            var totalDefault = 1000.000;
            var totalKg = api.column(2).data().reduce(function (a, b) {
                    return a + b;
            }, 0);
            var diferencia = totalDefault - totalKg.toFixed(3);

            var footer = '';
                footer += '<tfoot>';

                footer += '<tr>';
                footer += '<th></th>';
                footer += '<th class="text-right">Batch(kg)</th>';
                footer += '<th class="text-right">' + obj._formatNumber(totalKg) + '</th>';
                footer += '<th colspan="2"></th>';
                footer += '</tr>';

                footer += '<tr>';
                footer += '<th></th>';
                footer += '<th class="text-right">Diferencia</th>';
                footer += '<th class="text-right">' + obj._formatNumber(diferencia) + '</th>';
                footer += '<th colspan="2"></th>';
                footer += '</tr>';

                footer += '</tfoot>';

            $(this).append(footer);
        }
    });
};

PlantillaDetalle.prototype._formatNumber = function (data) {
    return accounting.formatMoney(data, "", 3, ",", ".");
};

PlantillaDetalle.prototype.reloadTblArticulos = function () {
    this.tblModalArticulos.DataTable().ajax.reload();
};

PlantillaDetalle.prototype.listarTblArticulos = function () {
    var obj = this;

    obj.tblModalArticulos.DataTable({
        dom: "<''>"+
            "t"+
            "r"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        responsive: false,
        language: {
            search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            emptyTable: "No hay datos que mostrar.",
            processing: "Procesando...",
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No se encontraron resultados",
            info: "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoEmpty:      "Mostrando del 0 al 0 de 0 registros",
            loadingRecords: "Cargando...",
            paginate: {
                first: "<<",
                last: ">>",
                next: ">",
                previous: "<"
            }
        },
        info: false,
        sort: false,
        serverSide : true,
        processing: true,
        displayLength: 15,
        ajax: {
            url: this.url + 'articulos/listar',
            type: "post",
            data: function (d) {
                d.codigo = obj.inptModalCodigo.val();
                d.codigo2 = obj.inptModalCodigo2.val();
                d.descripcion = obj.inptModalDescripcion.val();
            }
        },
        columns: [
            {'title': 'C&oacute;digo', 'data': 'articuloCodigo2', 'orderable': false},
            {'title': 'C&oacute;digo SAP', 'data': 'articuloCodigo', 'orderable': false},
            {'title': 'Descripci&oacute;n', 'data': 'articuloDescripcion', 'orderable': false},
            {'title': 'Unid. Med.', 'data': 'unidadMedida.unidadMedidaDescripcion', 'orderable': false},
            {'title': 'Precio ($)', 'class': 'text-right', 'data': 'precio.precioValor', 'orderable': false},
            {'title': 'P. Flete ($)', 'class': 'text-right', 'data': 'precio.precioFleteValor', 'orderable': false},
            {
                'data': null,
                'class': '',
                'orderable': false,
                'render': function(data, type, full) {
                    var r = '<button type="button" class="btn btn-warning btn-xs btn-seleccionar">';
                    r += '<i class="fa fa-check"></i> Seleccionar';
                    r += '</button>';
                    return r;
                }
            }
        ]
    });
};

PlantillaDetalle.prototype.guardarDetalle = function () {
    var obj = this;

    var request = {
        formulaDetalleId: obj.inptPlantillaDetalleId.val(),
        articulo: {
            articuloId: obj.inptArticuloId.val()
        },
        formulaDetalleCantidad: obj.inptCantidad.val(),
        formulaDetalleObservaciones: obj.inptObservaciones.val().toUpperCase(),
        formula: {
            formulaId: obj.inptPlantillaId.val()
        },
        accion: obj.accion
    };

    $.ajax({
        url: obj.url + 'plantillas/guardarDetalle',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadTblDetalles();
                obj.modalDetalle.modal('hide');
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al guardar Detalle, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

PlantillaDetalle.prototype.nuevoDetalle = function () {
    var obj = this;

    obj.inptPlantillaDetalleId.val('');
    obj.inptArticuloId.val('');
    obj.inptArticuloCodigo.val('');
    obj.inptArticuloCodigoSap.val('');
    obj.inptArticuloDescripcion.val('');
    obj.inptArticuloUnidadMedidaId.val('');
    obj.inptArticuloUnidadMedida.val('');
    obj.inptArticuloObservacion.val('');
    obj.inptObservaciones.val('');
    obj.inptCantidad.val('');
    obj.accion = 1;

    obj.frmDetalle.find('.form-group').removeClass('has-error');
    obj.frmDetalle.validate().resetForm();
};

PlantillaDetalle.prototype.editarDetalle = function (detalleId) {
    var obj = this;

    $.ajax({
        url: obj.url + 'plantillas/obtenerDetalle/' + detalleId,
        method: 'get',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                // load data
                obj.frmDetalle.find('.form-group').removeClass('has-error');
                obj.frmDetalle.validate().resetForm();

                obj.inptPlantillaDetalleId.val(data.result.formulaDetalleId);
                obj.inptArticuloId.val(data.result.articulo.articuloId);
                obj.inptArticuloCodigo.val(data.result.articulo.articuloCodigo2);
                obj.inptArticuloCodigoSap.val(data.result.articulo.articuloCodigo);
                obj.inptArticuloDescripcion.val(data.result.articulo.articuloDescripcion);
                obj.inptArticuloUnidadMedidaId.val(data.result.articulo.unidadMedida.unidadMedidaId);
                obj.inptArticuloUnidadMedida.val(data.result.articulo.unidadMedida.unidadMedidaDescripcion);
                obj.inptArticuloObservacion.val(data.result.articulo.articuloObservacion);
                obj.inptObservaciones.val(data.result.formulaDetalleObservaciones);
                obj.inptCantidad.val(data.result.formulaDetalleCantidad);
                obj.accion = 2;

                // show modal
                obj.modalDetalle.modal({
                    backdrop: 'static',
                    keyboard: false
                });
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al obtener Detalle, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

PlantillaDetalle.prototype.eliminarDetalle = function (formulaDetalleId) {
    var obj = this;

    var request = {
        formulaDetalleId: formulaDetalleId,
        accion: 3
    };

    $.ajax({
        url: obj.url + 'plantillas/eliminarDetalle',
        method: 'delete',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadTblDetalles();
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al eliminar Detalle, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};
