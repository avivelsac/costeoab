var urlMain;

function FormulaLista(url) {
    urlMain = url;
    this.url = url;

    this.slcFiltroAnio = $('#slc-filtro-anio');
    this.inptFiltroSemana = $('#inpt-filtro-semana');
    this.slcFiltroEmpresa = $('#slc-filtro-empresa');
    this.inptCodigo = $('#inpt-codigo');
    this.inptDescripcion = $('#inpt-descripcion');
    this.btnBuscar = $('#btn-buscar');
    this.btnNuevo = $('#btn-nuevo');
    this.tblFormulas = $('#tbl-formulas');

    this.modalNuevo = $('#modal-nuevo');
    this.frmNuevo = $('#frm-nuevo');
    this.slcPlantilla = $('#slc-plantilla');
    this.linkPlantilla = $('#link-plantilla');
    this.slcAnio = $('#slc-anio');
    this.inptSemana = $('#inpt-semana');
    this.inptPlantillaCodigo = $('#inpt-plantilla-codigo');
    this.inptPlantillaEmpresa = $('#inpt-plantilla-empresa');
    this.slcPlantillaEmpresa = $('#slc-plantilla-empresa');
    this.inptPlantillaDescripcion = $('#inpt-plantilla-descripcion');
    this.inptPlantillaObservaciones = $('#inpt-plantilla-observaciones');
    this.btnGuardar = $('#btn-guardar');

    this.modalClonar = $('#modal-clonar');
    this.frmClonar = $('#frm-clonar');
    this.inptClonarFormulaParentId = $('#inpt-clonar-formula-parent-id');
    this.inptClonarCodigo = $('#inpt-clonar-codigo');
    this.inptClonarEmpresa = $('#inpt-clonar-empresa');
    this.slcClonarEmpresa = $('#slc-clonar-empresa');
    this.inptClonarDescripcion = $('#inpt-clonar-descripcion');
    this.inptClonarObservaciones = $('#inpt-clonar-observaciones');
    this.slcClonarAnio = $('#slc-clonar-anio');
    this.inptClonarSemana = $('#inpt-clonar-semana');
    this.btnGuardarClonar = $('#btn-guardar-clonar');
}

FormulaLista.prototype.init = function () {
    this.listarFormulas();
    this.validarFrmNuevo();
    this.validarFrmClonar();
    this.handler();
};

FormulaLista.prototype.handler = function () {
    var obj = this;

    obj.btnBuscar.on('click', function (e) {
        e.preventDefault();

        obj.reloadFormulas();
    });

    obj.btnNuevo.on('click', function (e) {
        e.preventDefault();

        obj.nuevaFormula();

        obj.modalNuevo.modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    obj.slcPlantilla.on('change', function (e) {
        e.preventDefault();

        var plantillaId = $(this).val();

        if (plantillaId === '') {
            obj.linkPlantilla.attr('href', '');
            obj.linkPlantilla.addClass('hide');
            obj.inptPlantillaCodigo.val('');
            obj.slcPlantillaEmpresa.val('').trigger('change');
            obj.inptPlantillaDescripcion.val('');
            obj.inptPlantillaObservaciones.val('');
        } else {
            obj.obtenerPlantilla(plantillaId);
        }
    });

    obj.btnGuardar.on('click', function (e) {
        e.preventDefault();

        if (obj.frmNuevo.valid()) {
            obj.guardarFormula();
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos','error','Aceptar');
        }
    });

    obj.tblFormulas.on('click', '.btn-eliminar', function (e) {
        e.preventDefault();

        var data = obj.tblFormulas.DataTable().row($(this).parents('tr')).data();
        Custom.showAlertSiNo(
            'Eliminar',
            '¿Est\u00E1 seguro de eliminar la F\u00F3rmula con C\u00F3digo \"' + data.formulaCodigo + '\"?',
            'question',
            'Si',
            'No',
            function (result) {
                if (result.value) {
                    obj.eliminarFormula(data.formulaId);
                }
            });
    });

    obj.tblFormulas.on('click', '.btn-clonar', function (e) {
        e.preventDefault();

        var data = obj.tblFormulas.DataTable().row($(this).parents('tr')).data();
        obj.cargarFormula(data.formulaId);
    });

    obj.btnGuardarClonar.on('click', function (e) {
        e.preventDefault();

        if (obj.frmClonar.valid()) {
            obj.clonarFormula();
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos','error','Aceptar');
        }
    });
};

FormulaLista.prototype.validarFrmNuevo = function () {
    Custom.validateForm(this.frmNuevo, {
        data: {
            rules: {
                slcPlantilla: {
                    required: true,
                    minlength: 1
                },
                inptPlantillaCodigo: {
                    required: true
                },
                slcPlantillaEmpresa: {
                    required: true,
                    minlength: 1
                },
                inptPlantillaDescripcion: {
                    required: true
                },
                inptPlantillaObservaciones: {
                    required: false
                },
                slcAnio: {
                    required: true,
                    minlength: 1
                },
                inptSemana: {
                    required: true,
                    number: true
                }
            },
            messages: {
                slcPlantilla: {
                    required: 'Campo obligatorio',
                    minlength: 'Campo obligatorio'
                },
                inptPlantillaCodigo: {
                    required: 'Campo obligatorio'
                },
                slcPlantillaEmpresa: {
                    required: 'Campo obligatorio',
                    minlength: 'Campo obligatorio'
                },
                inptPlantillaDescripcion: {
                    required: 'Campo obligatorio'
                },
                inptPlantillaObservaciones: {
                    required: 'Campo obligatorio'
                },
                slcAnio: {
                    required: 'Campo obligatorio',
                    minlength: 'Campo obligatorio'
                },
                inptSemana: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                }
            }
        }
    })
};

FormulaLista.prototype.validarFrmClonar = function () {
    Custom.validateForm(this.frmClonar, {
        data: {
            rules: {
                inptClonarCodigo: {
                    required: true
                },
                slcClonarEmpresa: {
                    required: true,
                    minlength: 1
                },
                inptClonarDescripcion: {
                    required: true
                },
                inptClonarObservaciones: {
                    required: false
                },
                slcAnio: {
                    required: true,
                    minlength: 1
                },
                inptSemana: {
                    required: true,
                    number: true
                }
            },
            messages: {
                inptClonarCodigo: {
                    required: 'Campo obligatorio'
                },
                slcClonarEmpresa: {
                    required: 'Campo obligatorio',
                    minlength: 'Campo obligatorio'
                },
                inptClonarDescripcion: {
                    required: 'Campo obligatorio'
                },
                inptClonarObservaciones: {
                    required: 'Campo obligatorio'
                },
                slcAnio: {
                    required: 'Campo obligatorio',
                    minlength: 'Campo obligatorio'
                },
                inptSemana: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                }
            }
        }
    })
};

FormulaLista.prototype.reloadFormulas = function () {
    this.tblFormulas.DataTable().ajax.reload();
};

FormulaLista.prototype.obtenerPlantilla = function (plantillaId) {
    var obj = this;

    $.ajax({
        url: obj.url + 'formulas/obtenerPlantilla/' + plantillaId,
        method: 'get',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                obj.linkPlantilla.attr('href', obj.url + 'plantillas/detalle/' + data.result.formulaId);
                obj.linkPlantilla.removeClass('hide');
                obj.inptPlantillaCodigo.val(data.result.formulaCodigo);
                obj.slcPlantillaEmpresa.val(data.result.empresa.empresaId);
                obj.inptPlantillaDescripcion.val(data.result.formulaDescripcion);
                obj.inptPlantillaObservaciones.val(data.result.formulaObservaciones);

                obj.frmNuevo.valid();
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al obtener Plantilla, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

FormulaLista.prototype.listarFormulas = function () {
    var obj = this;

    obj.tblFormulas.DataTable({
        dom: "<''>"+
            "t"+
            "r"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        responsive: true,
        language: {
            search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            emptyTable: "No hay datos que mostrar.",
            processing: "Procesando...",
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No se encontraron resultados",
            info: "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoEmpty:      "Mostrando del 0 al 0 de 0 registros",
            loadingRecords: "Cargando...",
            paginate: {
                first: "<<",
                last: ">>",
                next: ">",
                previous: "<"
            }
        },
        sort: false,
        serverSide : true,
        processing: true,
        lengthMenu: [[5, 10, 20], [5, 10, 20]],
        displayLength: 15,
        ajax: {
            url: this.url + 'formulas/listar',
            type: "post",
            data: function (d) {
                d.anio = obj.slcFiltroAnio.val();
                d.semana = obj.inptFiltroSemana.val();
                d.empresaId = obj.slcFiltroEmpresa.val();
                d.codigo = obj.inptCodigo.val();
                d.descripcion = obj.inptDescripcion.val();
            }
        },
        columns: [
            {'title': 'A&ntilde;o', 'data': 'formulaAnio', 'orderable': false},
            {'title': 'Semana', 'data': 'formulaSemana', 'orderable': false},
            {'title': 'Empresa', 'data': 'empresa.empresaRazonSocial', 'orderable': false},
            {'title': 'C&oacute;digo', 'data': 'formulaCodigo', 'orderable': false},
            {'title': 'Descripci&oacute;n', 'data': 'formulaDescripcion', 'orderable': false},
            {'title': 'Observaciones', 'data': 'formulaObservaciones', 'orderable': false},
            {
                'data': null,
                'class': '',
                'width': '130',
                'orderable': false,
                'render': function(data, type, full) {
                    var r = '';
                    r += '<a href="' + urlMain + 'formulas/detalle/' + data.formulaId + '" class="btn btn-warning btn-xs btn-editar m-r-xs">';
                    r += '<i class="fa fa-pencil"></i> Editar';
                    r += '</a>';

                    r += '<button type="button" class="btn btn-danger btn-xs btn-eliminar m-r-xs" data-toggle="tooltip" data-placement="top" title="Eliminar">';
                    r += '<i class="fa fa-trash"></i> ';
                    r += '</button>';

                    r += '<button type="button" class="btn btn-info btn-xs btn-clonar m-r-xs" data-toggle="tooltip" data-placement="top" title="Clonar">';
                    r += '<i class="fa fa-copy"></i> ';
                    r += '</button>';
                    return r;
                }
            }
        ]
    });
};

FormulaLista.prototype.nuevaFormula = function () {
    var obj = this;

    obj.slcPlantilla.val('').trigger('change');
    obj.inptPlantillaCodigo.val('');
    obj.slcPlantillaEmpresa.val('');
    obj.inptPlantillaDescripcion.val('');
    obj.inptPlantillaObservaciones.val('');
    obj.slcAnio.val('').trigger('change');
    obj.inptSemana.val('');

    obj.frmNuevo.find('.form-group').removeClass('has-error');
    obj.frmNuevo.validate().resetForm();
};

FormulaLista.prototype.cargarFormula = function (formulaId) {
    var obj = this;

    $.ajax({
        url: obj.url + 'formulas/obtenerFormula/' + formulaId,
        method: 'get',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {

                // load data
                obj.frmClonar.find('.form-group').removeClass('has-error');
                obj.frmClonar.validate().resetForm();

                obj.inptClonarCodigo.val(data.result.formulaCodigo);
                obj.slcClonarEmpresa.val(data.result.empresa.empresaId);
                obj.inptClonarDescripcion.val(data.result.formulaDescripcion);
                obj.inptClonarObservaciones.val(data.result.formulaObservaciones);
                obj.slcClonarAnio.val(data.result.formulaAnio);
                obj.inptClonarSemana.val(data.result.formulaSemana);
                obj.inptClonarFormulaParentId.val(formulaId);

                // show modal
                obj.modalClonar.modal({
                    backdrop: 'static',
                    keyboard: false
                });
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al obtener F\u00F3rmula, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

FormulaLista.prototype.guardarFormula = function () {
    var obj = this;

    var request = {
        formulaCodigo: obj.inptPlantillaCodigo.val().toUpperCase(),
        formulaDescripcion: obj.inptPlantillaDescripcion.val().toUpperCase(),
        empresa: {
            empresaId: obj.slcPlantillaEmpresa.val()
        },
        formulaObservaciones: obj.inptPlantillaObservaciones.val().toUpperCase(),
        formulaAnio: obj.slcAnio.val(),
        formulaSemana: obj.inptSemana.val(),
        formulaParentId: obj.slcPlantilla.val(),
        accion: 1
    };

    $.ajax({
        url: obj.url + 'formulas/guardarFormula',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadFormulas();
                obj.modalNuevo.modal('hide');
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al guardar F\u00F3rmula, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

FormulaLista.prototype.eliminarFormula = function (formulaId) {
    var obj = this;

    var request = {
        formulaId: formulaId,
        accion: 3
    };

    $.ajax({
        url: obj.url + 'formulas/eliminarFormula',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadFormulas();
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al eliminar F\u00F3rmula, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

FormulaLista.prototype.clonarFormula = function () {
    var obj = this;

    var request = {
        formulaCodigo: obj.inptClonarCodigo.val().toUpperCase(),
        formulaDescripcion: obj.inptClonarDescripcion.val().toUpperCase(),
        empresa: {
            empresaId: obj.slcClonarEmpresa.val()
        },
        formulaObservaciones: obj.inptClonarObservaciones.val().toUpperCase(),
        formulaAnio: obj.slcClonarAnio.val(),
        formulaSemana: obj.inptClonarSemana.val(),
        formulaParentId: obj.inptClonarFormulaParentId.val(),
        formulaTipo: 'F',
        accion: 1
    };

    $.ajax({
        url: obj.url + 'formulas/clonarFormula',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadFormulas();
                obj.modalClonar.modal('hide');
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al clonar F\u00F3rmula, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};