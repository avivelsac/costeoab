var urlMain;

function CosteoLista(url) {
    urlMain = url;
    this.url = url;

    this.slcFiltroEmpresa = $('#slc-filtro-empresa');
    this.slcFiltroAnio = $('#slc-filtro-anio');
    this.inptFiltroSemana = $('#inpt-filtro-semana');
    this.btnBuscar = $('#btn-buscar');
    this.btnNuevo = $('#btn-nuevo');
    this.tblCosteos = $('#tbl-costeos');

    this.modalNuevo = $('#modal-nuevo');
    this.frmNuevo = $('#frm-nuevo');
    this.inptEnvioId = $('#inpt-envio-id');
    this.slcEmpresa = $('#slc-empresa');
    this.slcAnio = $('#slc-anio');
    this.inptSemana = $('#inpt-semana');
    this.inptObservaciones = $('#inpt-observaciones');
    this.btnGuardar = $('#btn-guardar');

}

CosteoLista.prototype.init = function () {
    this.listarCosteos();
    this.validarFrmNuevo();
    this.handler();
};

CosteoLista.prototype.handler = function () {
    var obj = this;

    obj.btnBuscar.on('click', function (e) {
        e.preventDefault();

        obj.reloadTblCosteos();
    });

    obj.btnNuevo.on('click', function (e) {
        e.preventDefault();

        obj.nuevoCosteo();

        obj.modalNuevo.modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    obj.btnGuardar.on('click', function (e) {
        e.preventDefault();

        if (obj.frmNuevo.valid()) {
            obj.guardarCosteo();
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos ','error','Aceptar');
        }
    });

    obj.tblCosteos.on('click', '.btn-eliminar', function (e) {
        e.preventDefault();

        var data = obj.tblCosteos.DataTable().row($(this).parents('tr')).data();
        Custom.showAlertSiNo(
            'Eliminar',
            '¿Est\u00E1 seguro de eliminar el Costeo con A\u00F1o \"' + data.costeoAnio + '\" y Semana \"' + data.costeoSemana + '\"?',
            'question',
            'Si',
            'No',
            function (result) {
                if (result.value) {
                    obj.eliminarCosteo(data.costeoId);
                }
            });
    });

};

CosteoLista.prototype.validarFrmNuevo = function () {
    Custom.validateForm(this.frmNuevo, {
        data: {
            rules: {
                inptEnvioId: {
                    required: true
                },
                slcEmpresa: {
                    required: true,
                    minlength: 1
                },
                slcAnio: {
                    required: true,
                    minlength: 1
                },
                inptSemana: {
                    required: true,
                    number: true
                },
                inptObservaciones: {
                    required: false
                }
            },
            messages: {
                inptEnvioId: {
                    required: 'Campo obligatorio'
                },
                slcEmpresa: {
                    required: 'Campo obligatorio',
                    minlength: 'Campo obligatorio'
                },
                slcAnio: {
                    required: 'Campo obligatorio',
                    minlength: 'Campo obligatorio'
                },
                inptSemana: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                },
                inptObservaciones: {
                    required: 'Campo obligatorio'
                }
            }
        }
    })
};

CosteoLista.prototype.reloadTblCosteos = function () {
    this.tblCosteos.DataTable().ajax.reload();
};

CosteoLista.prototype.listarCosteos = function () {
    var obj = this;

    obj.tblCosteos.DataTable({
        dom: "<''>"+
            "t"+
            "r"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        responsive: true,
        language: {
            search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            emptyTable: "No hay datos que mostrar.",
            processing: "Procesando...",
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No se encontraron resultados",
            info: "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoEmpty:      "Mostrando del 0 al 0 de 0 registros",
            loadingRecords: "Cargando...",
            paginate: {
                first: "<<",
                last: ">>",
                next: ">",
                previous: "<"
            }
        },
        sort: false,
        serverSide : true,
        processing: true,
        lengthMenu: [[5, 10, 20], [5, 10, 20]],
        displayLength: 15,
        ajax: {
            url: this.url + 'costeos/listar',
            type: "post",
            data: function (d) {
                d.empresaId = obj.slcFiltroEmpresa.val();
                d.anio = obj.slcFiltroAnio.val();
                d.semana = obj.inptFiltroSemana.val();
            }
        },
        columns: [
            {'title': 'Empresa', 'data': 'empresa.empresaRazonSocial', 'orderable': false},
            {'title': 'A&ntilde;o', 'data': 'costeoAnio', 'orderable': false},
            {'title': 'Semana', 'data': 'costeoSemana', 'orderable': false},
            {'title': 'Observaciones', 'data': 'costeoObservaciones', 'orderable': false},
            {
                'data': null,
                'class': '',
                'orderable': false,
                'render': function(data, type, full) {
                    var r = '';
                        r += '<a href="' + urlMain + 'costeos/detalle/' + data.costeoId + '" class="btn btn-warning btn-xs m-r-xs">';
                        r += '<i class="fa fa-pencil"></i> Detalle';
                        r += '</a>';

                        r += '<button type="button" class="btn btn-danger btn-xs btn-eliminar m-r-xs">';
                        r += '<i class="fa fa-trash"></i> Eliminar';
                        r += '</button>';

                    return r;
                }
            }
        ]
    });
};

CosteoLista.prototype.nuevoCosteo = function () {
    var obj = this;

    obj.slcEmpresa.val('').trigger('change');
    obj.slcAnio.val('').trigger('change');
    obj.inptSemana.val('');
    obj.inptObservaciones.val('');

    obj.frmNuevo.find('.form-group').removeClass('has-error');
    obj.frmNuevo.validate().resetForm();
};

CosteoLista.prototype.guardarCosteo = function () {
    var obj = this;

    var request = {
        empresa: {
            empresaId: obj.slcEmpresa.val()
        },
        costeoAnio: obj.slcAnio.val(),
        costeoSemana: obj.inptSemana.val(),
        costeoObservaciones: obj.inptObservaciones.val().toUpperCase(),
        accion: 1
    };

    $.ajax({
        url: obj.url + 'costeos/guardarCosteo',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadTblCosteos();
                obj.modalNuevo.modal('hide');
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al eliminar el Costeo, contacte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

CosteoLista.prototype.eliminarCosteo = function (costeoId) {
    var obj = this;

    var request = {
        costeoId: costeoId,
        accion: 3
    };

    $.ajax({
        url: obj.url + 'costeos/eliminarCosteo',
        method: 'delete',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadTblCosteos();
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al eliminar el Costeo, contacte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};