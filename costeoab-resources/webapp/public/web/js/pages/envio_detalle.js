var urlMain;

function EnvioDetalle(url) {
    urlMain = url;
    this.url = url;

    this.inptEnvioId =$('#inpt-envio-id');
    this.tblDetalles = $('#tbl-detalles');

    this.btnCosteo = $('#btn-costeo');
    this.modalGenerarCosteo = $('#modal-generar-costeo');
    this.frmGenerarCosteo = $('#frm-generar-costeo');
    this.inptFechaTipoCambio = $('#inpt-fecha-tipo-cambio');
    this.inptTipoCambio = $('#inpt-tipo-cambio');
    this.inptPrecioFleteViaje = $('#inpt-precio-flete-viaje');
    this.btnGenerarCosteo = $('#btn-generar-costeo');

    this.tblAdjuntos = $('#tbl-adjuntos');
    this.btnAgregarAdjunto = $('#btn-agregar-adjunto');
    this.modalAgregarAdjunto = $('#modal-agregar-adjunto');
    this.frmAdjunto = $('#frm-adjunto');
    this.inptFileArchivo = $('#inpt-file-archivo');
    this.btnGuardarAdjunto = $('#btn-guardar-adjunto');

    this.modalPrevisualizarAdjunto = $('#modal-previsualizar-adjunto');
    this.nombreAdjuntoServer = '';
    this.nombreAdjunto = '';
    this.btnDescargarAdjunto = $('#btn-descargar-adjunto');
    this.divPreviewModal = $('#div-preview-modal');
}

EnvioDetalle.prototype.init = function () {
    this.listarTblDetalles();
    this.listarTblAdjuntos();
    this.validarFrmGenerarCosteo();
    this.validarFrmAdjunto();
    this.handler();
};

EnvioDetalle.prototype.handler = function () {
    var obj = this;

    obj.inptFechaTipoCambio.datepickerInFullscreen({
        todayWord: 'Hoy',
        clearWord: 'Limpiar',
        closeWord: 'Cerrar',
        datepicker: {
            language: 'es'
        },
        format: 'DD/MM/YYYY',
        fakeInputFormat: 'DD/MM/YYYY',
        fakeInput: false,
        touchSwipe: false,
        onChange: function(modal, settings) {
            obj.inptFechaTipoCambio.valid();
        }
    });

    obj.btnCosteo.on('click', function (e) {
        e.preventDefault();

        obj.modalGenerarCosteo.modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    obj.btnGenerarCosteo.on('click', function (e) {
        e.preventDefault();

        if (obj.frmGenerarCosteo.valid()) {
            Custom.showAlertSiNo(
                'Confirmar',
                '¿Est\u00e1 seguro de Generar el Costeo para el Env\u00EDo?',
                'question',
                'Si',
                'No',
                function (result) {
                    if (result.value) {
                        obj.generarCosteo();
                    }
                });
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos ','error','Aceptar');
        }

    });

    obj.btnAgregarAdjunto.on('click', function (e) {
        e.preventDefault();
        obj.modalAgregarAdjunto.modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    obj.btnGuardarAdjunto.on('click', function (e) {
        e.preventDefault();

        if (obj.frmAdjunto.valid()) {
            obj.guardarArchivoAdjunto();
        } else {
            Custom.showAlertAceptar('Error','Revisar campos','error','Aceptar');
        }
    });

    obj.tblAdjuntos.on('click', '.btn-eliminar', function (e) {
        e.preventDefault();

        var data = obj.tblAdjuntos.DataTable().row($(this).parents('tr')).data();
        Custom.showAlertSiNo(
            'Eliminar',
            '¿Est\u00E1 seguro de eliminar el adjunto \"' + data.envioAdjuntoNombre + '\"?',
            'question',
            'Si',
            'No',
            function (result) {
                if (result.value) {
                    obj.eliminarArchivoAdjunto(data.envioAdjuntoId);
                }
            });
    });

    obj.tblAdjuntos.on('click', '.btn-previsualizar', function (e) {
        e.preventDefault();

        var data = obj.tblAdjuntos.DataTable().row($(this).parents('tr')).data();

        obj.nombreAdjuntoServer = data.envioAdjuntoNombreServer;
        obj.nombreAdjunto = data.envioAdjuntoNombre;
        obj.cargarAdjunto(data.envioAdjuntoNombreServer, data.envioAdjuntoNombre, data.envioAdjuntoExtension, 2);

        obj.modalPrevisualizarAdjunto.modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    obj.tblAdjuntos.on('click', '.btn-descargar', function (e) {
        e.preventDefault();

        var data = obj.tblAdjuntos.DataTable().row($(this).parents('tr')).data();
        obj.cargarAdjunto(data.envioAdjuntoNombreServer, data.envioAdjuntoNombre, '', 1);
    });

    obj.btnDescargarAdjunto.on('click', function (e) {
        e.preventDefault();

        obj.cargarAdjunto(obj.nombreAdjuntoServer, obj.nombreAdjunto, '', 1);
    });
};

EnvioDetalle.prototype.validarFrmAdjunto = function() {
    Custom.validateForm(this.frmAdjunto, {
        data: {
            rules: {
                inptFileArchivo: {
                    required: true,
                    extension: 'png|jpe?g|gif|pdf',
                    filesize: 10485760
                }
            },
            messages: {
                inptFileArchivo: {
                    required: 'Campo obligatorio',
                    extension: 'Tipos de archivo permitidos: JPG, GIF, PNG, PDF',
                    filesize: 'Tamaño m&aacute;ximo: 10MB'
                }
            }
        }
    });
};

EnvioDetalle.prototype.validarFrmGenerarCosteo = function () {
    Custom.validateForm(this.frmGenerarCosteo, {
        data: {
            rules: {
                inptFechaTipoCambio: {
                    required: true
                },
                inpTipoCambio: {
                    required: true,
                    number: true
                },
                inptPrecioFleteViaje: {
                    required: true,
                    number: true
                }
            },
            messages: {
                inptFechaTipoCambio: {
                    required: 'Campo obligatorio'
                },
                inpTipoCambio: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                },
                inptPrecioFleteViaje: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                }
            }
        }
    })
};

EnvioDetalle.prototype.listarTblDetalles = function () {
    var obj = this;

    obj.tblDetalles.DataTable({
        dom: "<''>"+
            "t"+
            "r"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        responsive: false,
        language: {
            search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            emptyTable: "No hay datos que mostrar.",
            processing: "Procesando...",
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No se encontraron resultados",
            info: "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoEmpty:      "Mostrando del 0 al 0 de 0 registros",
            loadingRecords: "Cargando...",
            paginate: {
                first: "<<",
                last: ">>",
                next: ">",
                previous: "<"
            }
        },
        sort: false,
        serverSide : true,
        processing: true,
        displayLength: 100,
        ajax: {
            url: this.url + 'envios/listarDetalles',
            type: "post",
            data: function (d) {
                d.envioId = obj.inptEnvioId.val();
            }
        },
        columns: [
            {'title': 'F&oacute;rmula', 'data': 'formula.formulaDescripcion', 'orderable': false},
            {'title': 'Cantidad Producida (KG)', 'class': 'text-right', 'data': 'formula.formulaCantidadProducida', 'orderable': false, 'render': obj._formatNumber},
            {
                'data': null,
                'orderable': false,
                'render': function(data, type, full) {
                    return '<a href="' + urlMain + 'formulas/detalle/' + data.formula.formulaId + '" class="btn btn-info btn-xs" target="_blank">' +
                                '<i class="fa fa-external-link"></i> Ver F&oacute;rmula' +
                            '</a>';
                }
            }
        ],
        "footerCallback": function(tfoot, data, start, end, display) {
            $(this).find('tfoot').html('');

            var api = this.api();

            var sumaCantidad = api.column(1).data().reduce(function (a, b) {
                return a + b;
            }, 0);

            var footer = '<tfoot>';

            footer += '<tr>';
            footer += '<th class="text-right">Total (kg)</th>';
            footer += '<th class="text-right">' + obj._formatNumber(sumaCantidad) + '</th>';
            footer += '<th></th>';
            footer += '</tr>';

            footer += '</tfoot>';

            $(this).append(footer);
        }
    });
};

EnvioDetalle.prototype._formatNumber = function (data) {
    return accounting.formatMoney(data, "", 2, ",", ".");
};

EnvioDetalle.prototype.reloadTblAdjuntos = function() {
    this.tblAdjuntos.DataTable().ajax.reload();
};

EnvioDetalle.prototype.listarTblAdjuntos = function() {
    var obj = this;

    obj.tblAdjuntos.DataTable({
        dom: "<''>"+
            "t"+
            "r"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        responsive: false,
        language: {
            search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            emptyTable: "No hay datos que mostrar.",
            processing: "Procesando...",
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No se encontraron resultados",
            info: "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoEmpty:      "Mostrando del 0 al 0 de 0 registros",
            loadingRecords: "Cargando...",
            paginate: {
                first: "<<",
                last: ">>",
                next: ">",
                previous: "<"
            }
        },
        info: false,
        sort: false,
        serverSide : true,
        processing: true,
        displayLength: 15,
        ajax: {
            url: this.url + 'envios/listarAdjuntos',
            type: 'post',
            data: function (d) {
                d.envioId = obj.inptEnvioId.val();
            }
        },
        columns: [
            {'title': 'Nombre', 'data': 'envioAdjuntoNombre', 'orderable': false},
            {
                'title': '',
                'data': null,
                'orderable': false,
                'render': function(data, type, full) {
                    var r = '';

                    r += '<button type="button" class="btn btn-info btn-xs btn-previsualizar m-r-xs">';
                    r += '<i class="fa fa-external-link"></i> Previsualizar';
                    r += '</button>';

                    r += '<button type="button" class="btn btn-warning btn-xs btn-descargar m-r-xs">';
                    r += '<i class="fa fa-download"></i> Descargar';
                    r += '</button>';

                    r += '<button type="button" class="btn btn-danger btn-xs btn-eliminar m-r-xs">';
                    r += '<i class="fa fa-trash"></i> Eliminar';
                    r += '</button>';

                    return r;
                }
            }
        ]
    });
};

EnvioDetalle.prototype.generarCosteo = function () {
    var obj = this;

    var request = {
        envioId: obj.inptEnvioId.val(),
        costeo: {
            costeoFechaTipoCambio: moment(obj.inptFechaTipoCambio.val(), 'DD/MM/YYYY'),
            costeoTipoCambio: obj.inptTipoCambio.val(),
            costeoPrecioFleteViaje: obj.inptPrecioFleteViaje.val()
        },
        accion: 4
    };

    $.ajax({
        url: obj.url + 'envios/generarCosteo',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                obj.btnCosteo.addClass('hide');
                obj.modalGenerarCosteo.modal('hide');

                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Ir al Costeo...',
                    function (result) {
                        if (result.value) {
                            window.location = urlMain + 'costeos/detalle/' + data.result;
                        }
                    }
                );
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al generar Costeo, contacte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

EnvioDetalle.prototype.guardarArchivoAdjunto = function () {
    var obj = this;

    var formData = new FormData();
    formData.append('envioId', obj.inptEnvioId.val());
    formData.append('archivo', obj.inptFileArchivo.get(0).files[0]);

    $.ajax({
        url: obj.url + 'envios/guardarAdjunto',
        contentType: false,
        processData: false,
        cache: false,
        type: 'post',
        data: formData,
        beforeSend: function() {
            Custom.blockUI();
        },
        success: function(data) {
            Custom.unblockUI();
            if (data.estado) {
                obj.reloadTblAdjuntos();
                obj.modalAgregarAdjunto.modal('hide');
                Custom.showAlertAceptar('Exito', data.mensaje, 'success', 'Aceptar');
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function(xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al guardar Adjunto. Por favor contacte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

EnvioDetalle.prototype.eliminarArchivoAdjunto = function (adjuntoId) {
    var obj = this;

    var request = {
        envioAdjuntoId: adjuntoId,
        accion: 3
    };

    $.ajax({
        url: obj.url + 'envios/eliminarAdjunto',
        method: 'delete',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadTblAdjuntos();
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al eliminar Adjunto, contacte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};

EnvioDetalle.prototype.cargarAdjunto = function (nombreServer, nombre, formato, accion) {
    var obj = this;

    if (accion === 1) {
        var urlDescargar = encodeURI(obj.url + 'envios/descargarAdjunto?nombreServer=' + nombreServer + '&nombre=' + nombre);
        window.open(urlDescargar, '_blank');

    } else if (accion === 2) {
        var urlPrevisualizar = encodeURI(obj.url + 'envios/previsualizarAdjunto?nombreServer=' + nombreServer + '&formato=' + formato + '&nombre=' + nombre);

        if (formato === 'PNG' || formato === 'JPEG' || formato === 'JPG' || formato === 'GIF') {
            obj.divPreviewModal.text('');
            obj.divPreviewModal.prepend('<img class="img-responsive" src="' + urlPrevisualizar + '" alt="" />');

        } else if (formato === 'PDF') {
            PDFObject.embed(urlPrevisualizar, "#div-preview-modal", {
                id: 'div-preview-modal',
                fallbackLink: '<p>El archivo no se puede previsualizar. Clic en descargar para obtenerlo.</p>',
                height: "600px"
            });

        } else {
            obj.divPreviewModal.text('El archivo no se puede previsualizar. Clic en descargar para obtenerlo.');
        }
    }
};
