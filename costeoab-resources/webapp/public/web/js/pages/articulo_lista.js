var urlMain;

function ArticuloLista(url) {
    this.url = url;
    urlMain = url;

    this.tblArticulos = $('#tbl-articulos');
    this.btnBuscar = $('#btn-buscar');
    this.inptCodigo2 = $('#inpt-codigo2');
    this.inptCodigo = $('#inpt-codigo');
    this.inptDescripcion = $('#inpt-descripcion');

    this.modalPrecios = $('#modal-precios');
    this.frmPrecios = $('#frm-precios');
    this.inptArticuloId = $('#inpt-articulo-id');
    this.inptArticuloDescripcion = $('#inpt-articulo-descripcion');
    this.inptUnidadMedida = $('#inpt-unidad-medida');
    this.inptPrecio = $('#inpt-precio');
    this.inptPrecioFlete = $('#inpt-precio-flete');
    this.btnGuardarPrecios = $('#btn-guardar-precios');

}

ArticuloLista.prototype.init = function () {
    this.listarArticulos();
    this.validarFrmPrecios();
    this.handler();
};

ArticuloLista.prototype.handler = function () {
    var obj = this;

    obj.btnBuscar.on('click', function (e) {
        e.preventDefault();
        obj.reloadArticulos();
    });

    obj.tblArticulos.on('click', '.btn-asignar-precios', function (e) {
        e.preventDefault();

        var data = obj.tblArticulos.DataTable().row($(this).parents('tr')).data();
        obj.inptArticuloId.val(data.articuloId);
        obj.inptArticuloDescripcion.val(data.articuloDescripcion);
        obj.inptUnidadMedida.val(data.unidadMedida.unidadMedidaDescripcion);
        obj.inptPrecio.val(data.precio.precioValor);
        obj.inptPrecioFlete.val(data.precio.precioFleteValor);

        obj.modalPrecios.modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    obj.btnGuardarPrecios.on('click', function (e) {
        e.preventDefault();

        if (obj.frmPrecios.valid()) {
            obj.guardarPrecios();
        } else {
            Custom.showAlertAceptar('Error', 'Revisar campos','error','Aceptar');
        }
    });
};

ArticuloLista.prototype.validarFrmPrecios = function() {
    Custom.validateForm(this.frmPrecios, {
        data: {
            rules: {
                inptArticuloId: {
                    required: true
                },
                inptPrecio: {
                    required: true,
                    number: true
                },
                inptPrecioFlete: {
                    required: true,
                    number: true
                }
            },
            messages: {
                inptArticuloId: {
                    required: 'Campo obligatorio'
                },
                inptPrecio: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                },
                inptPrecioFlete: {
                    required: 'Campo obligatorio',
                    number: 'Solo numeros'
                }
            }
        }
    });
};

ArticuloLista.prototype.reloadArticulos = function () {
    this.tblArticulos.DataTable().ajax.reload();
};

ArticuloLista.prototype.listarArticulos = function () {
    var obj = this;

    obj.tblArticulos.DataTable({
        dom: "<''>"+
            "t"+
            "r"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        responsive: false,
        language: {
            search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            emptyTable: "No hay datos que mostrar.",
            processing: "Procesando...",
            lengthMenu: "Mostrar _MENU_ registros",
            zeroRecords: "No se encontraron resultados",
            info: "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoEmpty:      "Mostrando del 0 al 0 de 0 registros",
            loadingRecords: "Cargando...",
            paginate: {
                first: "<<",
                last: ">>",
                next: ">",
                previous: "<"
            }
        },
        sort: false,
        serverSide : true,
        processing: true,
        lengthMenu: [[5, 10, 20], [5, 10, 20]],
        displayLength: 15,
        ajax: {
            url: this.url + 'articulos/listar',
            type: "post",
            data: function (d) {
                d.codigo = obj.inptCodigo.val();
                d.codigo2 = obj.inptCodigo2.val();
                d.descripcion = obj.inptDescripcion.val();
            }
        },
        columns: [
            {'title': 'C&oacute;digo', 'data': 'articuloCodigo2', 'orderable': false},
            {'title': 'C&oacute;digo SAP', 'data': 'articuloCodigo', 'orderable': false},
            {'title': 'Descripci&oacute;n', 'data': 'articuloDescripcion', 'orderable': false},
            {'title': 'Unid. Med.', 'data': 'unidadMedida.unidadMedidaDescripcion', 'orderable': false},
            {'title': 'Precio ($)', 'class': 'text-right', 'data': 'precio.precioValor', 'orderable': false, 'render': obj._formatNumber},
            {'title': 'P. Flete ($)', 'class': 'text-right', 'data': 'precio.precioFleteValor', 'orderable': false, 'render': obj._formatNumber},
            {
                'data': null,
                'class': '',
                'orderable': false,
                'render': function(data, type, full) {
                    var r = '';
                        r += '<button type="button" class="btn btn-success btn-xs btn-asignar-precios m-r-xs">';
                        r += '<i class="fa fa-dollar"></i> Precios';
                        r += '</button>';

                        r += '<button type="button" class="btn btn-info btn-xs btn-historial hide" data-toggle="tooltip" data-placement="top" title="Historial de Precios">';
                        r += '<i class="fa fa-bar-chart-o"></i> ';
                        r += '</button>';
                    return r;
                }
            }
        ]
    });
};

ArticuloLista.prototype._formatNumber = function (data) {
    return accounting.formatMoney(data, "", 2, ",", ".");
};

ArticuloLista.prototype.guardarPrecios = function () {
    var obj = this;

    var request = {
        articulo: {
            articuloId: obj.inptArticuloId.val()
        },
        precioValor: obj.inptPrecio.val(),
        precioFleteValor: obj.inptPrecioFlete.val(),
        precioActual: true,
        accion: 1
    };

    $.ajax({
        url: obj.url + 'articulos/guardarPrecios',
        method: 'post',
        dataType: 'json',
        contentType : "application/json; charset=utf-8",
        data: JSON.stringify(request),
        beforeSend: function () {
            Custom.blockUI();
        },
        success: function (data) {
            Custom.unblockUI();
            if (data.estado) {
                Custom.showAlertAceptar('Exito', data.mensaje, 'success','Aceptar');
                obj.reloadArticulos();
                obj.modalPrecios.modal('hide');
            } else {
                Custom.showAlertAceptar('Error', data.mensaje,'error', 'Aceptar');
                console.log(data);
            }
        },
        error: function (xhr) {
            Custom.unblockUI();
            Custom.showAlertAceptar(
                'Error',
                'Error al guardar Precios, consulte con soporte.',
                'error',
                'Aceptar');
            console.log(xhr);
        }
    });
};