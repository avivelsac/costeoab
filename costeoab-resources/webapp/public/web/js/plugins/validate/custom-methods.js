$.validator.addMethod('filesize', function(value, element, param) {
    // param = size (in bytes)
    // element = element to validate (<input>)
    // value = value of the element (file name)
    return this.optional(element) || (element.files[0].size <= param)
}, 'File size must be less than {0}');
