$(document).ready(function() {
    $(document).on('hidden.bs.modal', '.modal', function () {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });
});

function Custom() {
}

Custom.blockUI = function () {
    this.showUIAlert = Swal.mixin({
        text: 'Cargando...',
        allowOutsideClick: false,
        showConfirmButton: false
    });
    this.showUIAlert.fire();
    this.showUIAlert.showLoading();
};

Custom.unblockUI = function () {
    this.showUIAlert.close();
};

Custom.showAlertAceptar = function (title, message, type, okButton, thenMethod) {
    Swal.fire({
        allowOutsideClick: false,
        title: title,
        text: message,
        type: type,
        confirmButtonColor: '#0e9aef',
        confirmButtonText: okButton
    }).then(thenMethod);
};

Custom.showAlertSiNo = function (title, message, type, okButton, cancelButton, thenMethod) {
    Swal.fire({
        title: title,
        text: message,
        type: type,
        showCancelButton: true,
        confirmButtonColor: '#0e9aef',
        cancelButtonColor: '#d4d4d4',
        confirmButtonText: okButton,
        cancelButtonText: cancelButton,
        allowOutsideClick: false
    }).then(thenMethod);
};

Custom.validateForm = function ($form, array, ignore) {
    ignore = $.trim(ignore).length > 0 ? ignore : [];
    $form.validate({
        ignore: ignore,
        rules: array.data.rules,
        messages: array.data.messages,
        highlight: function(element) {
            if (array.button) {
                $('.btn-group>button', $(element).parent()).removeClass('btn-success');
                $('.btn-group>button', $(element).parent()).addClass('btn-danger');
            } else {
                $(element).closest('.form-group,.input-group').addClass('has-error');
            }
        },
        unhighlight: function(element) {
            if (array.button) {
                $('.btn-group>button', $(element).parent()).removeClass('btn-danger');
                $('.btn-group>button', $(element).parent()).addClass('btn-success');
            } else {
                $(element).closest('.form-group,.input-group').removeClass('has-error');
            }

        },
        focusInvalid: false,
        invalidHandler: function(form, validator) {
            if (!validator.numberOfInvalids()) return;
            /*$('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top-120
            }, 700);*/
        },
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);
            }
        }
    });
};

