package pe.com.avivel.sistemas.costeoab.dao.molino.definition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoDefinition;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Costeo;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CosteoDaoDefinition extends DaoDefinition<Costeo> {

    private EmpresaDaoDefinition empresaDaoDefinition;
    private EnvioDaoDefinition envioDaoDefinition;

    @Autowired
    public CosteoDaoDefinition(EmpresaDaoDefinition empresaDaoDefinition, EnvioDaoDefinition envioDaoDefinition) {
        super(Costeo.class);
        this.empresaDaoDefinition = empresaDaoDefinition;
        this.envioDaoDefinition = envioDaoDefinition;
    }

    @Override
    public Costeo mapRow(ResultSet rs, int rowNumber) throws SQLException {
        Costeo costeo = super.mapRow(rs, rowNumber);
        costeo.setEmpresa(empresaDaoDefinition.mapRow(rs, rowNumber));
        costeo.setEnvio(envioDaoDefinition.mapRow(rs, rowNumber));
        return costeo;
    }
}
