package pe.com.avivel.sistemas.costeoab.dao.molino.definition;

import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoDefinition;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.EnvioAdjunto;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class EnvioAdjuntoDaoDefinition extends DaoDefinition<EnvioAdjunto> {

    public EnvioAdjuntoDaoDefinition() {
        super(EnvioAdjunto.class);
    }

    @Override
    public EnvioAdjunto mapRow(ResultSet rs, int rowNumber) throws SQLException {
        return super.mapRow(rs, rowNumber);
    }
}
