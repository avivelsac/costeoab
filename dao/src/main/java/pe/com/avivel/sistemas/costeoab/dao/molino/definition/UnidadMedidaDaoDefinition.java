package pe.com.avivel.sistemas.costeoab.dao.molino.definition;

import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoDefinition;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.UnidadMedida;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UnidadMedidaDaoDefinition extends DaoDefinition<UnidadMedida> {

    public UnidadMedidaDaoDefinition() {
        super(UnidadMedida.class);
    }

    @Override
    public UnidadMedida mapRow(ResultSet rs, int rowNumber) throws SQLException {
        return super.mapRow(rs, rowNumber);
    }
}
