package pe.com.avivel.sistemas.costeoab.dao.molino.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoBuilder;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoGenerator;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoParameter;
import pe.com.avivel.sistemas.costeoab.dao.molino.FormulaDetalleDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.definition.FormulaDetalleDaoDefinition;
import pe.com.avivel.sistemas.costeoab.dao.utils.ConstantesMolinoDAO;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.FormulaDetalle;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroFormulaDetalle;

import java.sql.Types;
import java.util.List;

@Repository
public class FormulaDetalleDaoImpl extends DaoGenerator implements FormulaDetalleDao {

    private FormulaDetalleDaoDefinition formulaDetalleDaoDefinition;

    @Autowired
    public FormulaDetalleDaoImpl(JdbcTemplate jdbcTemplate, FormulaDetalleDaoDefinition formulaDetalleDaoDefinition) {
        super(jdbcTemplate);
        this.formulaDetalleDaoDefinition = formulaDetalleDaoDefinition;
    }


    @Override
    public List<FormulaDetalle> listarPorFormulaId(Integer formulaId) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_FORMULA_DETALLE_LISTAR_POR_FORMULAID)
                .addParameter(new DaoParameter("par_formulaid", Types.INTEGER, formulaId))
                .setDaoDefinition(formulaDetalleDaoDefinition)
                .build();
    }

    @Override
    public List<FormulaDetalle> listarPlantillaPorFormulaId(Integer formulaId) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_FORMULA_DETALLE_PLANTILLA_LISTAR_POR_FORMULAID)
                .addParameter(new DaoParameter("par_formulaid", Types.INTEGER, formulaId))
                .setDaoDefinition(formulaDetalleDaoDefinition)
                .build();
    }

    @Override
    public List<FormulaDetalle> listarFiltro(FiltroFormulaDetalle filtroFormulaDetalle) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_FORMULA_DETALLE_LISTAR_FILTRO)
                .addParameter(new DaoParameter("par_formulaid", Types.INTEGER, filtroFormulaDetalle.getFormulaId()))
                .addParameter(new DaoParameter("par_tamanio", Types.INTEGER, filtroFormulaDetalle.getLength()))
                .addParameter(new DaoParameter("par_inicio", Types.INTEGER, filtroFormulaDetalle.getStart()))
                .setDaoDefinition(formulaDetalleDaoDefinition)
                .build();
    }

    @Override
    public Integer contarFiltro(FiltroFormulaDetalle filtroFormulaDetalle) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_FORMULA_DETALLE_CONTAR_FILTRO)
                .addParameter(new DaoParameter("par_fomulaid", Types.INTEGER, filtroFormulaDetalle.getFormulaId()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }

    @Override
    public Integer mantenimiento(FormulaDetalle formulaDetalle) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_FORMULA_DETALLE_MANTENIMIENTO)
                .addParameter(new DaoParameter("par_formula_detalle_id", Types.INTEGER, formulaDetalle.getFormulaDetalleId()))
                .addParameter(new DaoParameter("par_detalle_secuencia", Types.INTEGER, formulaDetalle.getFormulaDetalleSecuencia()))
                .addParameter(new DaoParameter("par_detalle_articulo_id", Types.INTEGER, formulaDetalle.getArticulo().getArticuloId()))
                .addParameter(new DaoParameter("par_detalle_precio", Types.DECIMAL, formulaDetalle.getFormulaDetallePrecio()))
                .addParameter(new DaoParameter("par_detalle_flete", Types.DECIMAL, formulaDetalle.getFormulaDetalleFlete()))
                .addParameter(new DaoParameter("par_detalle_cantidad", Types.DECIMAL, formulaDetalle.getFormulaDetalleCantidad()))
                .addParameter(new DaoParameter("par_detalle_observaciones", Types.VARCHAR, formulaDetalle.getFormulaDetalleObservaciones()))
                .addParameter(new DaoParameter("par_formula_id", Types.INTEGER, formulaDetalle.getFormula().getFormulaId()))
                .addParameter(new DaoParameter("par_detalle_estado", Types.INTEGER, formulaDetalle.getFormulaDetalleEstado()))
                .addParameter(new DaoParameter("par_usuario_mantenimiento", Types.INTEGER, formulaDetalle.getAuditoria().getUsuarioCreacion()))
                .addParameter(new DaoParameter("par_anio", Types.INTEGER, formulaDetalle.getFormula().getFormulaAnio()))
                .addParameter(new DaoParameter("par_semana", Types.INTEGER, formulaDetalle.getFormula().getFormulaSemana()))
                .addParameter(new DaoParameter("par_accion", Types.INTEGER, formulaDetalle.getAccion()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }

    @Override
    public FormulaDetalle obtenerPorId(Integer id) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_FORMULA_DETALLE_OBTENER_POR_ID)
                .addParameter(new DaoParameter("par_formula_detalle_id", Types.INTEGER, id))
                .setDaoDefinition(formulaDetalleDaoDefinition)
                .build(FormulaDetalle.class);
    }
}
