package pe.com.avivel.sistemas.costeoab.dao.molino.definition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoDefinition;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Costeo;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Envio;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class EnvioDaoDefinition extends DaoDefinition<Envio> {

    private EmpresaDaoDefinition empresaDaoDefinition;

    @Autowired
    public EnvioDaoDefinition(EmpresaDaoDefinition empresaDaoDefinition) {
        super(Envio.class);
        this.empresaDaoDefinition = empresaDaoDefinition;
    }

    @Override
    public Envio mapRow(ResultSet rs, int rowNumber) throws SQLException {
        Envio envio = super.mapRow(rs, rowNumber);
        envio.setEmpresa(empresaDaoDefinition.mapRow(rs, rowNumber));

        Costeo costeo = new Costeo();
        Integer costeoId = this.valInt("envio_costeo_id", rs);
        if (costeoId != null && costeoId > 0) {
            costeo.setCosteoId(costeoId);
        }
        envio.setCosteo(costeo);

        return envio;
    }
}
