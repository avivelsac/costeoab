package pe.com.avivel.sistemas.costeoab.dao.molino.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoBuilder;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoGenerator;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoParameter;
import pe.com.avivel.sistemas.costeoab.dao.molino.CosteoDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.definition.CosteoDaoDefinition;
import pe.com.avivel.sistemas.costeoab.dao.utils.ConstantesMolinoDAO;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Costeo;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroCosteo;

import java.sql.Types;
import java.util.List;

@Repository
public class CosteoDaoImpl extends DaoGenerator implements CosteoDao {

    private CosteoDaoDefinition costeoDaoDefinition;

    @Autowired
    public CosteoDaoImpl(JdbcTemplate jdbcTemplate, CosteoDaoDefinition costeoDaoDefinition) {
        super(jdbcTemplate);
        this.costeoDaoDefinition = costeoDaoDefinition;
    }

    @Override
    public List<Costeo> listarFiltro(FiltroCosteo filtroCosteo) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_COSTEO_LISTAR_FILTRO)
                .addParameter(new DaoParameter("par_empresa_id", Types.INTEGER, filtroCosteo.getEmpresaId()))
                .addParameter(new DaoParameter("par_anio", Types.INTEGER, filtroCosteo.getAnio()))
                .addParameter(new DaoParameter("par_semana", Types.INTEGER, filtroCosteo.getSemana()))
                .addParameter(new DaoParameter("par_tamanio", Types.INTEGER, filtroCosteo.getLength()))
                .addParameter(new DaoParameter("par_inicio", Types.INTEGER, filtroCosteo.getStart()))
                .setDaoDefinition(costeoDaoDefinition)
                .build();
    }

    @Override
    public Integer contarFiltro(FiltroCosteo filtroCosteo) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_COSTEO_CONTAR_FILTRO)
                .addParameter(new DaoParameter("par_empresa_id", Types.INTEGER, filtroCosteo.getEmpresaId()))
                .addParameter(new DaoParameter("par_anio", Types.INTEGER, filtroCosteo.getAnio()))
                .addParameter(new DaoParameter("par_semana", Types.INTEGER, filtroCosteo.getSemana()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }

    @Override
    public Integer mantenimiento(Costeo costeo) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_COSTEO_MANTENIMIENTO)
                .addParameter(new DaoParameter("par_costeo_id", Types.INTEGER, costeo.getCosteoId()))
                .addParameter(new DaoParameter("par_empresa_id", Types.INTEGER, costeo.getEmpresa().getEmpresaId()))
                .addParameter(new DaoParameter("par_anio", Types.INTEGER, costeo.getCosteoAnio()))
                .addParameter(new DaoParameter("par_semana", Types.INTEGER, costeo.getCosteoSemana()))
                .addParameter(new DaoParameter("par_observaciones", Types.VARCHAR, costeo.getCosteoObservaciones()))
                .addParameter(new DaoParameter("par_fecha_tipo_cambio", Types.DATE, costeo.getCosteoFechaTipoCambio()))
                .addParameter(new DaoParameter("par_tipo_cambio", Types.VARCHAR, costeo.getCosteoTipoCambio()))
                .addParameter(new DaoParameter("par_precio_flete_viaje", Types.VARCHAR, costeo.getCosteoPrecioFleteViaje()))
                .addParameter(new DaoParameter("par_estado", Types.INTEGER, costeo.getCosteoEstado()))
                .addParameter(new DaoParameter("par_usuario_mantenimiento", Types.INTEGER, costeo.getAuditoria().getUsuarioCreacion()))
                .addParameter(new DaoParameter("par_factura_fecha_emision", Types.DATE, costeo.getCosteoFacturaFechaEmision()))
                .addParameter(new DaoParameter("par_factura_serie", Types.VARCHAR, costeo.getCosteoFacturaSerie()))
                .addParameter(new DaoParameter("par_factura_numero", Types.VARCHAR, costeo.getCosteoFacturaNumero()))
                .addParameter(new DaoParameter("par_guia_remision", Types.VARCHAR, costeo.getCosteoFacturaGuiaRemision()))
                .addParameter(new DaoParameter("par_accion", Types.INTEGER, costeo.getAccion()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }

    @Override
    public Integer generarFactura(Costeo costeo) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_COSTEO_GENERAR_FACTURA)
                .addParameter(new DaoParameter("par_costeo_id", Types.INTEGER, costeo.getCosteoId()))
                .addParameter(new DaoParameter("par_fecha_emision", Types.DATE, costeo.getCosteoFacturaFechaEmision()))
                .addParameter(new DaoParameter("par_factura_serie", Types.VARCHAR, costeo.getCosteoFacturaSerie()))
                .addParameter(new DaoParameter("par_factura_numero", Types.VARCHAR, costeo.getCosteoFacturaNumero()))
                .addParameter(new DaoParameter("par_guia_remision", Types.VARCHAR, costeo.getCosteoFacturaGuiaRemision()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }

    @Override
    public Costeo obtenerPorId(Integer id) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_COSTEO_OBTENER_POR_ID)
                .addParameter(new DaoParameter("par_costeo_id", Types.INTEGER, id))
                .setDaoDefinition(costeoDaoDefinition)
                .build(Costeo.class);
    }
}
