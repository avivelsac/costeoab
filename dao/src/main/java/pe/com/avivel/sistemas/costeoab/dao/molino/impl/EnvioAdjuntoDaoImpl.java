package pe.com.avivel.sistemas.costeoab.dao.molino.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoBuilder;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoGenerator;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoParameter;
import pe.com.avivel.sistemas.costeoab.dao.molino.EnvioAdjuntoDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.definition.EnvioAdjuntoDaoDefinition;
import pe.com.avivel.sistemas.costeoab.dao.utils.ConstantesMolinoDAO;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.EnvioAdjunto;

import java.sql.Types;
import java.util.List;

@Repository
public class EnvioAdjuntoDaoImpl extends DaoGenerator implements EnvioAdjuntoDao {

    private EnvioAdjuntoDaoDefinition envioAdjuntoDaoDefinition;

    @Autowired
    public EnvioAdjuntoDaoImpl(JdbcTemplate jdbcTemplate, EnvioAdjuntoDaoDefinition envioAdjuntoDaoDefinition) {
        super(jdbcTemplate);
        this.envioAdjuntoDaoDefinition = envioAdjuntoDaoDefinition;
    }

    @Override
    public List<EnvioAdjunto> listarPorEnvioId(Integer envioId) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_ENVIO_ADJUNTO_LISTAR_POR_ENVIOID)
                .addParameter(new DaoParameter("par_envio_id", Types.INTEGER, envioId))
                .setDaoDefinition(envioAdjuntoDaoDefinition)
                .build();
    }

    @Override
    public Integer mantenimiento(EnvioAdjunto envioAdjunto) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_ENVIO_ADJUNTO_MANTENIMIENTO)
                .addParameter(new DaoParameter("par_envio_adjunto_id", Types.INTEGER, envioAdjunto.getEnvioAdjuntoId()))
                .addParameter(new DaoParameter("par_nombre", Types.VARCHAR, envioAdjunto.getEnvioAdjuntoNombre()))
                .addParameter(new DaoParameter("par_nombre_server", Types.VARCHAR, envioAdjunto.getEnvioAdjuntoNombreServer()))
                .addParameter(new DaoParameter("par_tipo", Types.VARCHAR, envioAdjunto.getEnvioAdjuntoTipo()))
                .addParameter(new DaoParameter("par_extension", Types.VARCHAR, envioAdjunto.getEnvioAdjuntoExtension()))
                .addParameter(new DaoParameter("par_estado", Types.INTEGER, envioAdjunto.getEnvioAdjuntoEstado()))
                .addParameter(new DaoParameter("par_envio_id", Types.INTEGER, envioAdjunto.getEnvio().getEnvioId()))
                .addParameter(new DaoParameter("par_usuario_mantenimiento", Types.INTEGER, envioAdjunto.getAuditoria().getUsuarioCreacion()))
                .addParameter(new DaoParameter("par_accion", Types.INTEGER, envioAdjunto.getAccion()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }
}
