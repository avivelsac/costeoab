package pe.com.avivel.sistemas.costeoab.dao.molino.definition;

import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoDefinition;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Rol;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class RolDaoDefinition extends DaoDefinition<Rol> {

    public RolDaoDefinition() {
        super(Rol.class);
    }

    @Override
    public Rol mapRow(ResultSet rs, int rowNumber) throws SQLException {
        return super.mapRow(rs, rowNumber);
    }
}
