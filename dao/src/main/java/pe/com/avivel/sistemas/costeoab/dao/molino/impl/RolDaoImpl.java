package pe.com.avivel.sistemas.costeoab.dao.molino.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoBuilder;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoGenerator;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoParameter;
import pe.com.avivel.sistemas.costeoab.dao.molino.RolDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.definition.RolDaoDefinition;
import pe.com.avivel.sistemas.costeoab.dao.utils.ConstantesMolinoDAO;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Rol;

import java.sql.Types;
import java.util.List;

@Repository
public class RolDaoImpl extends DaoGenerator implements RolDao {

    private RolDaoDefinition rolDaoDefinition;

    @Autowired
    public RolDaoImpl(JdbcTemplate jdbcTemplate, RolDaoDefinition rolDaoDefinition) {
        super(jdbcTemplate);
        this.rolDaoDefinition = rolDaoDefinition;
    }

    @Override
    public List<Rol> listarPorUsuarioId(Integer usuarioId) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_ROL_LISTAR_POR_USUARIOID)
                .addParameter(new DaoParameter("par_usuario_id", Types.INTEGER, usuarioId))
                .setDaoDefinition(rolDaoDefinition)
                .build();
    }
}
