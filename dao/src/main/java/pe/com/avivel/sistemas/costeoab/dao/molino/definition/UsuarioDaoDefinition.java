package pe.com.avivel.sistemas.costeoab.dao.molino.definition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoDefinition;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Usuario;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UsuarioDaoDefinition extends DaoDefinition<Usuario> {

    @Autowired
    public UsuarioDaoDefinition() {
        super(Usuario.class);
    }

    @Override
    public Usuario mapRow(ResultSet rs, int rowNumber) throws SQLException {
        return super.mapRow(rs, rowNumber);
    }
}
