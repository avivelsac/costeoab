package pe.com.avivel.sistemas.costeoab.dao.molino;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.Formula;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroFormula;

import java.util.List;

public interface FormulaDao {

    List<Formula> listarTodos(FiltroFormula filtroFormula);

    List<Formula> listarFiltro(FiltroFormula filtroFormula);

    Integer contarFiltro(FiltroFormula filtroFormula);

    Integer mantenimiento(Formula formula);

    Formula obtenerPorId(Integer id);
}
