package pe.com.avivel.sistemas.costeoab.dao.molino;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.Articulo;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroArticulo;

import java.util.List;

public interface ArticuloDao {

    List<Articulo> listarFiltro(FiltroArticulo filtroArticulo);

    Integer contarFiltro(FiltroArticulo filtroArticulo);

    Articulo obtenerPorId(Integer id);
}
