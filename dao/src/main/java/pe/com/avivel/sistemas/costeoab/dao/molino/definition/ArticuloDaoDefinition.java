package pe.com.avivel.sistemas.costeoab.dao.molino.definition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoDefinition;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Articulo;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ArticuloDaoDefinition extends DaoDefinition<Articulo> {

    private UnidadMedidaDaoDefinition unidadMedidaDaoDefinition;
    private PrecioDaoDefinition precioDaoDefinition;

    @Autowired
    public ArticuloDaoDefinition(UnidadMedidaDaoDefinition unidadMedidaDaoDefinition, PrecioDaoDefinition precioDaoDefinition) {
        super(Articulo.class);
        this.unidadMedidaDaoDefinition = unidadMedidaDaoDefinition;
        this.precioDaoDefinition = precioDaoDefinition;
    }

    @Override
    public Articulo mapRow(ResultSet rs, int rowNumber) throws SQLException {
        Articulo articulo = super.mapRow(rs, rowNumber);
        articulo.setPrecio(precioDaoDefinition.mapRow(rs, rowNumber));
        articulo.setUnidadMedida(unidadMedidaDaoDefinition.mapRow(rs, rowNumber));
        return articulo;
    }
}
