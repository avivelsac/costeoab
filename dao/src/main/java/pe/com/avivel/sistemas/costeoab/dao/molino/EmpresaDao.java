package pe.com.avivel.sistemas.costeoab.dao.molino;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.Empresa;

import java.util.List;

public interface EmpresaDao {

    List<Empresa> listarTodos();
}
