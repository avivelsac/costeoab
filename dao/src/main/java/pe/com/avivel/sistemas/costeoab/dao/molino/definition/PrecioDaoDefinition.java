package pe.com.avivel.sistemas.costeoab.dao.molino.definition;

import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoDefinition;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Precio;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class PrecioDaoDefinition extends DaoDefinition<Precio> {

    public PrecioDaoDefinition() {
        super(Precio.class);
    }

    @Override
    public Precio mapRow(ResultSet rs, int rowNumber) throws SQLException {
        return super.mapRow(rs, rowNumber);
    }
}
