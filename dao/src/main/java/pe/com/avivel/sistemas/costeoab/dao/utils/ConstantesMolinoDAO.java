package pe.com.avivel.sistemas.costeoab.dao.utils;

public class ConstantesMolinoDAO {

    // schema
    public static final String SCHEMA_NAME = "bd_molino";

    // usuario
    public static final String SP_USUARIO_AUTENTICAR = "sp_usuario_autenticar";
    public static final String SP_USUARIO_LISTAR_PARA_NOTIFICAR = "sp_usuario_listar_para_notificar";

    // rol
    public static final String SP_ROL_LISTAR_POR_USUARIOID = "sp_rol_listar_por_usuarioid";

    // articulo
    public static final String SP_ARTICULO_LISTAR_FILTRO = "sp_articulo_listar_filtro";
    public static final String SP_ARTICULO_CONTAR_FILTRO = "sp_articulo_contar_filtro";
    public static final String SP_ARTICULO_OBTENER_POR_ID = "sp_articulo_obtener_por_id";

    // precio
    public static final String SP_PRECIO_MANTENIMIENTO = "sp_precio_mantenimiento";

    // empresa
    public static final String SP_EMPRESA_LISTAR_TODOS = "sp_empresa_listar_todos";

    // formula
    public static final String SP_FORMULA_LISTAR_TODOS = "sp_formula_listar_todos";
    public static final String SP_FORMULA_LISTAR_FILTRO = "sp_formula_listar_filtro";
    public static final String SP_FORMULA_CONTAR_FILTRO = "sp_formula_contar_filtro";
    public static final String SP_FORMULA_MANTENIMIENTO = "sp_formula_mantenimiento";
    public static final String SP_FORMULA_OBTENER_POR_ID = "sp_formula_obtener_por_id";

    // formula detalle
    public static final String SP_FORMULA_DETALLE_LISTAR_FILTRO = "sp_formula_detalle_listar_filtro";
    public static final String SP_FORMULA_DETALLE_CONTAR_FILTRO = "sp_formula_detalle_contar_filtro";
    public static final String SP_FORMULA_DETALLE_MANTENIMIENTO = "sp_formula_detalle_mantenimiento";
    public static final String SP_FORMULA_DETALLE_OBTENER_POR_ID = "sp_formula_detalle_obtener_por_id";
    public static final String SP_FORMULA_DETALLE_LISTAR_POR_FORMULAID = "sp_formula_plantilla_listar_por_formulaid";
    public static final String SP_FORMULA_DETALLE_PLANTILLA_LISTAR_POR_FORMULAID = "sp_formula_detalle_plantilla_listar_por_formulaid";

    // envio
    public static final String SP_ENVIO_LISTAR_FILTRO = "sp_envio_listar_filtro";
    public static final String SP_ENVIO_CONTAR_FILTRO = "sp_envio_contar_filtro";
    public static final String SP_ENVIO_MANTENIMIENTO = "sp_envio_mantenimiento";
    public static final String SP_ENVIO_OBTENER_POR_ID = "sp_envio_obtener_por_id";

    // envio detalle
    public static final String SP_ENVIO_DETALLE_LISTAR_FILTRO = "sp_envio_detalle_listar_filtro";
    public static final String SP_ENVIO_DETALLE_CONTAR_FILTRO = "sp_envio_detalle_contar_filtro";
    public static final String SP_ENVIO_DETALLE_LISTAR_POR_ENVIOID = "sp_envio_detalle_listar_por_envioid";

    // envio adjunto
    public static final String SP_ENVIO_ADJUNTO_LISTAR_POR_ENVIOID = "sp_envio_adjunto_listar_por_envioid";
    public static final String SP_ENVIO_ADJUNTO_MANTENIMIENTO = "sp_envio_adjunto_mantenimiento";

    // costeo
    public static final String SP_COSTEO_LISTAR_FILTRO = "sp_costeo_listar_filtro";
    public static final String SP_COSTEO_CONTAR_FILTRO = "sp_costeo_contar_filtro";
    public static final String SP_COSTEO_MANTENIMIENTO = "sp_costeo_mantenimiento";
    public static final String SP_COSTEO_OBTENER_POR_ID = "sp_costeo_obtener_por_id";
    public static final String SP_COSTEO_GENERAR_FACTURA = "sp_costeo_generar_factura";

    // costeo detalle
    public static final String SP_COSTEO_DETALLE_LISTAR_FILTRO = "sp_costeo_detalle_listar_filtro";
    public static final String SP_COSTEO_DETALLE_CONTAR_FILTRO = "sp_costeo_detalle_contar_filtro";
    public static final String SP_COSTEO_DETALLE_MANTENIMIENTO = "sp_costeo_detalle_mantenimiento";
    public static final String SP_COSTEO_DETALLE_OBTENER_POR_ID = "sp_costeo_detalle_obtener_por_id";
    public static final String SP_COSTEO_DETALLE_CALCULAR_FLETES = "sp_costeo_detalle_calcular_fletes";
}
