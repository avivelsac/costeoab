package pe.com.avivel.sistemas.costeoab.dao.molino;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.EnvioAdjunto;

import java.util.List;

public interface EnvioAdjuntoDao {

    List<EnvioAdjunto> listarPorEnvioId(Integer envioId);

    Integer mantenimiento(EnvioAdjunto envioAdjunto);
}
