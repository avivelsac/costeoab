package pe.com.avivel.sistemas.costeoab.dao.molino.definition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoDefinition;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.CosteoDetalle;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CosteoDetalleDaoDefinition extends DaoDefinition<CosteoDetalle> {

    private FormulaDaoDefinition formulaDaoDefinition;

    @Autowired
    public CosteoDetalleDaoDefinition(FormulaDaoDefinition formulaDaoDefinition) {
        super(CosteoDetalle.class);
        this.formulaDaoDefinition = formulaDaoDefinition;
    }

    @Override
    public CosteoDetalle mapRow(ResultSet rs, int rowNumber) throws SQLException {
        CosteoDetalle costeoDetalle = super.mapRow(rs, rowNumber);
        costeoDetalle.setFormula(formulaDaoDefinition.mapRow(rs, rowNumber));
        return costeoDetalle;
    }
}
