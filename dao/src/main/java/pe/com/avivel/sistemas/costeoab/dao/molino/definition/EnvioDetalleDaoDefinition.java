package pe.com.avivel.sistemas.costeoab.dao.molino.definition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoDefinition;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.EnvioDetalle;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class EnvioDetalleDaoDefinition extends DaoDefinition<EnvioDetalle> {

    private FormulaDaoDefinition formulaDaoDefinition;

    @Autowired
    public EnvioDetalleDaoDefinition(FormulaDaoDefinition formulaDaoDefinition) {
        super(EnvioDetalle.class);
        this.formulaDaoDefinition = formulaDaoDefinition;
    }

    @Override
    public EnvioDetalle mapRow(ResultSet rs, int rowNumber) throws SQLException {
        EnvioDetalle envioDetalle = super.mapRow(rs, rowNumber);
        envioDetalle.setFormula(formulaDaoDefinition.mapRow(rs, rowNumber));
        return envioDetalle;
    }
}
