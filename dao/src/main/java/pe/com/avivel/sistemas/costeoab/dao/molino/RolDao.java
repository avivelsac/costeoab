package pe.com.avivel.sistemas.costeoab.dao.molino;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.Rol;

import java.util.List;

public interface RolDao {

    List<Rol> listarPorUsuarioId(Integer usuarioId);
}
