package pe.com.avivel.sistemas.costeoab.dao.molino.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoBuilder;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoGenerator;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoParameter;
import pe.com.avivel.sistemas.costeoab.dao.molino.ArticuloDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.definition.ArticuloDaoDefinition;
import pe.com.avivel.sistemas.costeoab.dao.utils.ConstantesMolinoDAO;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Articulo;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroArticulo;

import java.sql.Types;
import java.util.List;

@Repository
public class ArticuloDaoImpl extends DaoGenerator implements ArticuloDao {

    private ArticuloDaoDefinition articuloDaoDefinition;

    @Autowired
    public ArticuloDaoImpl(JdbcTemplate jdbcTemplate, ArticuloDaoDefinition articuloDaoDefinition) {
        super(jdbcTemplate);
        this.articuloDaoDefinition = articuloDaoDefinition;
    }

    @Override
    public List<Articulo> listarFiltro(FiltroArticulo filtroArticulo) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_ARTICULO_LISTAR_FILTRO)
                .addParameter(new DaoParameter("par_codigo", Types.VARCHAR, filtroArticulo.getCodigo()))
                .addParameter(new DaoParameter("par_codigo2", Types.VARCHAR, filtroArticulo.getCodigo2()))
                .addParameter(new DaoParameter("par_descripcion", Types.VARCHAR, filtroArticulo.getDescripcion()))
                .addParameter(new DaoParameter("par_tamanio", Types.INTEGER, filtroArticulo.getLength()))
                .addParameter(new DaoParameter("par_inicio", Types.INTEGER, filtroArticulo.getStart()))
                .setDaoDefinition(articuloDaoDefinition)
                .build();
    }

    @Override
    public Integer contarFiltro(FiltroArticulo filtroArticulo) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_ARTICULO_CONTAR_FILTRO)
                .addParameter(new DaoParameter("par_codigo", Types.VARCHAR, filtroArticulo.getCodigo()))
                .addParameter(new DaoParameter("par_codigo2", Types.VARCHAR, filtroArticulo.getCodigo2()))
                .addParameter(new DaoParameter("par_descripcion", Types.VARCHAR, filtroArticulo.getDescripcion()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }

    @Override
    public Articulo obtenerPorId(Integer id) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_ARTICULO_OBTENER_POR_ID)
                .addParameter(new DaoParameter("par_articulo_id", Types.INTEGER, id))
                .setDaoDefinition(articuloDaoDefinition)
                .build(Articulo.class);
    }
}
