package pe.com.avivel.sistemas.costeoab.dao.molino.definition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoDefinition;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.FormulaDetalle;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class FormulaDetalleDaoDefinition extends DaoDefinition<FormulaDetalle> {

    private ArticuloDaoDefinition articuloDaoDefinition;

    @Autowired
    public FormulaDetalleDaoDefinition(ArticuloDaoDefinition articuloDaoDefinition) {
        super(FormulaDetalle.class);
        this.articuloDaoDefinition = articuloDaoDefinition;
    }

    @Override
    public FormulaDetalle mapRow(ResultSet rs, int rowNumber) throws SQLException {
        FormulaDetalle formulaDetalle = super.mapRow(rs, rowNumber);
        formulaDetalle.setArticulo(articuloDaoDefinition.mapRow(rs, rowNumber));
        return formulaDetalle;
    }
}
