package pe.com.avivel.sistemas.costeoab.dao.molino;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.FormulaDetalle;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroFormulaDetalle;

import java.util.List;

public interface FormulaDetalleDao {

    List<FormulaDetalle> listarPorFormulaId(Integer formulaId);

    List<FormulaDetalle> listarPlantillaPorFormulaId(Integer formulaId);

    List<FormulaDetalle> listarFiltro(FiltroFormulaDetalle filtroFormulaDetalle);

    Integer contarFiltro(FiltroFormulaDetalle filtroFormulaDetalle);

    Integer mantenimiento(FormulaDetalle formulaDetalle);

    FormulaDetalle obtenerPorId(Integer id);
}
