package pe.com.avivel.sistemas.costeoab.dao.molino.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoBuilder;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoGenerator;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoParameter;
import pe.com.avivel.sistemas.costeoab.dao.molino.FormulaDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.definition.FormulaDaoDefinition;
import pe.com.avivel.sistemas.costeoab.dao.utils.ConstantesMolinoDAO;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Formula;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroFormula;

import java.sql.Types;
import java.util.List;

@Repository
public class FormulaDaoImpl extends DaoGenerator implements FormulaDao {

    private FormulaDaoDefinition formulaDaoDefinition;

    @Autowired
    public FormulaDaoImpl(JdbcTemplate jdbcTemplate, FormulaDaoDefinition formulaDaoDefinition) {
        super(jdbcTemplate);
        this.formulaDaoDefinition = formulaDaoDefinition;
    }

    @Override
    public List<Formula> listarTodos(FiltroFormula filtroFormula) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_FORMULA_LISTAR_TODOS)
                .addParameter(new DaoParameter("par_tipo", Types.VARCHAR, filtroFormula.getTipo()))
                .addParameter(new DaoParameter("par_estado", Types.INTEGER, filtroFormula.getEstado()))
                .addParameter(new DaoParameter("par_anio", Types.INTEGER, filtroFormula.getAnio()))
                .addParameter(new DaoParameter("par_semana", Types.INTEGER, filtroFormula.getSemana()))
                .addParameter(new DaoParameter("par_empresa_id", Types.INTEGER, filtroFormula.getEmpresaId()))
                .setDaoDefinition(formulaDaoDefinition)
                .build();
    }

    @Override
    public List<Formula> listarFiltro(FiltroFormula filtroFormula) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_FORMULA_LISTAR_FILTRO)
                .addParameter(new DaoParameter("par_codigo", Types.VARCHAR, filtroFormula.getCodigo()))
                .addParameter(new DaoParameter("par_descripcion", Types.VARCHAR, filtroFormula.getDescripcion()))
                .addParameter(new DaoParameter("par_empresa_id", Types.INTEGER, filtroFormula.getEmpresaId()))
                .addParameter(new DaoParameter("par_anio", Types.INTEGER, filtroFormula.getAnio()))
                .addParameter(new DaoParameter("par_semana", Types.INTEGER, filtroFormula.getSemana()))
                .addParameter(new DaoParameter("par_tipo", Types.VARCHAR, filtroFormula.getTipo()))
                .addParameter(new DaoParameter("par_estado", Types.INTEGER, filtroFormula.getEstado()))
                .addParameter(new DaoParameter("par_tamanio", Types.INTEGER, filtroFormula.getLength()))
                .addParameter(new DaoParameter("par_inicio", Types.INTEGER, filtroFormula.getStart()))
                .setDaoDefinition(formulaDaoDefinition)
                .build();
    }

    @Override
    public Integer contarFiltro(FiltroFormula filtroFormula) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_FORMULA_CONTAR_FILTRO)
                .addParameter(new DaoParameter("par_codigo", Types.VARCHAR, filtroFormula.getCodigo()))
                .addParameter(new DaoParameter("par_descripcion", Types.VARCHAR, filtroFormula.getDescripcion()))
                .addParameter(new DaoParameter("par_empresa_id", Types.INTEGER, filtroFormula.getEmpresaId()))
                .addParameter(new DaoParameter("par_anio", Types.INTEGER, filtroFormula.getAnio()))
                .addParameter(new DaoParameter("par_semana", Types.INTEGER, filtroFormula.getSemana()))
                .addParameter(new DaoParameter("par_tipo", Types.VARCHAR, filtroFormula.getTipo()))
                .addParameter(new DaoParameter("par_estado", Types.INTEGER, filtroFormula.getEstado()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }

    @Override
    public Integer mantenimiento(Formula formula) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_FORMULA_MANTENIMIENTO)
                .addParameter(new DaoParameter("par_formula_id", Types.INTEGER, formula.getFormulaId()))
                .addParameter(new DaoParameter("par_codigo", Types.VARCHAR, formula.getFormulaCodigo()))
                .addParameter(new DaoParameter("par_etapa", Types.VARCHAR, formula.getFormulaEtapa()))
                .addParameter(new DaoParameter("par_descripcion", Types.VARCHAR, formula.getFormulaDescripcion()))
                .addParameter(new DaoParameter("par_empresa_id", Types.INTEGER, formula.getEmpresa().getEmpresaId()))
                .addParameter(new DaoParameter("par_cantidad", Types.DECIMAL, formula.getFormulaCantidad()))
                .addParameter(new DaoParameter("par_cantidad_producida", Types.DECIMAL, formula.getFormulaCantidadProducida()))
                .addParameter(new DaoParameter("par_observaciones", Types.VARCHAR, formula.getFormulaObservaciones()))
                .addParameter(new DaoParameter("par_parent_id", Types.INTEGER, formula.getFormulaParentId()))
                .addParameter(new DaoParameter("par_semana", Types.INTEGER, formula.getFormulaSemana()))
                .addParameter(new DaoParameter("par_anio", Types.INTEGER, formula.getFormulaAnio()))
                .addParameter(new DaoParameter("par_tipo", Types.VARCHAR, formula.getFormulaTipo()))
                .addParameter(new DaoParameter("par_estado", Types.INTEGER, formula.getFormulaEstado()))
                .addParameter(new DaoParameter("par_usuario_mantenimiento", Types.INTEGER, formula.getAuditoria().getUsuarioCreacion()))
                .addParameter(new DaoParameter("par_accion", Types.INTEGER, formula.getAccion()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }

    @Override
    public Formula obtenerPorId(Integer id) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_FORMULA_OBTENER_POR_ID)
                .addParameter(new DaoParameter("par_formula_id", Types.INTEGER, id))
                .setDaoDefinition(formulaDaoDefinition)
                .build(Formula.class);
    }
}
