package pe.com.avivel.sistemas.costeoab.dao.molino.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoBuilder;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoGenerator;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoParameter;
import pe.com.avivel.sistemas.costeoab.dao.molino.PrecioDao;
import pe.com.avivel.sistemas.costeoab.dao.utils.ConstantesMolinoDAO;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Precio;

import java.sql.Types;

@Repository
public class PrecioDaoImpl extends DaoGenerator implements PrecioDao {

    @Autowired
    public PrecioDaoImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    public Integer mantenimiento(Precio precio) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_PRECIO_MANTENIMIENTO)
                .addParameter(new DaoParameter("par_precio_id", Types.INTEGER, precio.getPrecioId()))
                .addParameter(new DaoParameter("par_precio_semana", Types.INTEGER, precio.getPrecioSemana()))
                .addParameter(new DaoParameter("par_articulo_id", Types.INTEGER, precio.getArticulo().getArticuloId()))
                .addParameter(new DaoParameter("par_precio_valor", Types.DECIMAL, precio.getPrecioValor()))
                .addParameter(new DaoParameter("par_precio_flete_valor", Types.DECIMAL, precio.getPrecioFleteValor()))
                .addParameter(new DaoParameter("par_precio_actual", Types.BOOLEAN, precio.getPrecioActual()))
                .addParameter(new DaoParameter("par_precio_estado", Types.INTEGER, precio.getPrecioEstado()))
                .addParameter(new DaoParameter("par_usuario_mantenimiento", Types.INTEGER, precio.getAuditoria().getUsuarioCreacion()))
                .addParameter(new DaoParameter("par_accion", Types.INTEGER, precio.getAccion()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }
}
