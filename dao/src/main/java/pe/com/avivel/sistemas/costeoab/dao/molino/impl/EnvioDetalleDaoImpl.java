package pe.com.avivel.sistemas.costeoab.dao.molino.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoBuilder;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoGenerator;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoParameter;
import pe.com.avivel.sistemas.costeoab.dao.molino.EnvioDetalleDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.definition.EnvioDetalleDaoDefinition;
import pe.com.avivel.sistemas.costeoab.dao.utils.ConstantesMolinoDAO;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.EnvioDetalle;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroEnvioDetalle;

import java.sql.Types;
import java.util.List;

@Repository
public class EnvioDetalleDaoImpl extends DaoGenerator implements EnvioDetalleDao {

    private EnvioDetalleDaoDefinition envioDetalleDaoDefinition;

    @Autowired
    public EnvioDetalleDaoImpl(JdbcTemplate jdbcTemplate, EnvioDetalleDaoDefinition envioDetalleDaoDefinition) {
        super(jdbcTemplate);
        this.envioDetalleDaoDefinition = envioDetalleDaoDefinition;
    }

    @Override
    public List<EnvioDetalle> listarFiltro(FiltroEnvioDetalle filtroEnvioDetalle) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_ENVIO_DETALLE_LISTAR_FILTRO)
                .addParameter(new DaoParameter("par_envio_id", Types.INTEGER, filtroEnvioDetalle.getEnvioId()))
                .addParameter(new DaoParameter("par_tamanio", Types.INTEGER, filtroEnvioDetalle.getLength()))
                .addParameter(new DaoParameter("par_inicio", Types.INTEGER, filtroEnvioDetalle.getStart()))
                .setDaoDefinition(envioDetalleDaoDefinition)
                .build();
    }

    @Override
    public Integer contarFiltro(FiltroEnvioDetalle filtroEnvioDetalle) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_ENVIO_DETALLE_CONTAR_FILTRO)
                .addParameter(new DaoParameter("par_envio_id", Types.INTEGER, filtroEnvioDetalle.getEnvioId()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }

    @Override
    public List<EnvioDetalle> listarPorEnvioId(Integer envioId) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_ENVIO_DETALLE_LISTAR_POR_ENVIOID)
                .addParameter(new DaoParameter("par_envio_id", Types.INTEGER, envioId))
                .setDaoDefinition(envioDetalleDaoDefinition)
                .build();
    }
}
