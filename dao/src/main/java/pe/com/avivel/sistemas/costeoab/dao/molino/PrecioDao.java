package pe.com.avivel.sistemas.costeoab.dao.molino;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.Precio;

public interface PrecioDao {

    Integer mantenimiento(Precio precio);
}
