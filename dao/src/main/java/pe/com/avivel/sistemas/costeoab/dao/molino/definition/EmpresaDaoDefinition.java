package pe.com.avivel.sistemas.costeoab.dao.molino.definition;

import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoDefinition;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Empresa;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class EmpresaDaoDefinition extends DaoDefinition<Empresa> {

    public EmpresaDaoDefinition() {
        super(Empresa.class);
    }

    @Override
    public Empresa mapRow(ResultSet rs, int rowNumber) throws SQLException {
        return super.mapRow(rs, rowNumber);
    }
}
