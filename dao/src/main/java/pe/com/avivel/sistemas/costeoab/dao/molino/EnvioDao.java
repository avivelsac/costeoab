package pe.com.avivel.sistemas.costeoab.dao.molino;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.Envio;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroEnvio;

import java.util.List;

public interface EnvioDao {

    List<Envio> listarFiltro(FiltroEnvio filtroEnvio);

    Integer contarFiltro(FiltroEnvio filtroEnvio);

    Integer mantenimiento(Envio envio);

    Envio obtenerPorId(Integer id);
}
