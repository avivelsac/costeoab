package pe.com.avivel.sistemas.costeoab.dao.molino;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.EnvioDetalle;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroEnvioDetalle;

import java.util.List;

public interface EnvioDetalleDao {

    List<EnvioDetalle> listarFiltro(FiltroEnvioDetalle filtroEnvioDetalle);

    Integer contarFiltro(FiltroEnvioDetalle filtroEnvioDetalle);

    List<EnvioDetalle> listarPorEnvioId(Integer envioId);
}
