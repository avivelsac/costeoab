package pe.com.avivel.sistemas.costeoab.dao.molino.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoBuilder;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoGenerator;
import pe.com.avivel.sistemas.costeoab.dao.molino.EmpresaDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.definition.EmpresaDaoDefinition;
import pe.com.avivel.sistemas.costeoab.dao.utils.ConstantesMolinoDAO;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Empresa;

import java.util.List;

@Repository
public class EmpresaDaoImpl extends DaoGenerator implements EmpresaDao {

    private EmpresaDaoDefinition empresaDaoDefinition;

    @Autowired
    public EmpresaDaoImpl(JdbcTemplate jdbcTemplate, EmpresaDaoDefinition empresaDaoDefinition) {
        super(jdbcTemplate);
        this.empresaDaoDefinition = empresaDaoDefinition;
    }

    @Override
    public List<Empresa> listarTodos() {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_EMPRESA_LISTAR_TODOS)
                .setDaoDefinition(empresaDaoDefinition)
                .build();
    }
}
