package pe.com.avivel.sistemas.costeoab.dao.molino.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoBuilder;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoGenerator;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoParameter;
import pe.com.avivel.sistemas.costeoab.dao.molino.EnvioDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.definition.EnvioDaoDefinition;
import pe.com.avivel.sistemas.costeoab.dao.utils.ConstantesMolinoDAO;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Envio;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroEnvio;

import java.sql.Types;
import java.util.List;

@Repository
public class EnvioDaoImpl extends DaoGenerator implements EnvioDao {

    private EnvioDaoDefinition envioDaoDefinition;

    @Autowired
    public EnvioDaoImpl(JdbcTemplate jdbcTemplate, EnvioDaoDefinition envioDaoDefinition) {
        super(jdbcTemplate);
        this.envioDaoDefinition = envioDaoDefinition;
    }

    @Override
    public List<Envio> listarFiltro(FiltroEnvio filtroEnvio) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_ENVIO_LISTAR_FILTRO)
                .addParameter(new DaoParameter("par_desde", Types.DATE, filtroEnvio.getDesde()))
                .addParameter(new DaoParameter("par_hasta", Types.DATE, filtroEnvio.getHasta()))
                .addParameter(new DaoParameter("par_empresa_id", Types.INTEGER, filtroEnvio.getEmpresaId()))
                .addParameter(new DaoParameter("par_anio", Types.INTEGER, filtroEnvio.getAnio()))
                .addParameter(new DaoParameter("par_semana", Types.INTEGER, filtroEnvio.getSemana()))
                .addParameter(new DaoParameter("par_estado", Types.INTEGER, filtroEnvio.getEstado()))
                .addParameter(new DaoParameter("par_tamanio", Types.INTEGER, filtroEnvio.getLength()))
                .addParameter(new DaoParameter("par_inicio", Types.INTEGER, filtroEnvio.getStart()))
                .setDaoDefinition(envioDaoDefinition)
                .build();
    }

    @Override
    public Integer contarFiltro(FiltroEnvio filtroEnvio) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_ENVIO_CONTAR_FILTRO)
                .addParameter(new DaoParameter("par_desde", Types.DATE, filtroEnvio.getDesde()))
                .addParameter(new DaoParameter("par_hasta", Types.DATE, filtroEnvio.getHasta()))
                .addParameter(new DaoParameter("par_empresa_id", Types.INTEGER, filtroEnvio.getEmpresaId()))
                .addParameter(new DaoParameter("par_anio", Types.INTEGER, filtroEnvio.getAnio()))
                .addParameter(new DaoParameter("par_semana", Types.INTEGER, filtroEnvio.getSemana()))
                .addParameter(new DaoParameter("par_estado", Types.INTEGER, filtroEnvio.getEstado()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }

    @Override
    public Integer mantenimiento(Envio envio) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_ENVIO_MANTENIMIENTO)
                .addParameter(new DaoParameter("par_envio_id", Types.INTEGER, envio.getEnvioId()))
                .addParameter(new DaoParameter("par_empresa_id", Types.INTEGER, envio.getEmpresa().getEmpresaId()))
                .addParameter(new DaoParameter("par_anio", Types.INTEGER, envio.getEnvioAnio()))
                .addParameter(new DaoParameter("par_semana", Types.INTEGER, envio.getEnvioSemana()))
                .addParameter(new DaoParameter("par_fecha_produccion", Types.DATE, envio.getEnvioFechaProduccion()))
                .addParameter(new DaoParameter("par_ccg", Types.DECIMAL, envio.getEnvioCCG()))
                .addParameter(new DaoParameter("par_mmi", Types.DECIMAL, envio.getEnvioMMI()))
                .addParameter(new DaoParameter("par_viajes", Types.DECIMAL, envio.getEnvioViajes()))
                .addParameter(new DaoParameter("par_observaciones", Types.VARCHAR, envio.getEnvioObservaciones()))
                .addParameter(new DaoParameter("par_estado", Types.INTEGER, envio.getEnvioEstado()))
                .addParameter(new DaoParameter("par_usuario_mantenimiento", Types.INTEGER, envio.getAuditoria().getUsuarioCreacion()))
                .addParameter(new DaoParameter("par_costeo_fecha_tipo_cambio", Types.DATE, envio.getCosteo().getCosteoFechaTipoCambio()))
                .addParameter(new DaoParameter("par_costeo_tipo_cambio", Types.DECIMAL, envio.getCosteo().getCosteoTipoCambio()))
                .addParameter(new DaoParameter("par_costeo_precio_flete_viaje", Types.DECIMAL, envio.getCosteo().getCosteoPrecioFleteViaje()))
                .addParameter(new DaoParameter("par_accion", Types.INTEGER, envio.getAccion()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }

    @Override
    public Envio obtenerPorId(Integer id) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_ENVIO_OBTENER_POR_ID)
                .addParameter(new DaoParameter("par_envio_id", Types.INTEGER, id))
                .setDaoDefinition(envioDaoDefinition)
                .build(Envio.class);
    }
}
