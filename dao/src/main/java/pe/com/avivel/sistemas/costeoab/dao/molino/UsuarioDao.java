package pe.com.avivel.sistemas.costeoab.dao.molino;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.Usuario;

import java.util.List;

public interface UsuarioDao {

    List<Usuario> listarParaNotificar();

    Usuario autenticar(String usuarioNombre);
}
