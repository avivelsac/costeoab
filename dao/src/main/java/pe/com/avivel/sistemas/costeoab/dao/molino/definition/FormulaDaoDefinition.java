package pe.com.avivel.sistemas.costeoab.dao.molino.definition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoDefinition;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Formula;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class FormulaDaoDefinition extends DaoDefinition<Formula> {

    private EmpresaDaoDefinition empresaDaoDefinition;

    @Autowired
    public FormulaDaoDefinition(EmpresaDaoDefinition empresaDaoDefinition) {
        super(Formula.class);
        this.empresaDaoDefinition = empresaDaoDefinition;
    }

    @Override
    public Formula mapRow(ResultSet rs, int rowNumber) throws SQLException {
        Formula formula = super.mapRow(rs, rowNumber);
        formula.setEmpresa(empresaDaoDefinition.mapRow(rs, rowNumber));
        return formula;
    }
}
