package pe.com.avivel.sistemas.costeoab.dao.molino.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoBuilder;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoGenerator;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoParameter;
import pe.com.avivel.sistemas.costeoab.dao.molino.CosteoDetalleDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.definition.CosteoDetalleDaoDefinition;
import pe.com.avivel.sistemas.costeoab.dao.utils.ConstantesMolinoDAO;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Costeo;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.CosteoDetalle;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroCosteoDetalle;

import java.sql.Types;
import java.util.List;

@Repository
public class CosteoDetalleDaoImpl extends DaoGenerator implements CosteoDetalleDao {

    private CosteoDetalleDaoDefinition costeoDetalleDaoDefinition;

    @Autowired
    public CosteoDetalleDaoImpl(JdbcTemplate jdbcTemplate, CosteoDetalleDaoDefinition costeoDetalleDaoDefinition) {
        super(jdbcTemplate);
        this.costeoDetalleDaoDefinition = costeoDetalleDaoDefinition;
    }

    @Override
    public List<CosteoDetalle> listarFiltro(FiltroCosteoDetalle filtroCosteoDetalle) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_COSTEO_DETALLE_LISTAR_FILTRO)
                .addParameter(new DaoParameter("par_costeo_id", Types.INTEGER, filtroCosteoDetalle.getCosteoId()))
                .addParameter(new DaoParameter("par_tamanio", Types.INTEGER, filtroCosteoDetalle.getLength()))
                .addParameter(new DaoParameter("par_inicio", Types.INTEGER, filtroCosteoDetalle.getStart()))
                .setDaoDefinition(costeoDetalleDaoDefinition)
                .build();
    }

    @Override
    public Integer contarFiltro(FiltroCosteoDetalle filtroCosteoDetalle) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_COSTEO_DETALLE_CONTAR_FILTRO)
                .addParameter(new DaoParameter("par_costeo_id", Types.INTEGER, filtroCosteoDetalle.getCosteoId()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }

    @Override
    public Integer mantenimiento(CosteoDetalle costeoDetalle) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_COSTEO_DETALLE_MANTENIMIENTO)
                .addParameter(new DaoParameter("par_costeo_detalle_id", Types.INTEGER, costeoDetalle.getCosteoDetalleId()))
                .addParameter(new DaoParameter("par_cantidad", Types.DECIMAL, costeoDetalle.getCosteoDetalleCantidad()))
                .addParameter(new DaoParameter("par_formula_id", Types.INTEGER, costeoDetalle.getFormula().getFormulaId()))
                .addParameter(new DaoParameter("par_flete", Types.DECIMAL, costeoDetalle.getCosteoDetalleFlete()))
                .addParameter(new DaoParameter("par_facator_flete", Types.DECIMAL, costeoDetalle.getCosteoDetalleFactorFlete()))
                .addParameter(new DaoParameter("par_estado", Types.INTEGER, costeoDetalle.getCosteoDetalleEstado()))
                .addParameter(new DaoParameter("par_costeo_id", Types.INTEGER, costeoDetalle.getCosteo().getCosteoId()))
                .addParameter(new DaoParameter("par_usuario_mantenimiento", Types.INTEGER, costeoDetalle.getAuditoria().getUsuarioCreacion()))
                .addParameter(new DaoParameter("par_accion", Types.INTEGER, costeoDetalle.getAccion()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }

    @Override
    public Integer calcularFletes(Costeo costeo) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_COSTEO_DETALLE_CALCULAR_FLETES)
                .addParameter(new DaoParameter("par_costeo_id", Types.INTEGER, costeo.getCosteoId()))
                .addParameter(new DaoParameter("par_usuario_mantenimiento", Types.INTEGER, costeo.getAuditoria().getUsuarioCreacion()))
                .setReturnDaoParameter(new DaoParameter("resultado", Types.INTEGER))
                .build(Integer.class);
    }

    @Override
    public CosteoDetalle obtenerPorId(Integer id) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_COSTEO_DETALLE_OBTENER_POR_ID)
                .addParameter(new DaoParameter("par_costeo_detalle_id", Types.INTEGER, id))
                .setDaoDefinition(costeoDetalleDaoDefinition)
                .build(CosteoDetalle.class);
    }
}
