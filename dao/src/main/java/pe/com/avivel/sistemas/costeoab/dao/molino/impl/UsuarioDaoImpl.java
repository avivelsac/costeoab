package pe.com.avivel.sistemas.costeoab.dao.molino.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoBuilder;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoGenerator;
import pe.com.avivel.sistemas.costeoab.commons.utils.dao.DaoParameter;
import pe.com.avivel.sistemas.costeoab.dao.utils.ConstantesMolinoDAO;
import pe.com.avivel.sistemas.costeoab.dao.molino.UsuarioDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.definition.UsuarioDaoDefinition;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Usuario;

import java.sql.Types;
import java.util.List;

@Repository
public class UsuarioDaoImpl extends DaoGenerator implements UsuarioDao {

    private UsuarioDaoDefinition usuarioDaoDefinition;

    @Autowired
    public UsuarioDaoImpl(JdbcTemplate jdbcTemplate, UsuarioDaoDefinition usuarioDaoDefinition) {
        super(jdbcTemplate);
        this.usuarioDaoDefinition = usuarioDaoDefinition;
    }

    @Override
    public List<Usuario> listarParaNotificar() {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_USUARIO_LISTAR_PARA_NOTIFICAR)
                .setDaoDefinition(usuarioDaoDefinition)
                .build();
    }

    @Override
    public Usuario autenticar(String usuarioNombre) {
        return DaoBuilder
                .getInstance(this)
                .setSchema(ConstantesMolinoDAO.SCHEMA_NAME)
                .setProcedureName(ConstantesMolinoDAO.SP_USUARIO_AUTENTICAR)
                .addParameter(new DaoParameter("par_usuario_nombre", Types.VARCHAR, usuarioNombre))
                .setDaoDefinition(usuarioDaoDefinition)
                .build(Usuario.class);
    }
}
