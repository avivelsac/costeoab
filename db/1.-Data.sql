# script inicial para correr sistema:

# limpiar las tablas
#delete from bd_molino.precio;
delete from bd_molino.formula_detalle;
delete from bd_molino.formula;
delete from bd_molino.costeo_detalle;
delete from bd_molino.costeo;
delete from bd_molino.envio_detalle;
delete from bd_molino.envio;

# generar las plantillas
set @roma_id = 1;
set @ava_id = 2;
insert into bd_molino.formula(formula_id,
                              formula_codigo,
                              formula_descripcion,
                              formula_empresa_id,
                              formula_cantidad,
                              formula_tipo,
                              formula_estado,
                              usuario_creacion,
                              fecha_creacion)
values
(1, 'PTEABA040001', 'POSTURA I 103G', @roma_id, 0, 'P', 1, 0, now()),
(2, 'PTEABA040002', 'POSTURA I 100G', @roma_id, 0, 'P', 1, 0, now()),
(3, 'PTEABA030003', 'PRE-POSTURA', @roma_id, 0, 'P', 1, 0, now()),
(4, 'PTEABA030002', 'CRECIMIENTO', @roma_id, 0, 'P', 1, 0, now()),
(5, 'PTEABA030001', 'PRE INICIO', @roma_id, 0, 'P', 1, 0, now()),
(6, 'PTEABA040003', 'POSTURA II', @roma_id, 0, 'P', 1, 0, now()),

(7, 'PTEABA040001', 'POSTURA I 103G', @ava_id, 0, 'P', 1, 0, now()),
(8, 'PTEABA040002', 'POSTURA I 100G', @ava_id, 0, 'P', 1, 0, now()),
(9, 'PTEABA030003', 'PRE-POSTURA', @ava_id, 0, 'P', 1, 0, now()),
(10, 'PTEABA030002', 'CRECIMIENTO', @ava_id, 0, 'P', 1, 0, now()),
(11, 'PTEABA030001', 'PRE INICIO', @ava_id, 0, 'P', 1, 0, now()),
(12, 'PTEABA040003', 'POSTURA II', @ava_id, 0, 'P', 1, 0, now());
