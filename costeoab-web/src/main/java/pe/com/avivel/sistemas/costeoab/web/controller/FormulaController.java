package pe.com.avivel.sistemas.costeoab.web.controller;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pe.com.avivel.sistemas.costeoab.dao.molino.FormulaDao;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Formula;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.FormulaDetalle;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanResult;
import pe.com.avivel.sistemas.costeoab.model.utils.Datatable;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroFormula;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroFormulaDetalle;
import pe.com.avivel.sistemas.costeoab.web.service.EmpresaService;
import pe.com.avivel.sistemas.costeoab.web.service.FormulaDetalleService;
import pe.com.avivel.sistemas.costeoab.web.service.FormulaService;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesStatus;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesWeb;
import pe.com.avivel.sistemas.costeoab.web.util.Util;

@Controller
@RequestMapping("formulas")
@RequiredArgsConstructor
public class FormulaController {

    private static final Logger log = LoggerFactory.getLogger(FormulaController.class);

    private final FormulaService formulaService;
    private final FormulaDetalleService formulaDetalleService;
    private final EmpresaService empresaService;

    @GetMapping({"", "/"})
    public String formulaPage(Model model) {
        model.addAttribute("plantillas", formulaService.listarTodos(new FiltroFormula("P", ConstantesStatus.ACTIVO)));
        model.addAttribute("empresas", empresaService.listarTodos());
        return ConstantesWeb.PAGE_FORMULAS;
    }

    @GetMapping("detalle/{formulaId}")
    public String detallePage(Model model, @PathVariable("formulaId") Integer formulaId) {
        model.addAttribute("empresas", empresaService.listarTodos());
        model.addAttribute("formula", formulaService.obtenerPorId(formulaId));
        return ConstantesWeb.PAGE_FORMULAS_DETALLE;
    }

    @ResponseBody
    @GetMapping("obtenerPlantilla/{plantillaId}")
    public BeanResult obtenerPlantilla(@PathVariable("plantillaId") Integer plantillaId) {
        BeanResult beanResult = new BeanResult();
        try {
            beanResult.setEstado(true);
            beanResult.setResult(formulaService.obtenerPorId(plantillaId));
        } catch (Exception e) {
            log.error("Error al obtener Plantilla, consulte con soporte. <- {0}", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al obtener Plantilla, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("guardarFormula")
    public BeanResult guardarFormula(@RequestBody Formula formula) {
        BeanResult beanResult = new BeanResult();
        try {
            formula.setFormulaEstado(ConstantesStatus.ACTIVO);
            formula.setAuditoria(Util.obtenerAuditoria());
            formula.setFormulaTipo("F");
            Integer resultado = formulaService.guardarFormula(formula);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("F\u00F3rmula guardada correctamente");
                beanResult.setResult(resultado);
                return beanResult;
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al guardar F\u00F3rmula, consulte con soporte.");
                return beanResult;
            }
        } catch (Exception e) {
            log.error("### Error al guardar  F\u00F3rmula, consulte con soporte. {0}", e);
            beanResult.setMensaje("Error al guardar  F\u00F3rmula, consulte con soporte");
            beanResult.setEstado(false);
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("actualizarFormula")
    public BeanResult actualizarFormula(@RequestBody Formula formula) {
        BeanResult beanResult = new BeanResult();
        try {
            formula.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = formulaService.mantenimiento(formula);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("F\u00F3rmula actualizada correctamente");
                beanResult.setResult(resultado);
                return beanResult;
            } else if (resultado == -4) {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al actualizar, la F\u00F3rmula est\u00E1 siendo utilizada.");
                beanResult.setResult(resultado);
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al actualizar F\u00F3rmula, consulte con soporte.");
                return beanResult;
            }
        } catch (Exception e) {
            log.error("### Error al actualizar F\u00F3rmula, consulte con soporte. {0}", e);
            beanResult.setMensaje("Error al actualizar F\u00F3rmula, consulte con soporte");
            beanResult.setEstado(false);
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("clonarFormula")
    public BeanResult clonarFormula(@RequestBody Formula formula) {
        BeanResult beanResult = new BeanResult();
        try {
            formula.setFormulaEstado(ConstantesStatus.ACTIVO);
            formula.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = formulaService.guardarFormula(formula);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("F\u00F3rmula clonada correctamente");
                beanResult.setResult(resultado);
                return beanResult;
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al clonar F\u00F3rmula, consulte con soporte.");
                return beanResult;
            }
        } catch (Exception e) {
            log.error("### Error al clonar F\u00F3rmula, consulte con soporte. {0}", e);
            beanResult.setMensaje("Error al clonar F\u00F3rmula, consulte con soporte");
            beanResult.setEstado(false);
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("eliminarFormula")
    public BeanResult eliminarFormula(@RequestBody Formula formula) {
        BeanResult beanResult = new BeanResult();
        try {
            formula.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = formulaService.mantenimiento(formula);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("F\u00F3rmula eliminada correctamente");
                beanResult.setResult(resultado);
                return beanResult;
            } else if (resultado == -4) {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al eliminar, la F\u00F3rmula est\u00E1 siendo utilizada.");
                beanResult.setResult(resultado);
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al eliminada F\u00F3rmula, consulte con soporte.");
                return beanResult;
            }
        } catch (Exception e) {
            log.error("### Error al eliminada F\u00F3rmula, consulte con soporte. {0}", e);
            beanResult.setMensaje("Error al eliminada F\u00F3rmula, consulte con soporte");
            beanResult.setEstado(false);
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @GetMapping("obtenerFormula/{formulaId}")
    public BeanResult formulaPlantilla(@PathVariable("formulaId") Integer formulaId) {
        BeanResult beanResult = new BeanResult();
        try {
            beanResult.setEstado(true);
            beanResult.setResult(formulaService.obtenerPorId(formulaId));
        } catch (Exception e) {
            log.error("Error al obtener F\u00F3rmula, consulte con soporte. <- {0}", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al obtener F\u00F3rmula, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("guardarDetalle")
    public BeanResult guardarDetalle(@RequestBody FormulaDetalle formulaDetalle) {
        BeanResult beanResult = new BeanResult();
        try {
            formulaDetalle.setFormulaDetalleEstado(ConstantesStatus.ACTIVO);
            formulaDetalle.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = formulaDetalleService.mantenimiento(formulaDetalle);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Detalle guardado correctamente");
                beanResult.setResult(resultado);
                return beanResult;
            } else if (resultado == -4) {
                beanResult.setEstado(false);
                beanResult.setMensaje("No se puede guardar el Detalle, porque la F\u00F3rmula esta siendo utilizada.");
                beanResult.setResult(resultado);
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al guardar Detalle, consulte con soporte.");
                beanResult.setResult(resultado);
                return beanResult;
            }
        } catch (Exception e) {
            log.error("### Error al guardar Detalle, consulte con soporte. {0}", e);
            beanResult.setMensaje("Error al guardar Detalle, consulte con soporte");
            beanResult.setEstado(false);
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("actualizarDetallePrecios")
    public BeanResult actualizarDetallePrecios(@RequestBody FormulaDetalle formulaDetalle) {
        BeanResult beanResult = new BeanResult();
        try {
            formulaDetalle.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = formulaDetalleService.actualizarPrecios(formulaDetalle);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Precios actualizados correctamente.");
                beanResult.setResult(resultado);
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al actualizar Precios, consulte con soporte.");
                beanResult.setResult(resultado);
            }
        } catch (Exception e) {
            log.error("### Error al actualizar Precios, consulte con soporte. {0}", e);
            beanResult.setMensaje("Error al actualizar Precios, consulte con soporte");
            beanResult.setEstado(false);
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @DeleteMapping("eliminarDetalle")
    public BeanResult eliminarDetalle(@RequestBody FormulaDetalle formulaDetalle) {
        BeanResult beanResult = new BeanResult();
        try {
            formulaDetalle.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = formulaDetalleService.mantenimiento(formulaDetalle);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Detalle eliminado correctamente.");
                beanResult.setResult(resultado);
                return beanResult;
            } else if (resultado == -4) {
                beanResult.setEstado(false);
                beanResult.setMensaje("No se puede guardar el Detalle, porque la F\u00F3rmula esta siendo utilizada.");
                beanResult.setResult(resultado);
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al eliminar Detalle, consulte con soporte.");
                return beanResult;
            }
        } catch (Exception e) {
            log.error("Error al eliminar Detalle, consulte con soporte. <- {0}", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al eliminar Detalle, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @GetMapping("obtenerDetalle/{detalleId}")
    public BeanResult obtenerDetalle(@PathVariable("detalleId") Integer detalleId) {
        BeanResult beanResult = new BeanResult();
        try {
            beanResult.setEstado(true);
            beanResult.setResult(formulaDetalleService.obtenerPorId(detalleId));
        } catch (Exception e) {
            log.error("Error al obtener Detalle, consulte con soporte. <- {0}", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al obtener Detalle, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("listar")
    public Datatable listar(FiltroFormula filtroFormula) {
        filtroFormula.setEstado(ConstantesStatus.ACTIVO);
        filtroFormula.setTipo("F");
        Integer contar = formulaService.contarFiltro(filtroFormula);
        Datatable datatable = new Datatable();
        datatable.setRecordsFiltered(contar);
        datatable.setRecordsTotal(contar);
        datatable.setData(formulaService.listarFiltro(filtroFormula));
        return datatable;
    }

    @ResponseBody
    @PostMapping("listarDetalles")
    public Datatable listarDetalle(FiltroFormulaDetalle filtroFormulaDetalle) {
        Datatable datatable = new Datatable();
        Integer contar = formulaDetalleService.contarFiltro(filtroFormulaDetalle);
        datatable.setRecordsTotal(contar);
        datatable.setRecordsFiltered(contar);
        datatable.setData(formulaDetalleService.listarFiltro(filtroFormulaDetalle));
        return datatable;
    }
}
