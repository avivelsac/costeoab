package pe.com.avivel.sistemas.costeoab.web.controller;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Precio;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanResult;
import pe.com.avivel.sistemas.costeoab.model.utils.Datatable;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroArticulo;
import pe.com.avivel.sistemas.costeoab.web.service.ArticuloService;
import pe.com.avivel.sistemas.costeoab.web.service.PrecioService;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesStatus;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesWeb;
import pe.com.avivel.sistemas.costeoab.web.util.Util;

@Controller
@RequestMapping("articulos")
@RequiredArgsConstructor
public class ArticuloController {

    private static final Logger log = LoggerFactory.getLogger(ArticuloController.class);

    private final ArticuloService articuloService;
    private final PrecioService precioService;

    @GetMapping({"", "/"})
    public String articuloPage() {
        return ConstantesWeb.PAGE_ARTICULOS;
    }

    @ResponseBody
    @GetMapping("obtenerArticulo/{articuloId}")
    public BeanResult obtenerArticulo(@PathVariable("articuloId") Integer articuloId) {
        BeanResult beanResult = new BeanResult();
        try {
            beanResult.setEstado(true);
            beanResult.setResult(articuloService.obtenerPorId(articuloId));
        } catch (Exception e) {
            log.error("Error al obtener Articulo, consulte con soporte. <- {0}", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al obtener Articulo, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("guardarPrecios")
    public BeanResult guardarPrecios(@RequestBody Precio precio) {
        BeanResult beanResult = new BeanResult();
        try {
            precio.setPrecioEstado(ConstantesStatus.ACTIVO);
            precio.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = precioService.mantenimiento(precio);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Precios guardados");
                beanResult.setResult(resultado);
                return beanResult;
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al guardar Precios, consulte con soporte.");
                beanResult.setResult(resultado);
                return beanResult;
            }
        } catch (Exception e) {
            log.error("Error al guardar Precios, consulte con soporte. <- {0}", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al guardar Precios, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("listar")
    public Datatable listar(FiltroArticulo filtroArticulo) {
        filtroArticulo.setDescripcion(Util.stringMultiwordsFilter(filtroArticulo.getDescripcion()));
        Datatable datatable = new Datatable();
        Integer contar = articuloService.contarFiltro(filtroArticulo);
        datatable.setRecordsTotal(contar);
        datatable.setRecordsFiltered(contar);
        datatable.setData(articuloService.listarFiltro(filtroArticulo));
        return datatable;
    }
}
