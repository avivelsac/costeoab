package pe.com.avivel.sistemas.costeoab.web.util;

public class ConstantesStatus {

    public static final Integer PENDIENTE = 1;
    public static final Integer APROBADO = 2;
    public static final Integer ANULADO = 3;
    public static final Integer CERRADO = 4;

    public static final Integer ACTIVO = 1;
    public static final Integer INACTIVO = -1;

}
