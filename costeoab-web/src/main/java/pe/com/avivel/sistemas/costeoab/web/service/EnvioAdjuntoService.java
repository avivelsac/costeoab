package pe.com.avivel.sistemas.costeoab.web.service;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.EnvioAdjunto;

import java.util.List;

public interface EnvioAdjuntoService {

    List<EnvioAdjunto> listarPorEnvioId(Integer envioId);

    Integer mantenimiento(EnvioAdjunto envioAdjunto);

    Integer mantenimiento(MultipartFile archivo, EnvioAdjunto envioAdjunto);
}
