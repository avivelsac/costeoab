package pe.com.avivel.sistemas.costeoab.web.controller;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Costeo;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.CosteoDetalle;
import pe.com.avivel.sistemas.costeoab.model.utils.*;
import pe.com.avivel.sistemas.costeoab.web.service.CosteoDetalleService;
import pe.com.avivel.sistemas.costeoab.web.service.CosteoService;
import pe.com.avivel.sistemas.costeoab.web.service.EmpresaService;
import pe.com.avivel.sistemas.costeoab.web.service.FormulaService;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesStatus;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesWeb;
import pe.com.avivel.sistemas.costeoab.web.util.Util;

@Controller
@RequestMapping("costeos")
@RequiredArgsConstructor
public class CosteoController {

    private static final Logger log = LoggerFactory.getLogger(CosteoController.class);

    private final FormulaService formulaService;
    private final CosteoService costeoService;
    private final CosteoDetalleService costeoDetalleService;
    private final EmpresaService empresaService;

    @GetMapping({"", "/"})
    public String costeoPage(Model model) {
        model.addAttribute("empresas", empresaService.listarTodos());
        return ConstantesWeb.PAGE_COSTEOS;
    }

    @GetMapping("detalle/{costeoId}")
    public String detallePage(Model model, @PathVariable("costeoId") Integer costeoId) {
        model.addAttribute("empresas", empresaService.listarTodos());

        Costeo costeo = costeoService.obtenerPorId(costeoId);
        model.addAttribute("costeo", costeo);

        FiltroFormula filtroFormula = new FiltroFormula();
        filtroFormula.setTipo("F");
        filtroFormula.setEstado(ConstantesStatus.ACTIVO);
        filtroFormula.setAnio(costeo.getEnvio().getEnvioAnio());
        filtroFormula.setSemana(costeo.getEnvio().getEnvioSemana());
        filtroFormula.setEmpresaId(costeo.getEnvio().getEmpresa().getEmpresaId());
        model.addAttribute("formulas", formulaService.listarTodos(filtroFormula));

        return ConstantesWeb.PAGE_COSTEOS_DETALLE;
    }

    @ResponseBody
    @PostMapping("guardarCosteo")
    public BeanResult guardarCosteo(@RequestBody Costeo costeo) {
        BeanResult beanResult = new BeanResult();
        try {
            costeo.setCosteoEstado(ConstantesStatus.ACTIVO);
            costeo.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = costeoService.mantenimiento(costeo);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Costeo guardado correctamente.");
                beanResult.setResult(resultado);
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al guardar Costeo, consulte con soporte.");
                beanResult.setResult(resultado);
            }
        } catch (Exception e) {
            log.error("### Error al guardar Costeo, consulte con soporte.", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al guardar Costeo, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @DeleteMapping("eliminarCosteo")
    public BeanResult eliminarCosteo(@RequestBody Costeo costeo) {
        BeanResult beanResult = new BeanResult();
        try {
            costeo.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = costeoService.mantenimiento(costeo);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Costeo eliminado correctamente.");
                beanResult.setResult(resultado);
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al eliminar Costeo, consulte con soporte.");
                beanResult.setResult(resultado);
            }
        } catch (Exception e) {
            log.error("### Error al eliminar Costeo, consulte con soporte.", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al eliminar Costeo, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("generarFactura")
    public BeanResult generarFactura(@RequestBody Costeo costeo) {
        BeanResult beanResult = new BeanResult();
        try {
            costeo.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = costeoService.generarFactura(costeo);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Factura generada correctamente.");
                beanResult.setResult(resultado);
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al generar Factura, consulte con soporte.");
                beanResult.setResult(resultado);
            }
        } catch (Exception e) {
            log.error("### Error al generar Factura, consulte con soporte.", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al generar Factura, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("guardarDetalle")
    public BeanResult guardarDetalle(@RequestBody CosteoDetalle costeoDetalle) {
        BeanResult beanResult = new BeanResult();
        try {
            costeoDetalle.setCosteoDetalleEstado(ConstantesStatus.ACTIVO);
            costeoDetalle.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = costeoDetalleService.mantenimiento(costeoDetalle);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Detalle guardado correctamente.");
                beanResult.setResult(resultado);
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al guardar Detalle, consulte con soporte.");
                beanResult.setResult(resultado);
            }
        } catch (Exception e) {
            log.error("### Error al guardar Costeo, consulte con soporte.", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al guardar Detalle, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @DeleteMapping("eliminarDetalle")
    public BeanResult eliminarDetalle(@RequestBody CosteoDetalle costeoDetalle) {
        BeanResult beanResult = new BeanResult();
        try {
            costeoDetalle.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = costeoDetalleService.mantenimiento(costeoDetalle);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Detalle eliminado correctamente.");
                beanResult.setResult(resultado);
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al eliminar Detalle, consulte con soporte.");
                beanResult.setResult(resultado);
            }
        } catch (Exception e) {
            log.error("### Error al eliminar Detalle, consulte con soporte.", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al eliminar Detalle, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @GetMapping("obtenerDetalle/{detalleId}")
    public BeanResult obtenerDetalle(@PathVariable("detalleId") Integer detalleId) {
        BeanResult beanResult = new BeanResult();
        try {
            beanResult.setEstado(true);
            beanResult.setResult(costeoDetalleService.obtenerPorId(detalleId));
        } catch (Exception e) {
            log.error("### Error al obtener Detalle, consulte con soporte.", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al obtener Detalle, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("listar")
    public Datatable listar(FiltroCosteo filtroCosteo) {
        Datatable datatable = new Datatable();
        Integer contar = costeoService.contarFiltro(filtroCosteo);
        datatable.setRecordsTotal(contar);
        datatable.setRecordsFiltered(contar);
        datatable.setData(costeoService.listarFiltro(filtroCosteo));
        return datatable;
    }

    @ResponseBody
    @PostMapping("listarDetalles")
    public Datatable listarDetalles(FiltroCosteoDetalle filtroCosteoDetalle) {
        Datatable datatable = new Datatable();
        Integer contar = costeoDetalleService.contarFiltro(filtroCosteoDetalle);
        datatable.setRecordsTotal(contar);
        datatable.setRecordsFiltered(contar);
        datatable.setData(costeoDetalleService.listarFiltro(filtroCosteoDetalle));
        return datatable;
    }
}
