package pe.com.avivel.sistemas.costeoab.web.service;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.Envio;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroEnvio;

import java.util.List;

public interface EnvioService {

    List<Envio> listarFiltro(FiltroEnvio filtroEnvio);

    Integer contarFiltro(FiltroEnvio filtroEnvio);

    Integer mantenimiento(Envio envio);

    Integer generarCosteo(Envio envio);

    Envio obtenerPorId(Integer id);
}
