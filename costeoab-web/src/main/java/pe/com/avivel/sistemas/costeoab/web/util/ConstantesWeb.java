package pe.com.avivel.sistemas.costeoab.web.util;

public class ConstantesWeb {

    public static final String EMPTY = "";
    public static final String LOCALE_DEFAULT = "es";

    // COLORS
    public static final String COLOR_ANULADO = "#ed5565";
    public static final String COLOR_PENDIENTE = "#f8ac59";
    public static final String COLOR_CERRADO = "#1ab394";

    // WEB
    public static final String SESSION_USUARIO = "usuario";

    // PAGES
    public static final String PAGE_LOGIN = "login";
    public static final String PAGE_INICIO = "inicio";

    public static final String PAGE_ARTICULOS = "maestras/articulo_lista";
    public static final String PAGE_PLANTILLAS = "maestras/plantilla_lista";
    public static final String PAGE_PLANTILLAS_DETALLE = "maestras/plantilla_detalle";
    public static final String PAGE_FORMULAS = "maestras/formula_lista";
    public static final String PAGE_FORMULAS_DETALLE = "maestras/formula_detalle";
    public static final String PAGE_ENVIOS = "envio_lista";
    public static final String PAGE_ENVIOS_DETALLE = "envio_detalle";
    public static final String PAGE_COSTEOS = "costeo_lista";
    public static final String PAGE_COSTEOS_DETALLE = "costeo_detalle";
}
