package pe.com.avivel.sistemas.costeoab.web.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.com.avivel.sistemas.costeoab.dao.molino.PrecioDao;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Precio;
import pe.com.avivel.sistemas.costeoab.web.service.PrecioService;

@Service
@RequiredArgsConstructor
public class PrecioServiceImpl implements PrecioService {

    private final PrecioDao precioDao;

    @Override
    @Transactional
    public Integer mantenimiento(Precio precio) {
        return precioDao.mantenimiento(precio);
    }
}
