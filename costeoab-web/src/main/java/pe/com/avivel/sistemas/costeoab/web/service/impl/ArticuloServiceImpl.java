package pe.com.avivel.sistemas.costeoab.web.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pe.com.avivel.sistemas.costeoab.dao.molino.ArticuloDao;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Articulo;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroArticulo;
import pe.com.avivel.sistemas.costeoab.web.service.ArticuloService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ArticuloServiceImpl implements ArticuloService {

    private final ArticuloDao articuloDao;

    @Override
    public List<Articulo> listarFiltro(FiltroArticulo filtroArticulo) {
        return articuloDao.listarFiltro(filtroArticulo);
    }

    @Override
    public Integer contarFiltro(FiltroArticulo filtroArticulo) {
        return articuloDao.contarFiltro(filtroArticulo);
    }

    @Override
    public Articulo obtenerPorId(Integer id) {
        return articuloDao.obtenerPorId(id);
    }
}
