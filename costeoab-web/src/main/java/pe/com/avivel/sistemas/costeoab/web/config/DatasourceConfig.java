package pe.com.avivel.sistemas.costeoab.web.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import pe.com.avivel.sistemas.costeoab.web.properties.CosteoABDBProperties;

import javax.sql.DataSource;

@Configuration
@RequiredArgsConstructor
public class DatasourceConfig {

    private final CosteoABDBProperties properties;

    @Bean
    public DataSource dataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(properties.getDatasource().getDriver());
        hikariConfig.setJdbcUrl(properties.getDatasource().getUrl());
        hikariConfig.setUsername(properties.getDatasource().getUsername());
        hikariConfig.setPassword(properties.getDatasource().getPassword());
        hikariConfig.setAutoCommit(properties.getDatasource().isAutoCommit());

        hikariConfig.setMaximumPoolSize(properties.getDatasource().getMaximumPoolSize());
        hikariConfig.setConnectionTestQuery(properties.getDatasource().getConnectionTestQuery());
        hikariConfig.setPoolName(properties.getDatasource().getPoolName());

        return new HikariDataSource(hikariConfig);
    }

    @Bean
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }
}
