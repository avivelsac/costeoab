package pe.com.avivel.sistemas.costeoab.web.util;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.thymeleaf.expression.Numbers;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Usuario;
import pe.com.avivel.sistemas.costeoab.model.utils.Auditoria;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Util {

    private static final String MENOS_INFINITO = "01/01/1900";
    private static final String MAS_INFINITO = "31/12/9999";

    public static HttpServletRequest getRequest() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        if (requestAttributes != null) {
            HttpServletRequest request = requestAttributes.getRequest();
            return request;
        }
        return null;
    }

    public static HttpSession getSession() {
        return Util.getRequest().getSession(false);
    }

    public static String getMessage(MessageSource messageSource, String key) {
        return getMessage(messageSource, key, null);
    }

    public static String getMessage(MessageSource messageSource, String key, Object[] params) {
        try {
            if (messageSource != null) {
                return messageSource.getMessage(key, params, LocaleContextHolder.getLocale());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ConstantesWeb.EMPTY;
    }

    public static Usuario autenticado() {
        Usuario usuario = (Usuario) getSession().getAttribute(ConstantesWeb.SESSION_USUARIO);
        if (usuario != null) {
            return usuario;
        }
        return null;
    }

    public static Auditoria obtenerAuditoria() {
        Auditoria auditoria = new Auditoria();
        auditoria.setUsuarioCreacion(autenticado().getUsuarioId());
        auditoria.setUsuarioModificacion(autenticado().getUsuarioId());
        return auditoria;
    }

    public static String obtenerRolUsuario() {
        String rol = ConstantesWeb.EMPTY;
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for (GrantedAuthority authority : authorities) {
            rol = authority.getAuthority();
        }
        return rol;
    }

    public static void actualizarRolUsuario(String rol) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<GrantedAuthority> updateAuthorities = new ArrayList<>(auth.getAuthorities());
        updateAuthorities.add(new SimpleGrantedAuthority(SpringUtil.PREFIJO_ROL + rol));
        Authentication newAuth = new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials(), updateAuthorities);
        SecurityContextHolder.getContext().setAuthentication(newAuth);
    }

    public static MediaType fromExtension(String extension) {
        switch (extension.toLowerCase()) {
            case "jpg":
            case "jpeg":
                return MediaType.IMAGE_JPEG;
            case "png":
                return MediaType.IMAGE_PNG;
            case "gif":
                return MediaType.IMAGE_GIF;
            case "pdf":
                return MediaType.APPLICATION_PDF;
            default:
                return null;
        }
    }

    public static Date stringToDate(String stringFecha, String stringFormato) {
        if (StringUtils.isEmpty(stringFecha)) {
            return null;
        }
        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern(stringFormato);
            return DateTime.parse(stringFecha, formatter).toDate();
        } catch (Exception e) {
            return null;
        }
    }

    public static Date stringToDate(String stringFecha) {
        if (StringUtils.isEmpty(stringFecha)) {
            return null;
        }
        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
            return DateTime.parse(stringFecha, formatter).toDate();
        } catch (Exception e) {
            return null;
        }
    }

    public static Date stringToDateInfinity(String stringFecha, String stringFormato, String signo) {
        if (StringUtils.isEmpty(stringFecha)) {
            if (signo.equals("+")) {
                return stringToDate(MAS_INFINITO);
            } else if (signo.equals("-")) {
                return stringToDate(MENOS_INFINITO);
            }
        }

        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern(stringFormato);
            return DateTime.parse(stringFecha, formatter).toDate();
        } catch (Exception e) {
            return null;
        }
    }

    public static String nowDateFormat(String format) {
        try {
            return new SimpleDateFormat(format).format(new Date());
        } catch (Exception e) {
            return UUID.randomUUID().toString().replace("-", "");
        }
    }

    public static String dateToString(Date fecha, String stringFormato) {
        if (fecha == null) {
            return null;
        }

        SimpleDateFormat format = new SimpleDateFormat(stringFormato);
        return format.format(fecha);
    }

    public static String randomUUID() {
        return (UUID.randomUUID().toString().replaceAll("-", ""));
    }

    public static String stringMultiwordsFilter(String sentence) {
        if (StringUtils.isEmpty(sentence)) {
            return null;
        }

        String d = sentence.trim().replaceAll(" +", " ");
        return d.replaceAll(" ", ".+");
    }

    public static String numberToString(Number number, Integer decimales) {
        if (number == null) {
            return "";
        }

        Numbers numbers = new Numbers(Locale.US);
        return numbers.formatDecimal(number, 1, decimales, "POINT");
    }

    public static String leerFichero(String pathFile) {
        StringBuffer buffer = new StringBuffer();
        String line = org.apache.commons.lang3.StringUtils.EMPTY;
        FileReader fReader = null;
        BufferedReader bReader;
        try {
            fReader = new FileReader(pathFile);
            bReader = new BufferedReader(fReader);
            while ((line = bReader.readLine()) != null) {
                buffer.append(line);
            }
            bReader.close();
            fReader.close();
            return buffer.toString();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        } finally {
            try {
                if (null != fReader) {
                    fReader.close();
                }
            } catch (Exception e2) {
                return null;
            }
        }

        return null;
    }
}
