package pe.com.avivel.sistemas.costeoab.web.service;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.Costeo;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroCosteo;

import java.util.List;

public interface CosteoService {

    List<Costeo> listarFiltro(FiltroCosteo filtroCosteo);

    Integer contarFiltro(FiltroCosteo filtroCosteo);

    Integer mantenimiento(Costeo costeo);

    Integer generarFactura(Costeo costeo);

    Costeo obtenerPorId(Integer id);
}
