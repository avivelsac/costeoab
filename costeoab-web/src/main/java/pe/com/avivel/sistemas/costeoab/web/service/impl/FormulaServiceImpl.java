package pe.com.avivel.sistemas.costeoab.web.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.com.avivel.sistemas.costeoab.dao.molino.FormulaDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.FormulaDetalleDao;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Formula;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.FormulaDetalle;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroFormula;
import pe.com.avivel.sistemas.costeoab.web.service.FormulaService;
import pe.com.avivel.sistemas.costeoab.web.util.Util;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FormulaServiceImpl implements FormulaService {

    private final FormulaDao formulaDao;
    private final FormulaDetalleDao formulaDetalleDao;

    @Override
    public List<Formula> listarTodos(FiltroFormula filtroFormula) {
        return formulaDao.listarTodos(filtroFormula);
    }

    @Override
    public List<Formula> listarFiltro(FiltroFormula filtroFormula) {
        return formulaDao.listarFiltro(filtroFormula);
    }

    @Override
    public Integer contarFiltro(FiltroFormula filtroFormula) {
        return formulaDao.contarFiltro(filtroFormula);
    }

    @Override
    @Transactional
    public Integer mantenimiento(Formula formula) {
        return formulaDao.mantenimiento(formula);
    }

    @Override
    @Transactional
    public Integer guardarFormula(Formula formula) {
        Integer resultado;

        try {
            Formula formulaComun = formulaDao.obtenerPorId(formula.getFormulaParentId());
            formulaComun.setFormulaCodigo(formula.getFormulaCodigo());
            formulaComun.setEmpresa(formula.getEmpresa());
            formulaComun.setFormulaDescripcion(formula.getFormulaDescripcion());
            formulaComun.setFormulaObservaciones(formula.getFormulaObservaciones());
            formulaComun.setFormulaSemana(formula.getFormulaSemana());
            formulaComun.setFormulaAnio(formula.getFormulaAnio());
            formulaComun.setAuditoria(Util.obtenerAuditoria());
            formulaComun.setFormulaTipo(formula.getFormulaTipo());
            formulaComun.setFormulaEstado(formula.getFormulaEstado());
            formulaComun.setFormulaParentId(formula.getFormulaParentId());
            formulaComun.setAccion(1);

            resultado = formulaDao.mantenimiento(formulaComun);
            if (resultado > 0) {
                List<FormulaDetalle> detalles = formulaDetalleDao.listarPlantillaPorFormulaId(formula.getFormulaParentId());
                for (FormulaDetalle d : detalles) {
                    d.setAuditoria(Util.obtenerAuditoria());
                    d.setFormulaDetallePrecio(d.getArticulo().getPrecio().getPrecioValor());
                    d.setFormulaDetalleFlete(d.getArticulo().getPrecio().getPrecioFleteValor());
                    d.setAccion(1);
                    d.setFormula(new Formula(resultado));
                    formulaDetalleDao.mantenimiento(d);
                }
            } else {
                throw new RuntimeException("Error al guardar F\u00F3rmula");
            }
        } catch (Exception e) {
            throw new RuntimeException("Error al guardar F\u00F3rmula", e);
        }

        return resultado;
    }

    @Override
    public Formula obtenerPorId(Integer id) {
        return formulaDao.obtenerPorId(id);
    }
}
