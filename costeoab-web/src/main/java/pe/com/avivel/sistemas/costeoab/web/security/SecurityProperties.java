package pe.com.avivel.sistemas.costeoab.web.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("sec")
public class SecurityProperties {

    public boolean valid(List<GrantedAuthority> authorities, String... strings) {
        boolean ret = false;

        for (GrantedAuthority ga : authorities) {
            for (String s : strings) {
                if (ga.getAuthority().equalsIgnoreCase(s)) {
                    ret = true;
                    break;
                }
            }
        }

        return ret;
    }
}
