package pe.com.avivel.sistemas.costeoab.web.security;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesSession;
import pe.com.avivel.sistemas.costeoab.web.util.SpringUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class LoginFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    private static final Logger log = LoggerFactory.getLogger(LoginFailureHandler.class);

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        String urlToRedirect = request.getContextPath() + SpringUtil.AUTHENTICATION_FAILURE_URL;

        log.info("#LoginFailureHandler.onAuthenticationFailure...");
        log.info("#Clase de exception: {}", exception.getClass());
        log.info("#Mensaje de la exception: {}", exception.getMessage());

        request.getSession().setAttribute(ConstantesSession.SESSION_KEY_MSG_SECURITY_ERROR, obtenerKeyMensajeExcepcion(exception));

        response.sendRedirect(urlToRedirect);
    }

    private String obtenerKeyMensajeExcepcion(AuthenticationException exception){
        String keyMsgError = StringUtils.EMPTY;

        if (exception instanceof BadCredentialsException) {
            keyMsgError = "Credenciales incorrectas";
        } else if (exception instanceof DisabledException) {
            keyMsgError = "Cuenta deshabilitada";
        } else if (exception instanceof AccountExpiredException) {
            keyMsgError = "Cuenta expirada";
        } else if (exception instanceof CredentialsExpiredException) {
            keyMsgError = "Credenciales expiradas";
        } else if (exception instanceof LockedException) {
            keyMsgError = "Cuenta bloqueada";
        }

        return keyMsgError;
    }
}
