package pe.com.avivel.sistemas.costeoab.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import pe.com.avivel.sistemas.costeoab.web.properties.CosteoABIPProperties;

import java.util.Properties;

@Configuration
public class JavaMailConfig {

    private final CosteoABIPProperties properties;

    @Autowired
    public JavaMailConfig(CosteoABIPProperties properties) {
        this.properties = properties;
    }

    @Bean
    public JavaMailSender mailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setUsername(properties.getMail().getCorreo());
        mailSender.setPassword(properties.getMail().getContrasena());
        mailSender.setHost(properties.getMail().getHost());
        mailSender.setPort(Integer.parseInt(properties.getMail().getPuerto()));


        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.smtp.auth", properties.getMail().isAuth());
        props.put("mail.smtp.starttls.enable", properties.getMail().isStarttls());
        props.put("mail.transport.protocol", properties.getMail().getProtocolo());

        //props.put("mail.smtp.ssl.trust", "*");
        //props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        //props.put("mail.smtp.socketFactory.fallback", "false");

        mailSender.setJavaMailProperties(props);

        return mailSender;
    }
}
