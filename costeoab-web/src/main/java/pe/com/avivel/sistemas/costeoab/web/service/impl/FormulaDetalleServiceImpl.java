package pe.com.avivel.sistemas.costeoab.web.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.com.avivel.sistemas.costeoab.dao.molino.FormulaDetalleDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.PrecioDao;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.FormulaDetalle;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Precio;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroFormulaDetalle;
import pe.com.avivel.sistemas.costeoab.web.service.FormulaDetalleService;
import pe.com.avivel.sistemas.costeoab.web.service.PrecioService;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesStatus;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FormulaDetalleServiceImpl implements FormulaDetalleService {

    private final FormulaDetalleDao formulaDetalleDao;
    private final PrecioDao precioDao;

    @Override
    public List<FormulaDetalle> listarFiltro(FiltroFormulaDetalle filtroFormulaDetalle) {
        return formulaDetalleDao.listarFiltro(filtroFormulaDetalle);
    }

    @Override
    public Integer contarFiltro(FiltroFormulaDetalle filtroFormulaDetalle) {
        return formulaDetalleDao.contarFiltro(filtroFormulaDetalle);
    }

    @Override
    @Transactional
    public Integer mantenimiento(FormulaDetalle formulaDetalle) {
        return formulaDetalleDao.mantenimiento(formulaDetalle);
    }

    @Override
    public FormulaDetalle obtenerPorId(Integer id) {
        return formulaDetalleDao.obtenerPorId(id);
    }

    @Override
    @Transactional
    public Integer actualizarPrecios(FormulaDetalle formulaDetalle) {
        Integer resultado;

        try {
            Precio precio = formulaDetalle.getArticulo().getPrecio();
            precio.setAuditoria(formulaDetalle.getAuditoria());
            precio.setPrecioEstado(ConstantesStatus.ACTIVO);
            resultado = precioDao.mantenimiento(precio);
            if (resultado > 0) {
                resultado = formulaDetalleDao.mantenimiento(formulaDetalle);
            } else {
                throw new RuntimeException("Error al actualizar precios");
            }
        } catch (Exception e) {
            throw new RuntimeException("Error al actualizar precios", e);
        }

        return resultado;
    }
}
