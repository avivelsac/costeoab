package pe.com.avivel.sistemas.costeoab.web.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import pe.com.avivel.sistemas.costeoab.dao.molino.EnvioAdjuntoDao;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.EnvioAdjunto;
import pe.com.avivel.sistemas.costeoab.web.properties.CosteoABIPProperties;
import pe.com.avivel.sistemas.costeoab.web.service.EnvioAdjuntoService;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class EnvioAdjuntoServiceImpl implements EnvioAdjuntoService {

    private final EnvioAdjuntoDao envioAdjuntoDao;
    private final CosteoABIPProperties properties;

    @Override
    public List<EnvioAdjunto> listarPorEnvioId(Integer envioId) {
        return envioAdjuntoDao.listarPorEnvioId(envioId);
    }

    @Override
    @Transactional
    public Integer mantenimiento(EnvioAdjunto envioAdjunto) {
        return envioAdjuntoDao.mantenimiento(envioAdjunto);
    }

    @Override
    @Transactional
    public Integer mantenimiento(MultipartFile archivo, EnvioAdjunto envioAdjunto) {
        Integer resultado;
        try {
            resultado = envioAdjuntoDao.mantenimiento(envioAdjunto);
            if (resultado > 0) {
                File file = new File(properties.getServer().getUploads().getPublico() + envioAdjunto.getEnvioAdjuntoNombreServer());
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }

                log.info("### Archivo adjunto: -> {}", archivo);

                if (archivo != null) {
                    BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
                    byte[] data = archivo.getBytes();
                    bufferedOutputStream.write(data);
                    bufferedOutputStream.close();
                } else {
                    throw new Exception("Error al adjuntar archivo, no se pudo cargar, contacte con soporte.");
                }

                //Files.write(file.toPath(), archivo.getBytes());
            } else {
                throw new Exception("Error al guardar Adjunto, contacte con soporte.");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return resultado;
    }
}
