package pe.com.avivel.sistemas.costeoab.web.controller;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Formula;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.FormulaDetalle;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanResult;
import pe.com.avivel.sistemas.costeoab.model.utils.Datatable;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroFormula;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroFormulaDetalle;
import pe.com.avivel.sistemas.costeoab.web.service.EmpresaService;
import pe.com.avivel.sistemas.costeoab.web.service.FormulaDetalleService;
import pe.com.avivel.sistemas.costeoab.web.service.FormulaService;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesStatus;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesWeb;
import pe.com.avivel.sistemas.costeoab.web.util.Util;

@Controller
@RequestMapping("plantillas")
@RequiredArgsConstructor
public class PlantillaController {

    private static final Logger log = LoggerFactory.getLogger(PlantillaController.class);

    private final FormulaService formulaService;
    private final EmpresaService empresaService;
    private final FormulaDetalleService formulaDetalleService;

    @GetMapping({"", "/"})
    public String plantillaPage(Model model) {
        model.addAttribute("empresas", empresaService.listarTodos());
        return ConstantesWeb.PAGE_PLANTILLAS;
    }

    @GetMapping("detalle/{plantillaId}")
    public String detallePage(Model model, @PathVariable("plantillaId") Integer plantillaId) {
        Formula plantilla = formulaService.obtenerPorId(plantillaId);

        model.addAttribute("empresas", empresaService.listarTodos());
        model.addAttribute("plantilla", plantilla);

        return ConstantesWeb.PAGE_PLANTILLAS_DETALLE;
    }

    @ResponseBody
    @GetMapping("obtenerPlantilla/{plantillaId}")
    public BeanResult formulaPlantilla(@PathVariable("plantillaId") Integer plantillaId) {
        BeanResult beanResult = new BeanResult();
        try {
            beanResult.setEstado(true);
            beanResult.setResult(formulaService.obtenerPorId(plantillaId));
        } catch (Exception e) {
            log.error("Error al obtener F\u00F3rmula, consulte con soporte. <- {0}", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al obtener F\u00F3rmula, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("guardarPlantilla")
    public BeanResult guardarPlantilla(@RequestBody Formula plantilla) {
        BeanResult beanResult = new BeanResult();
        try {
            plantilla.setFormulaTipo("P");
            plantilla.setFormulaEstado(ConstantesStatus.ACTIVO);
            plantilla.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = formulaService.mantenimiento(plantilla);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Plantilla guardada correctamente");
                beanResult.setResult(resultado);
                return beanResult;
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al guardar Plantilla, consulte con soporte.");
                return beanResult;
            }
        } catch (Exception e) {
            log.error("### Error al guardar Plantilla, consulte con soporte. {0}", e);
            beanResult.setMensaje("Error al guardar Plantilla, consulte con soporte");
            beanResult.setEstado(false);
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("clonarPlantilla")
    public BeanResult clonarPlantilla(@RequestBody Formula plantilla) {
        BeanResult beanResult = new BeanResult();
        try {
            plantilla.setFormulaEstado(ConstantesStatus.ACTIVO);
            plantilla.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = formulaService.guardarFormula(plantilla);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Plantilla clonada correctamente");
                beanResult.setResult(resultado);
                return beanResult;
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al clonar Plantilla, consulte con soporte.");
                return beanResult;
            }
        } catch (Exception e) {
            log.error("### Error al clonar Plantilla, consulte con soporte. {0}", e);
            beanResult.setMensaje("Error al clonar Plantilla, consulte con soporte");
            beanResult.setEstado(false);
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("eliminarPlantilla")
    public BeanResult eliminarPlantilla(@RequestBody Formula plantilla) {
        BeanResult beanResult = new BeanResult();
        try {
            plantilla.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = formulaService.mantenimiento(plantilla);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Plantilla eliminada correctamente");
                beanResult.setResult(resultado);
                return beanResult;
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al eliminar Plantilla, consulte con soporte.");
                return beanResult;
            }
        } catch (Exception e) {
            log.error("### Error al eliminar Plantilla, consulte con soporte. {0}", e);
            beanResult.setMensaje("Error al eliminar Plantilla, consulte con soporte");
            beanResult.setEstado(false);
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @GetMapping("obtenerDetalle/{detalleId}")
    public BeanResult obtenerDetalle(@PathVariable("detalleId") Integer detalleId) {
        BeanResult beanResult = new BeanResult();
        try {
            beanResult.setEstado(true);
            beanResult.setResult(formulaDetalleService.obtenerPorId(detalleId));
        } catch (Exception e) {
            log.error("Error al obtener Detalle, consulte con soporte. <- {0}", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al obtener Detalle, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @DeleteMapping("eliminarDetalle")
    public BeanResult eliminarDetalle(@RequestBody FormulaDetalle formulaDetalle) {
        BeanResult beanResult = new BeanResult();
        try {
            formulaDetalle.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = formulaDetalleService.mantenimiento(formulaDetalle);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Detalle eliminado correctamente.");
                beanResult.setResult(resultado);
                return beanResult;
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al eliminar Detalle, consulte con soporte.");
                return beanResult;
            }
        } catch (Exception e) {
            log.error("Error al eliminar Detalle, consulte con soporte. <- {0}", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al eliminar Detalle, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("guardarDetalle")
    public BeanResult guardarDetalle(@RequestBody FormulaDetalle formulaDetalle) {
        BeanResult beanResult = new BeanResult();
        try {
             formulaDetalle.setFormulaDetalleEstado(ConstantesStatus.ACTIVO);
             formulaDetalle.setAuditoria(Util.obtenerAuditoria());
             Integer resultado = formulaDetalleService.mantenimiento(formulaDetalle);
             if (resultado > 0) {
                 beanResult.setEstado(true);
                 beanResult.setMensaje("Detalle guardado correctamente");
                 beanResult.setResult(resultado);
                 return beanResult;
             } else {
                 beanResult.setEstado(false);
                 beanResult.setMensaje("Error al guardar Detalle, consulte con soporte.");
                 return beanResult;
             }
        } catch (Exception e) {
            log.error("### Error al guardar Detalle, consulte con soporte. {0}", e);
            beanResult.setMensaje("Error al guardar Detalle, consulte con soporte");
            beanResult.setEstado(false);
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("listar")
    public Datatable listar(FiltroFormula filtroFormula) {
        filtroFormula.setEstado(ConstantesStatus.ACTIVO);
        filtroFormula.setTipo("P");
        Integer contar = formulaService.contarFiltro(filtroFormula);
        Datatable datatable = new Datatable();
        datatable.setRecordsFiltered(contar);
        datatable.setRecordsTotal(contar);
        datatable.setData(formulaService.listarFiltro(filtroFormula));
        return datatable;
    }

    @ResponseBody
    @PostMapping("listarDetalles")
    public Datatable listarDetalle(FiltroFormulaDetalle filtroFormulaDetalle) {
        Datatable datatable = new Datatable();
        Integer contar = formulaDetalleService.contarFiltro(filtroFormulaDetalle);
        datatable.setRecordsTotal(contar);
        datatable.setRecordsFiltered(contar);
        datatable.setData(formulaDetalleService.listarFiltro(filtroFormulaDetalle));
        return datatable;
    }
}
