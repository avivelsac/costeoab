package pe.com.avivel.sistemas.costeoab.web.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pe.com.avivel.sistemas.costeoab.dao.molino.EmpresaDao;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Empresa;
import pe.com.avivel.sistemas.costeoab.web.service.EmpresaService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmpresaServiceImpl implements EmpresaService {

    private final EmpresaDao empresaDao;

    @Override
    public List<Empresa> listarTodos() {
        return empresaDao.listarTodos();
    }
}
