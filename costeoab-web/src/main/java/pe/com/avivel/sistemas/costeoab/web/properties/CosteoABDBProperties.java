package pe.com.avivel.sistemas.costeoab.web.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties("costeoab")
@PropertySource("file:${propertyHomeCosteoAB}/costeoab-db.properties")
public class CosteoABDBProperties {

    private DatasourceProperties datasource;

    @Getter
    @Setter
    public static class DatasourceProperties {

        private String driver;
        private String url;
        private String username;
        private String password;
        private boolean autoCommit;
        private int maximumPoolSize;
        private String connectionTestQuery;
        private String poolName;
    }
}
