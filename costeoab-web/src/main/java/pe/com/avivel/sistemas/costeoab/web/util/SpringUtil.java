package pe.com.avivel.sistemas.costeoab.web.util;

public class SpringUtil {

    public static final String AUTHENTICATION_FAILURE_URL = "/login?error=true";
    public static final String AUTHENTICATION_REGISTER_URL = "/login?registro";
    public static final String LOGIN_PROCESSING_URL = "/autenticar";
    public static final String LOGIN_PAGE = "/login";
    public static final String USERNAME_PARAMETER = "username";
    public static final String PASSWORD_PARAMETER = "password";
    public static final String LOGOUT_URL = "/cerrar-sesion";
    public static final String LOGOUT_SUCCESS_URL = "/login?logout";
    public static final String PREFIJO_ROL = "ROL_";

    public static final String JSESSIONID = "JSESSIONID";
    public static final String ACCES_DENIED_PAGE = "/login/403";
}
