package pe.com.avivel.sistemas.costeoab.web.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.com.avivel.sistemas.costeoab.dao.molino.CosteoDetalleDao;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Costeo;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.CosteoDetalle;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroCosteoDetalle;
import pe.com.avivel.sistemas.costeoab.web.service.CosteoDetalleService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CosteoDetalleServiceImpl implements CosteoDetalleService {

    private final CosteoDetalleDao costeoDetalleDao;

    @Override
    public List<CosteoDetalle> listarFiltro(FiltroCosteoDetalle filtroCosteoDetalle) {
        return costeoDetalleDao.listarFiltro(filtroCosteoDetalle);
    }

    @Override
    public Integer contarFiltro(FiltroCosteoDetalle filtroCosteoDetalle) {
        return costeoDetalleDao.contarFiltro(filtroCosteoDetalle);
    }

    @Override
    @Transactional
    public Integer mantenimiento(CosteoDetalle costeoDetalle) {
        return costeoDetalleDao.mantenimiento(costeoDetalle);
    }

    @Override
    @Transactional
    public Integer calcularFletes(Costeo costeo) {
        return costeoDetalleDao.calcularFletes(costeo);
    }

    @Override
    public CosteoDetalle obtenerPorId(Integer id) {
        return costeoDetalleDao.obtenerPorId(id);
    }
}
