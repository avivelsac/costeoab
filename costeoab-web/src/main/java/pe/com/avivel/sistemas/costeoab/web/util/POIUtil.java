package pe.com.avivel.sistemas.costeoab.web.util;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class POIUtil {

    /*public static void exportExcel(Map<String, Object[]> data, String finalName, HttpServletResponse response) {
        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet sheet = workbook.createSheet("Hoja");

        XSSFFont defaultFont = workbook.createFont();
        defaultFont.setFontHeightInPoints((short) 10);
        defaultFont.setFontName("Arial");
        defaultFont.setColor(IndexedColors.BLACK.getIndex());
        defaultFont.setBold(false);
        defaultFont.setItalic(false);

        XSSFFont fontTitle = workbook.createFont();
        fontTitle.setFontHeightInPoints((short) 10);
        fontTitle.setFontName("Arial");
        fontTitle.setColor(IndexedColors.BLACK.getIndex());
        fontTitle.setBold(true);
        fontTitle.setItalic(false);

        if (data == null) {
            data = new TreeMap<>();
        }

        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            Row row = sheet.createRow(rownum);

            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);

                if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }

                CellStyle style = workbook.createCellStyle();
                style.setWrapText(true);

                if (rownum == 0) {
                    style.setFont(fontTitle);
                } else {
                    style.setFont(defaultFont);
                    style.setWrapText(true);
                }
                cell.setCellStyle(style);
            }

            if (rownum == 1) {
                for (int i = 0; i < objArr.length; i++) {
                    sheet.autoSizeColumn(i);
                }
            }

            rownum++;
        }


        try {
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition", "attachment; filename=\"" + finalName + "\"");
            workbook.write(response.getOutputStream());
            workbook.close();
        }  catch (IOException e) {
            throw new RuntimeException("Error al generar archivo excel", e);
        }
    }*/

    public static void exportExcel(Map<String, Object[]> data, String finalName, HttpServletResponse response) {
        SXSSFWorkbook book = new SXSSFWorkbook();
        book.setCompressTempFiles(true);

        SXSSFSheet sheet = (SXSSFSheet) book.createSheet("Hoja");
        sheet.setRandomAccessWindowSize(100);

        Font defaultFont = book.createFont();
        defaultFont.setFontHeightInPoints((short) 10);
        defaultFont.setFontName("Arial");
        defaultFont.setColor(IndexedColors.BLACK.getIndex());
        defaultFont.setBold(false);
        defaultFont.setItalic(false);

        Font fontTitle = book.createFont();
        fontTitle.setFontHeightInPoints((short) 10);
        fontTitle.setFontName("Arial");
        fontTitle.setColor(IndexedColors.BLACK.getIndex());
        fontTitle.setBold(true);
        fontTitle.setItalic(false);

        if (data == null) {
            data = new TreeMap<>();
        }

        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            Row row = sheet.createRow(rownum);

            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);

                if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }

                CellStyle style = book.createCellStyle();
                style.setWrapText(true);

                if (rownum == 0) {
                    style.setFont(fontTitle);
                } else {
                    style.setFont(defaultFont);
                    style.setWrapText(true);
                }
                cell.setCellStyle(style);
            }

            if (rownum == 1) {
                for (int i = 0; i < objArr.length; i++) {
                    sheet.autoSizeColumn(i);
                }
            }

            rownum++;
        }


        try {
            response.setContentType("application/csv");
            response.setHeader("Content-disposition", "attachment; filename=\"" + finalName + "\"");
            book.write(response.getOutputStream());
            book.close();
        }  catch (IOException e) {
            throw new RuntimeException("Error al generar archivo excel", e);
        }
    }
}
