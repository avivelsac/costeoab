package pe.com.avivel.sistemas.costeoab.web.service;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.EnvioDetalle;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroEnvioDetalle;

import java.util.List;

public interface EnvioDetalleService {

    List<EnvioDetalle> listarFiltro(FiltroEnvioDetalle filtroEnvioDetalle);

    Integer contarFiltro(FiltroEnvioDetalle filtroEnvioDetalle);
}
