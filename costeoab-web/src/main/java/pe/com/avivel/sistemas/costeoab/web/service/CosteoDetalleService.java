package pe.com.avivel.sistemas.costeoab.web.service;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.Costeo;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.CosteoDetalle;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroCosteoDetalle;

import java.util.List;

public interface CosteoDetalleService {

    List<CosteoDetalle> listarFiltro(FiltroCosteoDetalle filtroCosteoDetalle);

    Integer contarFiltro(FiltroCosteoDetalle filtroCosteoDetalle);

    Integer mantenimiento(CosteoDetalle costeoDetalle);

    Integer calcularFletes(Costeo costeo);

    CosteoDetalle obtenerPorId(Integer id);
}
