package pe.com.avivel.sistemas.costeoab.web.controller;

import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.text.StringSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Envio;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.EnvioAdjunto;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Usuario;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanResult;
import pe.com.avivel.sistemas.costeoab.model.utils.Datatable;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroEnvio;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroEnvioDetalle;
import pe.com.avivel.sistemas.costeoab.web.properties.CosteoABIPProperties;
import pe.com.avivel.sistemas.costeoab.web.service.*;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesStatus;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesWeb;
import pe.com.avivel.sistemas.costeoab.web.util.EmailUtil;
import pe.com.avivel.sistemas.costeoab.web.util.Util;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("envios")
@RequiredArgsConstructor
public class EnvioController {

    private static final Logger log = LoggerFactory.getLogger(EnvioController.class);

    private final CosteoABIPProperties properties;
    private final EmailUtil emailUtil;
    private final EnvioService envioService;
    private final EnvioDetalleService envioDetalleService;
    private final EnvioAdjuntoService envioAdjuntoService;
    private final EmpresaService empresaService;
    private final UsuarioService usuarioService;

    @GetMapping({"", "/"})
    public String envioPage(Model model) {
        model.addAttribute("empresas", empresaService.listarTodos());
        return ConstantesWeb.PAGE_ENVIOS;
    }

    @GetMapping("detalle/{envioId}")
    public String detallePage(Model model,
                              @PathVariable("envioId") Integer envioId) {
        model.addAttribute("empresas", empresaService.listarTodos());
        model.addAttribute("envio", envioService.obtenerPorId(envioId));
        return ConstantesWeb.PAGE_ENVIOS_DETALLE;
    }

    @ResponseBody
    @PostMapping("guardarEnvio")
    public BeanResult guardarEnvio(@RequestBody Envio envio) {
        BeanResult beanResult = new BeanResult();
        try {
            envio.setEnvioEstado(ConstantesStatus.ACTIVO);
            envio.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = envioService.mantenimiento(envio);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Env\u00EDo guardado correctamente.");
                beanResult.setResult(resultado);

                new Thread(() -> enviarMail(resultado)).start();
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al guardar el Env\u00EDo, consulte con soporte.");
            }
        } catch (Exception e) {
            log.error("### Error al guardar el Env\u00EDo, consulte con soporte <- {0}", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al guardar el Env\u00EDo, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    private void enviarMail(Integer envioId) {
        Map<String, Object> map = new HashMap<>();
        map.put("numero", envioId);
        map.put("url", properties.getServer().getWeb().getUrlContextPath() + "envios/detalle/" + envioId);

        List<Usuario> usuarios = usuarioService.listarParaNotificar();
        String[] para = new String[usuarios.size()];
        for (int i = 0; i < usuarios.size(); i++) {
            para[i] = usuarios.get(i).getUsuarioCorreo();
        }

        String alias = properties.getMail().getAlias();
        String desde = properties.getMail().getCorreo();
        String asunto = "Env\u00EDo Nro " + envioId + " generado.";

        String pathTemplate = properties.getMail().getPlantillas().getEnvioGenerado();
        String template = Util.leerFichero(pathTemplate);
        String html;

        if (StringUtils.isEmpty(template)) {
            html = "No se encontro el formato de email";
            html += "<br>";
        } else {
            html = StringSubstitutor.replace(template, map);
        }

        emailUtil.sendMail(alias, desde, para, asunto, html);
    }

    @ResponseBody
    @PostMapping("generarCosteo")
    public BeanResult generarCosteo(@RequestBody Envio envio) {
        BeanResult beanResult = new BeanResult();
        try {
            envio.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = envioService.generarCosteo(envio);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Costeo generado correctamente.");
                beanResult.setResult(resultado);
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al generar Costeo, consulte con soporte.");
                beanResult.setResult(resultado);
            }
        } catch (Exception e) {
            log.error("### Error al generar Costeo, consulte con soporte <- {0}", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al generar Costeo, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @GetMapping("obtenerEnvio/{envioId}")
    public BeanResult obtenerEnvio(@PathVariable("envioId") Integer envioId) {
        BeanResult beanResult = new BeanResult();
        try {
            beanResult.setEstado(true);
            beanResult.setResult(envioService.obtenerPorId(envioId));
        } catch (Exception e) {
            log.error("### Error al obtener el Env\u00EDo, consulte con soporte <- {0}", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al obtener el Env\u00EDo, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @ResponseBody
    @PostMapping("eliminarEnvio")
    public BeanResult eliminarEnvio(@RequestBody Envio envio) {
        BeanResult beanResult = new BeanResult();
        try {
            envio.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = envioService.mantenimiento(envio);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Env\u00EDo eliminado correctamente.");
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al eliminar el Env\u00EDo, consulte con soporte.");
            }
        } catch (Exception e) {
            log.error("### Error al eliminar el Env\u00EDo, consulte con soporte <- {0}", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al eliminar el Env\u00EDo, consulte con soporte.");
            beanResult.setResult(e);
        }
        return beanResult;
    }

    @PostMapping("guardarAdjunto")
    public @ResponseBody BeanResult guardarAdjunto(@RequestParam("envioId") Integer envioId,
                                     @RequestParam("archivo") MultipartFile archivo) {
        BeanResult beanResult = new BeanResult();
        try {
            log.info("### Archivo adjunto controller: -> {}", archivo);

            String nombre = archivo.getOriginalFilename();
            String extension = FilenameUtils.getExtension(nombre);
            String tag = Util.nowDateFormat("yyyMMddhhmmss") + "-" + Util.randomUUID();
            String nombreServer = "envio_adjunto_" + tag + "." + extension;
            String contentType = archivo.getContentType();

            EnvioAdjunto adjunto = new EnvioAdjunto();
            adjunto.setEnvioAdjuntoNombre(nombre.toUpperCase());
            adjunto.setEnvioAdjuntoNombreServer(nombreServer.toUpperCase());
            adjunto.setEnvioAdjuntoEstado(ConstantesStatus.ACTIVO);
            adjunto.setEnvioAdjuntoExtension(extension.toUpperCase());
            adjunto.setEnvioAdjuntoTipo(contentType.toUpperCase());
            adjunto.setEnvio(new Envio(envioId));
            adjunto.setAuditoria(Util.obtenerAuditoria());
            adjunto.setAccion(1);

            Integer resultado = envioAdjuntoService.mantenimiento(archivo, adjunto);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Adjunto guardado correctamente.");
                beanResult.setResult(resultado);
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al guardar Adjunto, contacte con soporte.");
                beanResult.setResult(resultado);
            }
        } catch (Exception e) {
            log.error("Error al guardar Adjunto <- ", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al guardar Adjunto, contacte con soporte.");
            beanResult.setResult(e.getMessage());
        }
        return beanResult;
    }

    @ResponseBody
    @DeleteMapping("eliminarAdjunto")
    public BeanResult eliminarAdjunto(@RequestBody EnvioAdjunto envioAdjunto) {
        BeanResult beanResult = new BeanResult();
        try {
            envioAdjunto.setAuditoria(Util.obtenerAuditoria());
            Integer resultado = envioAdjuntoService.mantenimiento(envioAdjunto);
            if (resultado > 0) {
                beanResult.setEstado(true);
                beanResult.setMensaje("Adjunto eliminado correctamente.");
                beanResult.setResult(resultado);
            } else {
                beanResult.setEstado(false);
                beanResult.setMensaje("Error al eliminar Adjunto, contacte con soporte.");
                beanResult.setResult(resultado);
            }
        } catch (Exception e) {
            log.error("Error al eliminar Adjunto <- ", e);
            beanResult.setEstado(false);
            beanResult.setMensaje("Error al eliminar Adjunto, contacte con soporte.");
            beanResult.setResult(e.getMessage());
        }
        return beanResult;
    }

    @ResponseBody
    @GetMapping("previsualizarAdjunto")
    public void previsualizarAdjunto(@RequestParam("nombreServer") String nombreServer,
                                     @RequestParam("nombre") String nombre,
                                     @RequestParam("formato") String formato,
                                     HttpServletResponse response) throws IOException {
        String server = properties.getServer().getUploads().getPublico();

        FileInputStream inputStream = new FileInputStream(new File(server + nombreServer));
        response.setHeader("Content-Disposition", "inline; filename=\"" + nombre + "\"");

        if (formato.equalsIgnoreCase("PDF")) {
            response.setContentType("application/pdf; name=\"" + nombre + "\"");
        }

        IOUtils.copy(inputStream, response.getOutputStream());
        response.flushBuffer();
    }

    @GetMapping("descargarAdjunto")
    @ResponseBody
    public void archivoDescargar(@RequestParam("nombreServer") String nombreServer,
                                 @RequestParam("nombre") String nombre,
                                 HttpServletResponse response) throws IOException {
        String server = properties.getServer().getUploads().getPublico();

        FileInputStream inputStream = new FileInputStream(new File(server + nombreServer));
        response.setHeader("Content-Disposition", "attachment; filename=\"" + nombre + "\"");
        IOUtils.copy(inputStream, response.getOutputStream());
        response.flushBuffer();
    }

    @ResponseBody
    @PostMapping("listar")
    public Datatable listar(FiltroEnvio filtroEnvio) {
        filtroEnvio.setDesde(Util.stringToDateInfinity(filtroEnvio.getDesdeString(), "dd/MM/yyyy", "-"));
        filtroEnvio.setHasta(Util.stringToDateInfinity(filtroEnvio.getHastaString(), "dd/MM/yyyy", "+"));
        Datatable datatable = new Datatable();
        Integer contar = envioService.contarFiltro(filtroEnvio);
        datatable.setRecordsFiltered(contar);
        datatable.setRecordsTotal(contar);
        datatable.setData(envioService.listarFiltro(filtroEnvio));
        return datatable;
    }

    @ResponseBody
    @PostMapping("listarDetalles")
    public Datatable listarDetalles(FiltroEnvioDetalle filtroDetalleEnvio) {
        Datatable datatable = new Datatable();
        Integer contar = envioDetalleService.contarFiltro(filtroDetalleEnvio);
        datatable.setRecordsFiltered(contar);
        datatable.setRecordsTotal(contar);
        datatable.setData(envioDetalleService.listarFiltro(filtroDetalleEnvio));
        return datatable;
    }

    @ResponseBody
    @PostMapping("listarAdjuntos")
    public Datatable listarAdjuntos(Integer envioId) {
        List<EnvioAdjunto> adjuntos = envioAdjuntoService.listarPorEnvioId(envioId);
        Datatable datatable = new Datatable();
        Integer contar = adjuntos.size();
        datatable.setData(adjuntos);
        datatable.setRecordsFiltered(contar);
        datatable.setRecordsTotal(contar);
        return datatable;
    }
}
