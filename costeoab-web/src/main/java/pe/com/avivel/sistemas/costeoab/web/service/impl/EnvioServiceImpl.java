package pe.com.avivel.sistemas.costeoab.web.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.com.avivel.sistemas.costeoab.dao.molino.CosteoDetalleDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.EnvioDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.EnvioDetalleDao;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Costeo;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.CosteoDetalle;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Envio;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.EnvioDetalle;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroEnvio;
import pe.com.avivel.sistemas.costeoab.web.service.EnvioService;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesStatus;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EnvioServiceImpl implements EnvioService {

    private final EnvioDao envioDao;
    private final EnvioDetalleDao envioDetalleDao;
    private final CosteoDetalleDao costeoDetalleDao;

    @Override
    public List<Envio> listarFiltro(FiltroEnvio filtroEnvio) {
        return envioDao.listarFiltro(filtroEnvio);
    }

    @Override
    public Integer contarFiltro(FiltroEnvio filtroEnvio) {
        return envioDao.contarFiltro(filtroEnvio);
    }

    @Override
    @Transactional
    public Integer mantenimiento(Envio envio) {
        return envioDao.mantenimiento(envio);
    }

    @Override
    @Transactional
    public Integer generarCosteo(Envio envio) {
        Integer resultado = -1;
        Integer costeoId = -1;
        try {
            // se obtiene el "costeoId" para la accion: 4
             costeoId = envioDao.mantenimiento(envio);
             if (costeoId > 0) {
                 List<EnvioDetalle> envioDetalles = envioDetalleDao.listarPorEnvioId(envio.getEnvioId());
                 for (EnvioDetalle ed : envioDetalles) {
                     CosteoDetalle cd = new CosteoDetalle();
                     cd.setCosteo(new Costeo(costeoId));
                     cd.setFormula(ed.getFormula());
                     cd.setCosteoDetalleCantidad(ed.getFormula().getFormulaCantidadProducida().doubleValue());
                     cd.setCosteoDetalleFactorFlete(BigDecimal.valueOf(1.00));
                     cd.setAuditoria(envio.getAuditoria());
                     cd.setCosteoDetalleEstado(ConstantesStatus.ACTIVO);
                     cd.setAccion(1);

                     resultado = costeoDetalleDao.mantenimiento(cd);
                     if (resultado < 0) {
                         throw new RuntimeException("Error al generar Costeo, consulte con soporte");
                     }
                 }
             } else {
                 throw new RuntimeException("Error al generar Costeo, consulte con soporte");
             }
        } catch (Exception e) {
            throw new RuntimeException("Error al generar Costeo, consulte con soporte", e);
        }
        return costeoId;
    }



    @Override
    public Envio obtenerPorId(Integer id) {
        return envioDao.obtenerPorId(id);
    }
}
