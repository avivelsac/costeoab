package pe.com.avivel.sistemas.costeoab.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesSession;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesWeb;
import pe.com.avivel.sistemas.costeoab.web.util.Util;

@RequestMapping("/login")
@Controller
public class LoginController {

    private static final Logger log = LoggerFactory.getLogger(LoginController.class);

    @GetMapping({"", "/"})
    public String loginPage(Model model,
                            @RequestParam(value = "error", required = false) String error,
                            @RequestParam(value = "logout", required = false) String logout,
                            @RequestParam(value = "registro", required = false) String registro) {
        log.info("### login page ###");
        try {
            if (Util.autenticado() != null) {
                return "redirect:/";
            }

            if (error != null) {
                String msgError = Util.getSession().getAttribute(ConstantesSession.SESSION_KEY_MSG_SECURITY_ERROR).toString();
                model.addAttribute("msg", msgError);
            } else if (logout != null) {
                model.addAttribute("msg", "Has salido con exito");
            }
        } catch (Exception e) {
            log.error("### login?error session null <- {0}", e);
        }
        return ConstantesWeb.PAGE_LOGIN;
    }
}
