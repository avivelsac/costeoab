package pe.com.avivel.sistemas.costeoab.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import pe.com.avivel.sistemas.costeoab.web.security.DefaultPasswordEncoderFactories;
import pe.com.avivel.sistemas.costeoab.web.security.LoginFailureHandler;
import pe.com.avivel.sistemas.costeoab.web.security.LoginSuccessHandler;
import pe.com.avivel.sistemas.costeoab.web.security.UsuarioDetailsService;
import pe.com.avivel.sistemas.costeoab.web.util.SpringUtil;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UsuarioDetailsService usuarioDetailsService;
    private final LoginSuccessHandler loginSuccessHandler;
    private final LoginFailureHandler loginFailureHandler;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public WebSecurityConfig(UsuarioDetailsService usuarioDetailsService, LoginSuccessHandler loginSuccessHandler, LoginFailureHandler loginFailureHandler) {
        this.usuarioDetailsService = usuarioDetailsService;
        this.loginSuccessHandler = loginSuccessHandler;
        this.loginFailureHandler = loginFailureHandler;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                .loginPage(SpringUtil.LOGIN_PAGE)
                .loginProcessingUrl(SpringUtil.LOGIN_PROCESSING_URL)
                .usernameParameter(SpringUtil.USERNAME_PARAMETER)
                .passwordParameter(SpringUtil.PASSWORD_PARAMETER)
                .successHandler(loginSuccessHandler)
                .failureHandler(loginFailureHandler)
                .permitAll();
        http.logout()
                .logoutRequestMatcher(new AntPathRequestMatcher(SpringUtil.LOGOUT_URL))
                .logoutSuccessUrl(SpringUtil.LOGOUT_SUCCESS_URL)
                .deleteCookies(SpringUtil.JSESSIONID)
                .permitAll();
        http.authorizeRequests()
                .antMatchers("/login**").permitAll()
                .anyRequest().authenticated();
        http.rememberMe();
        http.csrf().disable();
        http.headers().frameOptions().sameOrigin();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        if (passwordEncoder == null) {
            passwordEncoder = DefaultPasswordEncoderFactories.createDelegatingPasswordEncoder();
        }
        return passwordEncoder;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(usuarioDetailsService).passwordEncoder(passwordEncoder());
    }
}
