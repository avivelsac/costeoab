package pe.com.avivel.sistemas.costeoab.web.controller;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesWeb;
import pe.com.avivel.sistemas.costeoab.web.util.Util;

@Controller
@RequiredArgsConstructor
public class InicioController {

    private static final Logger log = LoggerFactory.getLogger(InicioController.class);

    @GetMapping({"", "/"})
    public String indexPage() {
        return "redirect:/" + ConstantesWeb.PAGE_INICIO;
    }

    @GetMapping("inicio")
    public String inicioPage() {
        log.info("### inicio page ###");
        log.info("{}", Util.autenticado());
        return ConstantesWeb.PAGE_INICIO;
    }

}
