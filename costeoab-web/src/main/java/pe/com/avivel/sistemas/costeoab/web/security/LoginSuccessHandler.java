package pe.com.avivel.sistemas.costeoab.web.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Usuario;
import pe.com.avivel.sistemas.costeoab.web.util.ConstantesSession;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private static final Logger log = LoggerFactory.getLogger(LoginSuccessHandler.class);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
        UsuarioDetails usuarioDetails = (UsuarioDetails) authentication.getPrincipal();

        this.putUsuarioInSession(usuarioDetails.getUsuario(), request);

        super.onAuthenticationSuccess(request, response, authentication);
    }

    private void putUsuarioInSession(Usuario usuario, HttpServletRequest request) {
        log.info("### usuario en sesion ###");
        request.getSession().setAttribute(ConstantesSession.SESSION_USUARIO, usuario);
    }

}
