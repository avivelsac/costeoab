package pe.com.avivel.sistemas.costeoab.web.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Rol;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Usuario;
import pe.com.avivel.sistemas.costeoab.web.service.UsuarioService;
import pe.com.avivel.sistemas.costeoab.web.util.SpringUtil;

import java.util.*;

@Service
public class UsuarioDetailsService implements UserDetailsService {

    private final UsuarioService usuarioService;

    @Autowired
    public UsuarioDetailsService(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario = usuarioService.autenticar(username);
        if (usuario == null) {
            throw new UsernameNotFoundException("Credenciales incorrectas");
        }

        Set<GrantedAuthority> setAuths = new HashSet<>(0);
        for (Rol rol : usuario.getRoles()) {
            setAuths.add(new SimpleGrantedAuthority(SpringUtil.PREFIJO_ROL + rol.getRolNombre()));
        }

        List<GrantedAuthority> authorities = new ArrayList<>(setAuths);

        return new UsuarioDetails(
                usuario,
                usuario.getUsuarioNombre(),
                usuario.getUsuarioPassword(),
                Boolean.TRUE,
                Boolean.TRUE,
                Boolean.TRUE,
                Boolean.TRUE,
                authorities);
    }
}
