package pe.com.avivel.sistemas.costeoab.web.util;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
public class JasperReportUtil {

    private final ResourceLoader resourceLoader;

    @Autowired
    public JasperReportUtil(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public static OutputStream exportarReportePDF(String url, Map<String, Object> parametros, Collection lista, HttpServletResponse response) throws IOException, JRException {
        if (parametros == null) {
            parametros = new HashMap<>();
        }

        OutputStream outputStream = response.getOutputStream();

        JRDataSource jasperSource = new JRBeanCollectionDataSource(lista, false);
        JasperReport jasperReport = JasperCompileManager.compileReport(url);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametros, jasperSource);
        JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

        return outputStream;
    }

    public void exportPdfFile(String url, Collection collection, OutputStream outputStream) throws JRException, IOException {
        String path = resourceLoader.getResource("classpath:reports/reporte_reclamo.jrxml").getURI().getPath();

        JasperReport jasperReport = JasperCompileManager.compileReport(path);

        Map<String, Object> parameters = new HashMap<String, Object>();

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(collection, false);

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

        JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
    }

    public static void jasperToPDF(String urlReporte, Collection list, Map<String, Object> parametros, String nombre) throws JRException {
        try {
            // Compilar reporte desde .jrxml to .jasper
            JasperReport jasperReport = JasperCompileManager.compileReport(urlReporte);

            // Obtener data source
            JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(list, false);

            // parametros
            if (parametros == null) {
                parametros = new HashMap<>();
            }

            // llenar reporte
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametros, jrBeanCollectionDataSource);

            // exportar el report al archivo PDF
            JasperExportManager.exportReportToPdfFile(jasperPrint, nombre);
        } catch (JRException e) {
            throw new JRException(e);
        }
    }
}
