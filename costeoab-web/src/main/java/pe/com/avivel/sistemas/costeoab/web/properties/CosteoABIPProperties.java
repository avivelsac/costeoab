package pe.com.avivel.sistemas.costeoab.web.properties;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Getter
@Setter
@RequiredArgsConstructor
@Component("prop")
@ConfigurationProperties("costeoab")
@PropertySource("file:${propertyHomeCosteoAB}/costeoab-ip.properties")
public class CosteoABIPProperties {

    private final Environment env;

    private ServerProperties server;
    private WebProperties web;
    private MailProperties mail;
    private JobProperties job;

    public String path(String key) {
        return env.getProperty(key);
    }

    public String src(String key, String resource) {
        return env.getProperty(key) + resource;
    }

    public String srcVersion(String key, String resource) {
        return env.getProperty(key) + resource + "?" + (UUID.randomUUID().toString().replaceAll("-", ""));
    }

    @Getter
    @Setter
    public static class ServerProperties {

        private String ip;
        private String port;
        private WebProperties web;
        private ResourcesProperties resources;
        private UploadsProperties uploads;
    }

    @Getter
    @Setter
    public static class WebProperties {

        private String contextPath;
        private String urlContextPath;
    }

    @Getter
    @Setter
    public static class ResourcesProperties {

        private String url;
        private String contextPath;
        private String urlContextPath;
        private UploadServerProperties uploads;
    }

    @Getter
    @Setter
    public static class UploadServerProperties {

        private String web;
    }

    @Getter
    @Setter
    public static class UploadsProperties {

        private String publico;
        private String privado;
    }

    @Getter
    @Setter
    public static class MailProperties {

        private String correo;
        private String contrasena;
        private String alias;
        private String host;
        private String protocolo;
        private String puerto;
        private boolean auth;
        private boolean starttls;
        private String correoinfo;
        private PlantillasProperties plantillas;
    }

    @Getter
    @Setter
    public static class PlantillasProperties {

        private String envioGenerado;
    }

    @Getter
    @Setter
    public static class JobProperties {

        private String frecuencia;
    }
}
