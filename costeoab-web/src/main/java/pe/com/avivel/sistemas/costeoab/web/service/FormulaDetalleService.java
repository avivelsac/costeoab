package pe.com.avivel.sistemas.costeoab.web.service;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.FormulaDetalle;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroFormulaDetalle;

import java.util.List;

public interface FormulaDetalleService {

    List<FormulaDetalle> listarFiltro(FiltroFormulaDetalle filtroFormulaDetalle);

    Integer contarFiltro(FiltroFormulaDetalle filtroFormulaDetalle);

    Integer mantenimiento(FormulaDetalle formulaDetalle);

    FormulaDetalle obtenerPorId(Integer id);

    Integer actualizarPrecios(FormulaDetalle formulaDetalle);
}
