package pe.com.avivel.sistemas.costeoab.web.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pe.com.avivel.sistemas.costeoab.dao.molino.RolDao;
import pe.com.avivel.sistemas.costeoab.dao.molino.UsuarioDao;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Usuario;
import pe.com.avivel.sistemas.costeoab.web.service.UsuarioService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UsuarioServiceImpl implements UsuarioService {

    private final UsuarioDao usuarioDao;
    private final RolDao rolDao;

    @Override
    public List<Usuario> listarParaNotificar() {
        return usuarioDao.listarParaNotificar();
    }

    @Override
    public Usuario autenticar(String nombreUsuario) {
        Usuario usuario = usuarioDao.autenticar(nombreUsuario);
        if (!StringUtils.isEmpty(usuario)) {
            usuario.setRoles(rolDao.listarPorUsuarioId(usuario.getUsuarioId()));
        }
        return usuario;
    }
}
