package pe.com.avivel.sistemas.costeoab.web.util;

public class ConstantesSession {

    public static final String SESSION_USUARIO = "usuario";
    public static final String SESSION_MENU_MAP = "menus";
    public static final String SESSION_KEY_MSG_SECURITY_ERROR = "msgErrorCode";
    public static final String SESSION_MAPA_ROLES_URL = "maparoles";
}
