package pe.com.avivel.sistemas.costeoab.web.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pe.com.avivel.sistemas.costeoab.dao.molino.EnvioDetalleDao;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.EnvioDetalle;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroEnvioDetalle;
import pe.com.avivel.sistemas.costeoab.web.service.EnvioDetalleService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EnvioDetalleServiceImpl implements EnvioDetalleService {

    private final EnvioDetalleDao envioDetalleDao;

    @Override
    public List<EnvioDetalle> listarFiltro(FiltroEnvioDetalle filtroEnvioDetalle) {
        return envioDetalleDao.listarFiltro(filtroEnvioDetalle);
    }

    @Override
    public Integer contarFiltro(FiltroEnvioDetalle filtroEnvioDetalle) {
        return envioDetalleDao.contarFiltro(filtroEnvioDetalle);
    }
}
