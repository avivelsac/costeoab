package pe.com.avivel.sistemas.costeoab.web.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.com.avivel.sistemas.costeoab.dao.molino.CosteoDao;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Costeo;
import pe.com.avivel.sistemas.costeoab.model.utils.FiltroCosteo;
import pe.com.avivel.sistemas.costeoab.web.service.CosteoService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CosteoServiceImpl implements CosteoService {

    private final CosteoDao costeoDao;

    @Override
    public List<Costeo> listarFiltro(FiltroCosteo filtroCosteo) {
        return costeoDao.listarFiltro(filtroCosteo);
    }

    @Override
    public Integer contarFiltro(FiltroCosteo filtroCosteo) {
        return costeoDao.contarFiltro(filtroCosteo);
    }

    @Override
    @Transactional
    public Integer mantenimiento(Costeo costeo) {
        return costeoDao.mantenimiento(costeo);
    }

    @Override
    @Transactional
    public Integer generarFactura(Costeo costeo) {
        return costeoDao.generarFactura(costeo);
    }

    @Override
    public Costeo obtenerPorId(Integer id) {
        return costeoDao.obtenerPorId(id);
    }
}
