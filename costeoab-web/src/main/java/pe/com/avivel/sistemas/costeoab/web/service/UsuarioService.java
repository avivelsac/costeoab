package pe.com.avivel.sistemas.costeoab.web.service;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.Usuario;

import java.util.List;

public interface UsuarioService {

    List<Usuario> listarParaNotificar();

    Usuario autenticar(String nombreUsuario);
}
