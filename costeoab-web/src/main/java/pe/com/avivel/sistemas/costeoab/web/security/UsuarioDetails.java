package pe.com.avivel.sistemas.costeoab.web.security;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import pe.com.avivel.sistemas.costeoab.model.molino.bean.Usuario;

import java.util.Collection;

@Getter
@Setter
public class UsuarioDetails extends User {

    private Usuario usuario;

    public UsuarioDetails(Usuario usuario,
                          String username,
                          String password,
                          boolean enabled,
                          boolean accountNonExpired,
                          boolean credentialsNonExpired,
                          boolean accountNonLocked,
                          Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.usuario = usuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }
}
