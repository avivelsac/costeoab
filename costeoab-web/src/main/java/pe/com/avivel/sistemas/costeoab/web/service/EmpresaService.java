package pe.com.avivel.sistemas.costeoab.web.service;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.Empresa;

import java.util.List;

public interface EmpresaService {

    List<Empresa> listarTodos();
}
