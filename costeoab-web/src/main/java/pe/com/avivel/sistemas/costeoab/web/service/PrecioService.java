package pe.com.avivel.sistemas.costeoab.web.service;

import pe.com.avivel.sistemas.costeoab.model.molino.bean.Precio;

public interface PrecioService {

    Integer mantenimiento(Precio precio);
}
