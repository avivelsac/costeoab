package pe.com.avivel.sistemas.costeoab.model.molino.bean;

import lombok.Data;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanBase;

@Data
public class EnvioAdjunto extends BeanBase {

    private Integer envioAdjuntoId;
    private String envioAdjuntoNombre;
    private String envioAdjuntoNombreServer;
    private String envioAdjuntoTipo;
    private String envioAdjuntoExtension;
    private Integer envioAdjuntoEstado;
    private Envio envio;

    public Envio getEnvio() {
        if (envio == null) envio = new Envio();
        return envio;
    }
}
