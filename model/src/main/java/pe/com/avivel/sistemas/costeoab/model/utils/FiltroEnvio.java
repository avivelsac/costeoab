package pe.com.avivel.sistemas.costeoab.model.utils;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class FiltroEnvio implements Serializable {

    private static final long serialVersionUID = -5605424559159341397L;

    private Integer start;
    private Integer length;
    private Date desde;
    private Date hasta;
    private String desdeString;
    private String hastaString;
    private Integer empresaId;
    private Integer anio;
    private Integer semana;
    private Integer estado;
    private boolean soloActivos;
}
