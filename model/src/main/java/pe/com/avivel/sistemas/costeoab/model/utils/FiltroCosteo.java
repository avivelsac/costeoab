package pe.com.avivel.sistemas.costeoab.model.utils;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class FiltroCosteo implements Serializable {

    private static final long serialVersionUID = 3370425616563906171L;

    private Integer length;
    private Integer start;
    private Integer empresaId;
    private Integer anio;
    private Integer semana;
}
