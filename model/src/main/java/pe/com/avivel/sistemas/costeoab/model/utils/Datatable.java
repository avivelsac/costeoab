package pe.com.avivel.sistemas.costeoab.model.utils;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Datatable implements Serializable {

    private static final long serialVersionUID = -483073509104182984L;

    private Integer recordsTotal;
    private Integer recordsFiltered;
    private Object data;
}
