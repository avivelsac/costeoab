package pe.com.avivel.sistemas.costeoab.model.molino.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanBase;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Costeo extends BeanBase {

    private static final long serialVersionUID = -1957052524092319074L;

    private Integer costeoId;
    private Empresa empresa;
    private Integer costeoAnio;
    private Integer costeoSemana;
    private String costeoObservaciones;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date costeoFechaTipoCambio;
    private Double costeoTipoCambio;
    private Double costeoPrecioFleteViaje;
    private Integer costeoEstado;
    private Envio envio;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date costeoFacturaFechaEmision;
    private String costeoFacturaSerie;
    private String costeoFacturaNumero;
    private Boolean costeoFacturaGenerada;
    private String costeoFacturaGuiaRemision;

    public Costeo(Integer costeoId) {
        this.costeoId = costeoId;
    }

    public Empresa getEmpresa() {
        if (empresa == null) empresa = new Empresa();
        return empresa;
    }
}
