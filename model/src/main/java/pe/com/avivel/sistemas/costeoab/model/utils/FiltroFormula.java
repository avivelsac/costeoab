package pe.com.avivel.sistemas.costeoab.model.utils;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class FiltroFormula implements Serializable {

    private static final long serialVersionUID = 59874648341237353L;

    private Integer start;
    private Integer length;
    private String codigo;
    private String descripcion;
    private Integer empresaId;
    private String tipo;
    private Integer anio;
    private Integer semana;
    private Integer estado;

    public FiltroFormula() {
    }

    public FiltroFormula(String tipo, Integer estado) {
        this.tipo = tipo;
        this.estado = estado;
    }

    public FiltroFormula(String tipo, Integer anio, Integer semana, Integer estado) {
        this.tipo = tipo;
        this.anio = anio;
        this.semana = semana;
        this.estado = estado;
    }
}
