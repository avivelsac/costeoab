package pe.com.avivel.sistemas.costeoab.model.utils;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class FiltroRequerimientoDetalle implements Serializable {

    private static final long serialVersionUID = 8398375091533407567L;

    private Integer length;
    private Integer start;
    private Integer requerimientoId;
    private boolean soloActivos;
}
