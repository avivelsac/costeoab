package pe.com.avivel.sistemas.costeoab.model.molino.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanBase;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EnvioDetalle extends BeanBase {

    private static final long serialVersionUID = 6786442969966670449L;

    private Integer envioDetalleId;
    private Formula formula;
    private Envio envio;
    private Integer envioDetalleEstado;

    public Formula getFormula() {
        if (formula == null) formula = new Formula();
        return formula;
    }
}
