package pe.com.avivel.sistemas.costeoab.model.utils;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class FiltroCosteoDetalle implements Serializable {

    private static final long serialVersionUID = 4058716793288483678L;

    private Integer length;
    private Integer start;
    private Integer costeoId;
}
