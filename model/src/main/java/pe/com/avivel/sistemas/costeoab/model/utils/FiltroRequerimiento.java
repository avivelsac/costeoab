package pe.com.avivel.sistemas.costeoab.model.utils;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class FiltroRequerimiento implements Serializable {

    private static final long serialVersionUID = 8442437731772053770L;

    private Integer start;
    private Integer length;
    private Date desde;
    private Date hasta;
    private String desdeString;
    private String hastaString;
    private Integer requerimientoId;
    private Integer requerimientoEstadoId;
    private Integer requerimientoAreaId;
    private Integer solicitanteId;
    private Integer autorizaId;
    private Integer rolId;
    private boolean soloActivos;
}
