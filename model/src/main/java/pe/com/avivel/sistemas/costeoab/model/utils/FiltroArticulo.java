package pe.com.avivel.sistemas.costeoab.model.utils;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class FiltroArticulo implements Serializable {

    private static final long serialVersionUID = 4534109515434836240L;

    private Integer start;
    private Integer length;
    private String descripcion;
    private String codigo;
    private String codigo2;
}
