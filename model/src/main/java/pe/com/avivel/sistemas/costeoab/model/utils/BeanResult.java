package pe.com.avivel.sistemas.costeoab.model.utils;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class BeanResult implements Serializable {

    private static final long serialVersionUID = 8103825697328386787L;

    private Integer codigo;
    private String mensaje;
    private Boolean estado;
    private Integer successResult;
    private Integer errorResult;
    private Integer updateResult;
    private Integer ignoreResult;
    private Object result;
}
