package pe.com.avivel.sistemas.costeoab.model.molino.bean;

import lombok.*;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanBase;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Usuario extends BeanBase {

    private static final long serialVersionUID = -7630722737820600516L;

    private Integer usuarioId;
    private String usuarioNombre;
    private String usuarioPassword;
    private String usuarioCorreo;
    private Boolean usuarioActivo;
    private Date usuarioLastLogin;
    private List<Rol> roles;

    /*private String usuarioCodigo;
    private String usuarioUsername;*/
}
