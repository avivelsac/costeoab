package pe.com.avivel.sistemas.costeoab.model.utils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class BeanBase implements Serializable {

    private static final long serialVersionUID = -4079095971705420762L;

    private Auditoria auditoria;
    private Integer accion;
}
