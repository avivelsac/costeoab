package pe.com.avivel.sistemas.costeoab.model.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
public class SerieData implements Serializable {

    private static final long serialVersionUID = 3331218836502702726L;

    private String name;
    private Object[] data;
    private String color;
}
