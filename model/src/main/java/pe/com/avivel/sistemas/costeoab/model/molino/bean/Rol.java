package pe.com.avivel.sistemas.costeoab.model.molino.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanBase;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Rol extends BeanBase {

    private static final long serialVersionUID = -3938480226388908479L;

    private Integer rolId;
    private String rolNombre;

    /*private String rolDescripcion;*/
}
