package pe.com.avivel.sistemas.costeoab.model.utils;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class FiltroFormulaDetalle implements Serializable {

    private static final long serialVersionUID = -9042784638512897383L;

    private Integer start;
    private Integer length;
    private Integer formulaId;
}
