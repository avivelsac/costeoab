package pe.com.avivel.sistemas.costeoab.model.molino.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanBase;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FormulaDetalle extends BeanBase {

    private static final long serialVersionUID = 8754244507162137550L;

    private Integer formulaDetalleId;
    private Integer formulaDetalleSecuencia;
    private Articulo articulo;
    private BigDecimal formulaDetalleCantidad;
    private BigDecimal formulaDetallePrecio;
    private BigDecimal formulaDetalleFlete;
    private String formulaDetalleObservaciones;
    private Integer formulaDetalleEstado;
    private Formula formula;

    public Articulo getArticulo() {
        if (articulo == null) articulo = new Articulo();
        return articulo;
    }

    public Formula getFormula() {
        if (formula == null) formula = new Formula();
        return formula;
    }
}
