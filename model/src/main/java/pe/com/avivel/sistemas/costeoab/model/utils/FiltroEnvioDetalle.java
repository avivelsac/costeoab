package pe.com.avivel.sistemas.costeoab.model.utils;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class FiltroEnvioDetalle implements Serializable {

    private static final long serialVersionUID = 3751460016517483457L;

    private Integer length;
    private Integer start;
    private Integer envioId;
}
