package pe.com.avivel.sistemas.costeoab.model.molino.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanBase;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Empresa extends BeanBase {

    private static final long serialVersionUID = 6815316225569423443L;

    private Integer empresaId;
    private String empresaRuc;
    private String empresaRazonSocial;
    private Integer empresaEstado;
}
