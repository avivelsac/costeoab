package pe.com.avivel.sistemas.costeoab.model.molino.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanBase;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Formula extends BeanBase {

    private static final long serialVersionUID = -5614623244012388528L;

    private Integer formulaId;
    private String formulaCodigo;
    private String formulaEtapa;
    private String formulaDescripcion;
    private Empresa empresa;
    private BigDecimal formulaCantidad;
    private BigDecimal formulaCantidadProducida;
    private String formulaObservaciones;
    private Integer formulaParentId;
    private Integer formulaAnio;
    private Integer formulaSemana;
    private String formulaTipo;
    private Integer formulaEstado;

    public Formula(Integer formulaId) {
        this.formulaId = formulaId;
    }

    public Empresa getEmpresa() {
        if (empresa == null) empresa = new Empresa();
        return empresa;
    }
}
