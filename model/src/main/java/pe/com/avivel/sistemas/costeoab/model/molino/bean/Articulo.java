package pe.com.avivel.sistemas.costeoab.model.molino.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanBase;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Articulo extends BeanBase {

    private static final long serialVersionUID = 3941014685676304115L;

    private Integer articuloId;
    private String articuloCodigo;
    private String articuloDescripcion;
    private UnidadMedida unidadMedida;
    private String articuloCodigo2;
    private String articuloDescripcion2;
    private UnidadMedida unidadMedida2;
    private Boolean articuloSujetoDetraccion;
    private Double articuloPorcentajeDetraccion;
    private String articuloCodigoDetraccion;
    private String articuloObservacion;
    private Precio precio;

    public UnidadMedida getUnidadMedida() {
        if (unidadMedida == null) unidadMedida = new UnidadMedida();
        return unidadMedida;
    }

    public Precio getPrecio() {
        if (precio == null) precio = new Precio();
        return precio;
    }
}
