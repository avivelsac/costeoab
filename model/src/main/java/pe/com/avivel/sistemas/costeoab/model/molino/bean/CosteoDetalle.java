package pe.com.avivel.sistemas.costeoab.model.molino.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanBase;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CosteoDetalle extends BeanBase {

    private static final long serialVersionUID = 2858217499288629269L;

    private Integer costeoDetalleId;
    private Double costeoDetalleCantidad;
    private Formula formula;
    private BigDecimal costeoDetalleFlete;
    private Integer costeoDetalleEstado;
    private Costeo costeo;
    private BigDecimal costeoDetalleSubtotal;
    private BigDecimal costeoDetalleMaquila;
    private BigDecimal costeoDetalleSacos;
    private BigDecimal costeoDetallePrecioUnitario;
    private BigDecimal costeoDetalleTotal;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date costeoDetalleFechaTipoCambio;
    private BigDecimal costeoDetalleTipoCambio;
    private BigDecimal costeoDetallePrecioFleteViaje;
    private Integer costeoDetalleCantidadDetalles;
    private Integer costeoDetalleDividirEntre;
    private BigDecimal costeoDetalleFactorFlete;

    public Formula getFormula() {
        if (formula == null) formula = new Formula();
        return formula;
    }

    public Costeo getCosteo() {
        if (costeo == null) costeo = new Costeo();
        return costeo;
    }
}
