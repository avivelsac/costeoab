package pe.com.avivel.sistemas.costeoab.model.utils;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class Auditoria implements Serializable {

    private static final long serialVersionUID = 5562896680802312388L;

    private Integer usuarioCreacion;
    private Date fechaCreacion;
    private Integer usuarioModificacion;
    private Date fechaModificacion;

}
