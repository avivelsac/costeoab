package pe.com.avivel.sistemas.costeoab.model.molino.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanBase;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Envio extends BeanBase {

    private static final long serialVersionUID = 6403265498458594931L;

    private Integer envioId;
    private Empresa empresa;
    private Integer envioAnio;
    private Integer envioSemana;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date envioFechaProduccion;
    private BigDecimal envioCCG;
    private BigDecimal envioMMI;
    private BigDecimal envioViajes;
    private String envioObservaciones;
    private Integer envioEstado;
    private Costeo costeo;

    public Envio(Integer envioId) {
        this.envioId = envioId;
    }

    public Empresa getEmpresa() {
        if (empresa == null) empresa = new Empresa();
        return empresa;
    }

    public Costeo getCosteo() {
        if (costeo == null) costeo = new Costeo();
        return costeo;
    }
}
