package pe.com.avivel.sistemas.costeoab.model.molino.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanBase;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Precio extends BeanBase {

    private static final long serialVersionUID = 4006847767266175053L;

    private Integer precioId;
    private Integer precioSemana;
    private Articulo articulo;
    private BigDecimal precioValor;
    private BigDecimal precioFleteValor;
    private Boolean precioActual;
    private Integer precioEstado;
}
