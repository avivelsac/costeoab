package pe.com.avivel.sistemas.costeoab.model.molino.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.com.avivel.sistemas.costeoab.model.utils.BeanBase;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UnidadMedida extends BeanBase {

    private static final long serialVersionUID = 8515558061598123028L;

    private Integer unidadMedidaId;
    private String unidadMedidaCodigo;
    private String unidadMedidaDescripcion;
}
