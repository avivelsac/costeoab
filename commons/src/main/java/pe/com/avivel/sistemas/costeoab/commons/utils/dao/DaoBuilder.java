package pe.com.avivel.sistemas.costeoab.commons.utils.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.util.LinkedList;
import java.util.List;

public class DaoBuilder {

    private Logger logger;
    private boolean isLogger = true;
    private String schema;
    private String procedureName;
    private String daoDefinitionName = "resultado";
    private DaoDefinition daoDefinition;
    private List<DaoParameter> daoParameters = new LinkedList();
    private DaoParameter returnDaoParameter;
    private DaoGenerator daoGenerator;

    private DaoBuilder(DaoGenerator daoGenerator) {
        this.daoGenerator = daoGenerator;
    }

    public boolean isLogger() {
        return this.isLogger;
    }

    public DaoBuilder setLogger(boolean logger) {
        this.isLogger = logger;
        return this;
    }

    public String getSchema() {
        return this.schema;
    }

    public DaoBuilder setSchema(String schema) {
        if (StringUtils.isEmpty(schema)) {
            throw new IllegalArgumentException("Schema is null");
        } else {
            this.schema = schema;
            return this;
        }
    }

    public String getProcedureName() {
        return this.procedureName;
    }

    public DaoBuilder setProcedureName(String procedureName) {
        if (StringUtils.isEmpty(procedureName)) {
            throw new IllegalArgumentException("Procedure name is null");
        } else {
            this.procedureName = procedureName;
            return this;
        }
    }

    public DaoDefinition getDaoDefinition() {
        return this.daoDefinition;
    }

    public DaoBuilder setDaoDefinition(DaoDefinition daoDefinition) {
        if (StringUtils.isEmpty(daoDefinition)) {
            throw new IllegalArgumentException("DaoDefinition class is null");
        } else {
            this.daoDefinition = daoDefinition;
            return this;
        }
    }

    public List<DaoParameter> getDaoParameters() {
        return this.daoParameters;
    }

    public DaoBuilder addParameter(DaoParameter daoParameter) {
        if (daoParameter == null) {
            throw new IllegalArgumentException("DaoParameter is null");
        } else {
            this.daoParameters.add(daoParameter);
            return this;
        }
    }

    public DaoParameter getReturnDaoParameter() {
        return this.returnDaoParameter;
    }

    public DaoBuilder setReturnDaoParameter(DaoParameter returnDaoParameter) {
        if (returnDaoParameter == null) {
            throw new IllegalArgumentException("Return DaoParameter is null");
        } else {
            this.returnDaoParameter = returnDaoParameter;
            return this;
        }
    }

    public String getDaoDefinitionName() {
        return this.daoDefinitionName;
    }

    public DaoBuilder setDaoDefinitionName(String daoDefinitionName) {
        this.daoDefinitionName = daoDefinitionName;
        return this;
    }

    public Logger getLogger() {
        return this.logger;
    }

    public DaoBuilder setLogger(Logger logger) {
        this.logger = logger;
        return this;
    }

    public <T> List<T> build() {
        this.makeLogger();
        return this.daoGenerator.executeList(this);
    }

    public <T> T build(Class<T> clazz) {
        this.makeLogger();
        return this.daoGenerator.executeEntity(this, clazz);
    }

    private void makeLogger() {
        if (this.isLogger && this.logger == null) {
            this.logger = LoggerFactory.getLogger(this.daoGenerator.getClass());
        }

    }

    public static DaoBuilder getInstance(DaoGenerator daoGenerator) {
        if (daoGenerator == null) {
            throw new IllegalArgumentException("Instance DaoGenerator is null");
        } else {
            return new DaoBuilder(daoGenerator);
        }
    }
}