package pe.com.avivel.sistemas.costeoab.commons.utils.dao;

public class DaoParameter {

    private String parameterName;
    private Integer type;
    private Object value;

    public DaoParameter(String parameterName, Integer type) {
        this.parameterName = parameterName;
        this.type = type;
    }

    public DaoParameter(String parameterName, Integer type, Object value) {
        this.parameterName = parameterName;
        this.type = type;
        this.value = value;
    }

    public String getParameterName() {
        return this.parameterName;
    }

    public DaoParameter setParameterName(String parameterName) {
        this.parameterName = parameterName;
        return this;
    }

    public Integer getType() {
        return this.type;
    }

    public DaoParameter setType(Integer type) {
        this.type = type;
        return this;
    }

    public Object getValue() {
        return this.value;
    }

    public DaoParameter setValue(Object value) {
        this.value = value;
        return this;
    }
}