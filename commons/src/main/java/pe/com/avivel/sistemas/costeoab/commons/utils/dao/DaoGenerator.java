package pe.com.avivel.sistemas.costeoab.commons.utils.dao;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import java.lang.reflect.Field;
import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class DaoGenerator {
    private static final Integer SEPARATOR_COUNT = 40;
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcCall jdbcCall;
    private Map<Integer, String> sqlTypes;

    public DaoGenerator(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // SQLSERVER, MYSQL
    protected <T> T executeEntity(DaoBuilder daoBuilder, Class<T> returnClass) {
        Pair<SimpleJdbcCall, SqlParameterSource> result = this.execute(daoBuilder);
        this.printEndLogger(daoBuilder, returnClass);
        if (daoBuilder.getDaoDefinition() != null) {
            Map m = ((SimpleJdbcCall) result.getKey()).execute((SqlParameterSource) result.getValue());
            List<T> list = (List<T>) m.get(daoBuilder.getDaoDefinitionName());
            return list != null && !list.isEmpty() ? list.get(0) : null;
        } else {
            return ((SimpleJdbcCall) result.getKey()).executeObject(returnClass, (SqlParameterSource) result.getValue());
        }
    }

    protected <T> List<T> executeList(DaoBuilder daoBuilder) {
        Pair<SimpleJdbcCall, SqlParameterSource> result = this.execute(daoBuilder);
        this.printEndLogger(daoBuilder, List.class);
        Map m = ((SimpleJdbcCall) result.getKey()).execute((SqlParameterSource) result.getValue());
        return (List) m.get(daoBuilder.getDaoDefinitionName());
    }
    // -- SQLSERVER, MYSQL

    // POSTGRESQL
    /*protected <T> T executeEntity(DaoBuilder daoBuilder, Class<T> returnClass) {
        Pair<SimpleJdbcCall, SqlParameterSource> result = this.execute(daoBuilder);
        this.printEndLogger(daoBuilder, returnClass);
        if (daoBuilder.getDaoDefinition() != null) {
            List<T> list = (List) ((SimpleJdbcCall) result.getKey()).executeObject(List.class, (SqlParameterSource) result.getValue());
            return list != null && !list.isEmpty() ? list.get(0) : null;
        } else {
            return ((SimpleJdbcCall) result.getKey()).executeObject(returnClass, (SqlParameterSource) result.getValue());
        }
    }

    protected <T> List<T> executeList(DaoBuilder daoBuilder) {
        Pair<SimpleJdbcCall, SqlParameterSource> result = this.execute(daoBuilder);
        this.printEndLogger(daoBuilder, List.class);
        return (List) ((SimpleJdbcCall) result.getKey()).executeObject(List.class, (SqlParameterSource) result.getValue());
    }*/
    // -- POSTGRESQL

    private Pair<SimpleJdbcCall, SqlParameterSource> execute(DaoBuilder daoBuilder) {
        this.validate(daoBuilder);
        this.jdbcCall = new SimpleJdbcCall(this.jdbcTemplate);
        this.jdbcCall.withSchemaName(daoBuilder.getSchema());
        this.jdbcCall.withProcedureName(daoBuilder.getProcedureName());
        this.jdbcCall.withoutProcedureColumnMetaDataAccess();
        Integer parametersSize = daoBuilder.getDaoParameters().size();
        SqlParameter[] sqlParameters = new SqlParameter[parametersSize + (daoBuilder.getReturnDaoParameter() != null ? 1 : 0)];
        Map<String, Object> inParamMap = new HashMap();

        for (int i = 0; i < parametersSize; ++i) {
            DaoParameter daoParameter = (DaoParameter) daoBuilder.getDaoParameters().get(i);
            sqlParameters[i] = new SqlParameter(daoParameter.getParameterName(), daoParameter.getType());
            inParamMap.put(daoParameter.getParameterName(), daoParameter.getValue());
        }

        if (daoBuilder.getReturnDaoParameter() != null) {
            sqlParameters[parametersSize] = new SqlOutParameter(daoBuilder.getReturnDaoParameter().getParameterName(), daoBuilder.getReturnDaoParameter().getType());
        }

        this.jdbcCall.declareParameters(sqlParameters);
        SqlParameterSource in = new MapSqlParameterSource(inParamMap);
        if (daoBuilder.getDaoDefinition() != null) {
            this.jdbcCall.returningResultSet(daoBuilder.getDaoDefinitionName(), daoBuilder.getDaoDefinition());
        }

        return new MutablePair(this.jdbcCall, in);
    }

    private void validate(DaoBuilder daoBuilder) {
        if (daoBuilder.getSchema() == null) {
            throw new IllegalArgumentException("Schema name is null");
        } else if (daoBuilder.getProcedureName() == null) {
            throw new IllegalArgumentException("Procedure name is null");
        } else {
            this.printHeadLogger(daoBuilder);
        }
    }

    private void printHeadLogger(DaoBuilder daoBuilder) {
        if (daoBuilder.isLogger()) {
            Logger logger = daoBuilder.getLogger();
            logger.info(StringUtils.repeat("-", SEPARATOR_COUNT));
            logger.info(String.format("| Schema: %s", daoBuilder.getSchema()));
            logger.info(String.format("| StoredProcedure: %s", daoBuilder.getProcedureName()));
            if (daoBuilder.getReturnDaoParameter() != null) {
                logger.info(String.format("| Sql OutParameter: %s (%s)", daoBuilder.getReturnDaoParameter().getParameterName(), this.getType(daoBuilder.getReturnDaoParameter().getType())));
            }

            logger.info(String.format("| DaoDefinition: %s", daoBuilder.getDaoDefinition() != null ? "Present" : "Not Present"));
            if (daoBuilder.getDaoDefinition() != null) {
                logger.info(String.format("| DaoDefinitionName: %s", daoBuilder.getDaoDefinitionName()));
            }

            logger.info(String.format("| Parameters size: %s", daoBuilder.getDaoParameters().size()));
            Iterator i$ = daoBuilder.getDaoParameters().iterator();

            while (i$.hasNext()) {
                DaoParameter daoParameter = (DaoParameter) i$.next();
                logger.info(String.format("| - Parameter: %s (%s) => %s", daoParameter.getParameterName(), this.getType(daoParameter.getType()), daoParameter.getValue()));
            }
        }

    }

    private <T> void printEndLogger(DaoBuilder daoBuilder, Class<T> returnClass) {
        if (daoBuilder.isLogger()) {
            Logger logger = daoBuilder.getLogger();
            logger.info(String.format("| Return Type: %s", returnClass.getSimpleName()));
            logger.info(StringUtils.repeat("-", SEPARATOR_COUNT));
        }

    }

    public String getType(Integer value) {
        if (this.sqlTypes == null) {
            try {
                this.sqlTypes = new HashMap();
                Field[] arr$ = Types.class.getFields();
                int len$ = arr$.length;

                for (int i$ = 0; i$ < len$; ++i$) {
                    Field field = arr$[i$];
                    this.sqlTypes.put((Integer) field.get((Object) null), field.getName());
                }
            } catch (Exception var6) {
                throw new RuntimeException("Extract Sql Types Error");
            }
        }

        return (String) this.sqlTypes.get(value);
    }
}
