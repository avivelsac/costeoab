package pe.com.avivel.sistemas.costeoab.commons.utils.dao;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class DaoDefinition<T> extends DefaultBeanPropertyRowMapper<T> {

    public DaoDefinition(Class<T> mappedClass) {
        super(mappedClass);
    }

    public boolean findColumn(String columna, ResultSet rs) {
        try {
            return rs.findColumn(columna) > 0;
        } catch (SQLException var4) {
            return false;
        }
    }

    public Integer valInt(String columnName, ResultSet rs) throws SQLException {
        return hasColumn(rs, columnName) ? rs.getInt(columnName) : null;
    }

    public String valString(String columnName, ResultSet rs) throws SQLException {
        return hasColumn(rs, columnName) ? rs.getString(columnName) : null;
    }

    public Double valDouble(String columnName, ResultSet rs) throws SQLException {
        return hasColumn(rs, columnName) ? rs.getDouble(columnName) : null;
    }

    private boolean hasColumn(ResultSet rs, String columnName) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int columns = rsmd.getColumnCount();
        for (int i = 1; i <= columns; i++) {
            String columnNameRs = rsmd.getColumnLabel(i);
            if (columnName.equals(columnNameRs)) {
                return true;
            }
        }
        return false;
    }
}
